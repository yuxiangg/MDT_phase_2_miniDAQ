
# from dec10b8b_gen import *
# import re
import numpy as np
import collections



print "Hello! Let's get started!"

df= open('run_1229_res_001.bin','rb')

header_size = 14
header_value = 'e8393563c6c1ffffffc705010064'
packet_size = 5
packet_num = 20

EMPTY_PACKET ='ffffffffff'
# EMPTY_PACKET ='0000000000'

def bytes_from_file(filename, chunksize=1):
    with open(filename, "r") as f:
        while True:
            chunk = f.read(chunksize)
            if chunk:
                for b in chunk:
                    yield b
            else:
                break


def get_a_byte(df):
    raw_data = df.read(1)
    raw_data_hex = " ".join(hex(ord(n)) for n in raw_data)
    data_hex_string = raw_data_hex[2:4].zfill(2)
    # if data_hex_string == '0' :
    #     data_hex_string = '00'
    # print data_hex_string
    return data_hex_string;

def get_n_byte(df,n):
    data_n_byte = ''
    for i in range(n):
        data_n_byte = data_n_byte + get_a_byte(df)
    # print data_n_byte
    return data_n_byte;

def check_header(packet_num_cnt):
    header_temp = get_n_byte(df,header_size)
    if header_temp == header_value :
        header_true = 1
        header_status = "Header Found"
    else:
        header_true = 0
        header_status = "Header not Found !!!!!!!!"
    print "================ Checking header for packet #" + str(packet_num_cnt) +  ": =================="
    print "Header is: " + header_temp
    print "@@@@@@@@   " +header_status

    return header_true  

# get_n_byte(df,2)


def check_packet(packet_temp):
    # print packet_temp
    packet_bin = bin(int(packet_temp,16))[2:].zfill(40)
    # print packet_bin
    time_measure_bin = packet_bin[15:32]
    # print time_measure_bin
    time_measure = int(time_measure_bin,2)
    # print time_measure 
    Chnl_ID_bin = packet_bin[8:13]
    # print Chnl_ID_bin
    Chnl_ID = int(Chnl_ID_bin,2)
    # print Chnl_ID 

    edge_bin = packet_bin[13:15]
    # print edge_bin

    width_bin = packet_bin[32:]
    # print width_bin
    width = int(width_bin,2)
    # print width 



    last_bit_string = packet_temp[7]
    # print last_bit_string
    last_bit_binary = bin(int(last_bit_string, 16))[2:].zfill(4)
    # print last_bit_binary
    fine_time_bin = last_bit_binary[2:].zfill(2)
    # print fine_time
    fine_time = int(fine_time_bin,2)

    data_return = (Chnl_ID,edge_bin,time_measure,fine_time,width)
    # data_store.write(str(Chnl_ID)+","+str(edge_bin)+","+str(time_measure)+","+str(fine_time)+","+str(width)+",0x"+str(packet_temp)+"\n")
    return data_return


def check_packet_fin_bin(packet_temp):
    # print packet_temp
    packet_bin = bin(int(packet_temp,16))[2:].zfill(40)
    # print packet_bin
    raw_fine_time = int(packet_temp[9],16)
    # print raw_fine_time

    packet_ID = packet_bin[16:18]
    chnl_ID = int(packet_bin[18:23],2)

    # print packet_num
    data_return = (packet_ID,raw_fine_time,chnl_ID)
    return data_return



def data_ana(df):

    data_store = open('TDC_data_store.csv','wb')

    data_store.write("Chnl,Edge Mode,Measure,Fine Time,Width,Raw Data")
    data_store.write('\n')

    data_store_ordered = open('TDC_data_store_ordered.csv','wb')

    data_store_ordered.write("Chnl0,Edge Mode,Measure,Fine Time,Width,Raw Data,Chnl1,Edge Mode,Measure,Fine Time,Width,Raw Data,time_measure_diff")
    data_store_ordered.write('\n')

    Chnl0 = 5
    Chnl1 = 18

    packet_num_cnt = 1

    Fine_time_count_0 = np.zeros(4)
    Fine_time_count_1 = np.zeros(4)
    # print Fine_time_count

    hearder_true = check_header(packet_num_cnt)
    packet_num_cnt = packet_num_cnt +1
    eth_num_old = 0 
    time_diff_dic = {}
    # print "time_diff_dic:",
    # print time_diff_dic
    time_measure_old = 0 
    # print time_measure_old
    #################################################### Initialize ################################################################################################
    #######################################################################################################################################################


    while (hearder_true != 0):
        (Chnl_ID_0,edge_bin_0,time_measure_0,fine_time_0,width_0) = (0,'0',0,0,0)
        (Chnl_ID_1,edge_bin_1,time_measure_1,fine_time_1,width_1) = (0,'0',0,0,0)
        for i in range(packet_num):
            packet_temp = get_n_byte(df,packet_size)
            if packet_temp != EMPTY_PACKET:
                print packet_temp
                (Chnl_ID,edge_bin,time_measure,fine_time,width) = check_packet(packet_temp)
                data_store.write(str(Chnl_ID)+","+str(edge_bin)+","+str(time_measure)+","+str(fine_time)+","+str(width)+",0x"+str(packet_temp)+"\n")

                if Chnl_ID == Chnl0 :
                    (Chnl_ID_0,edge_bin_0,time_measure_0,fine_time_0,width_0,packet_temp_0) = (Chnl_ID,edge_bin,time_measure,fine_time,width,packet_temp)
                if Chnl_ID == Chnl1 :
                    (Chnl_ID_1,edge_bin_1,time_measure_1,fine_time_1,width_1,packet_temp_1) = (Chnl_ID,edge_bin,time_measure,fine_time,width,packet_temp)            

                # time_measure_diff = time_measure_new - time_measure_old
                # # print time_measure_new
                # # print time_measure_diff
                # if time_diff_dic.get(time_measure_diff,'Undefined key') == 'Undefined key' :
                #     time_diff_dic[time_measure_diff]= 1 
                # else: 
                #     time_diff_dic[time_measure_diff]= 1 + time_diff_dic[time_measure_diff]
                # print "time_diff_dic:",
                # print time_diff_dic
                # time_measure_old = time_measure_new

                # Fine_time_count[fine_time] +=1
                # print Fine_time_count
                
        time_measure_diff = time_measure_1 - time_measure_0
        time_measure_diff_debug = time_measure_diff
        if abs(time_measure_diff) >= 2**16:
            if time_measure_diff > 0:
                time_measure_diff = time_measure_diff -2**17
            else:
                time_measure_diff = time_measure_diff +2**17
            # data_store_ordered.write("OVERFLOW!"+","+str(time_measure_diff)+","+str(time_measure_diff_debug) +"\n")

        data_store_ordered.write(str(Chnl_ID_0)+","+str(edge_bin_0)+","+str(time_measure_0)+","+str(fine_time_0)+","+str(width_0)+",0x"+str(packet_temp_0)+",")     
        data_store_ordered.write(str(Chnl_ID_1)+","+str(edge_bin_1)+","+str(time_measure_1)+","+str(fine_time_1)+","+str(width_1)+",0x"+str(packet_temp_1)+",")   
        data_store_ordered.write(str(time_measure_diff_debug)+"\n")
        # data_store_ordered.write(str(time_measure_diff)+","+str(time_measure_diff_debug) +"\n")
        if time_diff_dic.get(time_measure_diff,'Undefined key') == 'Undefined key' :
            time_diff_dic[time_measure_diff]= 1 
        else: 
            time_diff_dic[time_measure_diff]= 1 + time_diff_dic[time_measure_diff]
        # print time_diff_dic

        Fine_time_count_0[fine_time_0] +=1
        Fine_time_count_1[fine_time_1] +=1
        eth_num_hex_string = get_n_byte(df,4)
        print eth_num_hex_string
        eth_num_current = int(eth_num_hex_string,16)
        eth_num_diff = eth_num_current - eth_num_old
        eth_num_old = eth_num_current
        if eth_num_diff != 1: 
            print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  ethernet packet loss  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" 
            print str(eth_num_diff -1)  + " packets have been lost."
        hearder_true = check_header(packet_num_cnt)
        packet_num_cnt = packet_num_cnt +1



    print '----------------------------------- Final data analysing   ------------------------------'



    total_count_0 = Fine_time_count_0[0] + Fine_time_count_0[1]  + Fine_time_count_0[2] + Fine_time_count_0[3]
    print "total received: " + str(total_count_0) + " in Chnl " + str(Chnl0)
    print "Final bin size in ps:"
    bin_count_0 = np.zeros(4)
    bin_count_0 = Fine_time_count_0 / total_count_0 * 3125
    print bin_count_0

    total_count_1 = Fine_time_count_1[0] + Fine_time_count_1[1]  + Fine_time_count_1[2] + Fine_time_count_1[3]
    print "total received: " + str(total_count_1) + " in Chnl " + str(Chnl1)
    print "Final bin size in ps:"
    bin_count_1 = np.zeros(4)
    bin_count_1 = Fine_time_count_1 / total_count_1 * 3125
    print bin_count_1

    # data_temp =  get_n_byte(df,header_size)

    # while (data_temp!= ''):
    #     print data_temp
    #     data_temp =  get_n_byte(df,header_size)
    sum = 0 
    total_count = 0
    average = 0.0

    print "Recieved intervel count:",
    print time_diff_dic


    for mykey in time_diff_dic.keys():
        # print mykey
        # print time_diff_dic[mykey]
        sum += mykey * time_diff_dic[mykey]
        total_count += time_diff_dic[mykey]

    average = float(sum)/total_count

    # print sum
    print "Total number of meaured intervel: ",
    print total_count
    print "Mean: ",
    print average


    std_sum = 0
    std = 0
    for mykey in time_diff_dic.keys():
        std_sum += time_diff_dic[mykey] * ( mykey - average)**2

    std = ((std_sum)/(total_count-1))**0.5
    print "Stdev: ",
    print std

    print 'Finish analysing!'



def ana_FT(data_cnt):
    print "sss"
    total_data = 0
    for i in range(16):
        total_data += data_cnt[i]


    data_cnt_bin = np.zeros(16)
    data_correct_right = np.zeros(4)
    data_correct_left = np.zeros(4)
    data_correct_customize = np.zeros(4)
    get_bin = lambda x, n: format(x, 'b').zfill(n)
    print "=======================Final Result:======================="
    print "We received: " + str(total_data) + "  data in total"
    for i in range(0,16):
        data_cnt_bin[i] =  float(data_cnt[i])/total_data * 3125
        if (data_cnt[i]):
            print "We have " + str(i) + ":"+ str(get_bin(i,4)) + " for " + str(data_cnt[i]) + " times and it occupays:  " + str(float(data_cnt[i])/total_data), #+ " " + data_cnt_bin[i]
            print data_cnt_bin [i]



    data_correct_left[0] = data_cnt_bin[9] + data_cnt_bin[8] + data_cnt_bin[13]
    data_correct_left[1] = data_cnt_bin[3] + data_cnt_bin[11] + data_cnt_bin[1]
    data_correct_left[2] = data_cnt_bin[6] + data_cnt_bin[2] + data_cnt_bin[7]
    data_correct_left[3] = data_cnt_bin[12] + data_cnt_bin[4] + data_cnt_bin[14]

    data_correct_right[0] = data_cnt_bin[9] + data_cnt_bin[1] + data_cnt_bin[11]
    data_correct_right[1] = data_cnt_bin[3] + data_cnt_bin[2] + data_cnt_bin[7]
    data_correct_right[2] = data_cnt_bin[6] + data_cnt_bin[4] + data_cnt_bin[14]
    data_correct_right[3] = data_cnt_bin[12] + data_cnt_bin[8] + data_cnt_bin[13]

    data_correct_customize[0] = data_cnt_bin[9] + data_cnt_bin[1] + data_cnt_bin[11]
    data_correct_customize[1] = data_cnt_bin[3] 
    data_correct_customize[2] = data_cnt_bin[6] + data_cnt_bin[2] + data_cnt_bin[7]
    data_correct_customize[3] = data_cnt_bin[12] + data_cnt_bin[8] + data_cnt_bin[13] + data_cnt_bin[4] + data_cnt_bin[14]


    print "======================= Correction ======================="
    print "---------------Result merge right edge-----------"
    for i in range(4):
        print "Bin " + str(i) + ": " + str(data_correct_right[i]) +" ps"

    print "---------------Result merge left edge-----------"
    for i in range(4):
        print "Bin " + str(i) + ": " + str(data_correct_left[i]) +" ps"

    print "---------------Result customized merge-----------"
    for i in range(4):
        print "Bin " + str(i) + ": " + str(data_correct_customize[i]) +" ps"





    return; 







def fine_bin_ana(df):


    packet_num_cnt = 1


    raw_fine_time_count = np.zeros(16)
    print raw_fine_time_count

    hearder_true = check_header(packet_num_cnt)
    packet_num_cnt = packet_num_cnt +1
    eth_num_old = 0 
    time_diff_dic = {}
    # print "time_diff_dic:",
    # print time_diff_dic
    time_measure_old = 0 
    # print time_measure_old
    #################################################### Initialize ################################################################################################
    #######################################################################################################################################################


    while (hearder_true != 0):
        for i in range(packet_num):
            packet_temp = get_n_byte(df,packet_size)
            if packet_temp != EMPTY_PACKET:
                print packet_temp
                (packet_ID,raw_fine_time,chnl_ID_temp)=check_packet_fin_bin(packet_temp)
                if packet_ID =="11":
                    raw_fine_time_count[raw_fine_time] +=1
                    # print raw_fine_time_count 
                if packet_ID =="00":
                    chnl_ID = chnl_ID_temp
                    # print chnl_ID
        eth_num_hex_string = get_n_byte(df,4)
        print eth_num_hex_string
        eth_num_current = int(eth_num_hex_string,16)
        eth_num_diff = eth_num_current - eth_num_old
        eth_num_old = eth_num_current
        if eth_num_diff != 1: 
            print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  ethernet packet loss  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" 
            print str(eth_num_diff -1)  + " packets have been lost."
        hearder_true = check_header(packet_num_cnt)
        packet_num_cnt = packet_num_cnt +1



    print '----------------------------------- Final data analysing   ------------------------------'
    print "Chnl: " + str(chnl_ID)

    ana_FT(raw_fine_time_count)




    print 'Finish analysing!'




# fine_bin_ana(df)
data_ana(df)