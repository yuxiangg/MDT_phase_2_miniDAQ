/*******************************
 * Ethernet packet capture program
 * Liang Guan (guanl@umich.edu)
 * 18 May 2016
 * 
 * How to run
 * $g++ eth_binsize.cpp -lpcap -o mac_daq
 *****************************/

#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <string>

#include <signal.h>  /*for signal() and raise()*/
#include <pcap.h>  
#include <errno.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstdlib>
#include <math.h>


using namespace std;

/* Define global variables to manipulate data */ 
pcap_t *pcap_handle;
char *dev;  //device name
char err_buf[PCAP_ERRBUF_SIZE]; 



int minus_flag=0;
int binsize_flag=0;
int binary_save_flag=0;

int *data_value;
int *chl1_data;
int *chl2_data;
int data_index=0;
int chl1_index=0;
int chl2_index=0;



int csv_index=0;
int bin_index=0;
int hit_index=0;
int data_count;
int chl_flag=0;
int chl1=99,chl2=99,last_chl=99;
int leading_edge1,leading_edge2;
int wait_for_second_hit=0;

char packet_filter[]="ether src ff:ff:ff:c7:05:01"; //filter expression
struct bpf_program fcode; // compile filter program  
bpf_u_int32 netmask=0xffffff; //

struct pcap_pkthdr* header;//
const u_char *pkt_data;//

string run_name;
bool daq_stop=false;

void signalHandler (int signum){
   daq_stop=true;
   cout<<"DAQ stopped!! --"<<signum<<endl;
}


/* create a file to save captured data*/ 
FILE *fp_binary;
FILE *fp_csv;


int packet_count_last=0;
int packet_count =0;

void check_eth_packet_lost(u_char *useless, const struct pcap_pkthdr* pkthdr, const u_char* packet_data){		
	int length;

	length=pkthdr->len;  //last 4 bytes stand for packet ID

	packet_count=(int)(*(packet_data+length-4))*1677716+(int)(*(packet_data+length-3))*65536+(int)(*(packet_data+length-2))*256+(int)(*(packet_data+length-1));
	if(packet_count!=packet_count_last+1){ 
		if(packet_count_last!=0) {
			//printf("warning:packet_lost!\n");
		}
	}	    
	packet_count_last=packet_count;
}

void binary_save(u_char *useless, const struct pcap_pkthdr* pkthdr, const u_char* packet_data){
		fwrite(packet_data,(pkthdr->len),1,fp_binary);
		bin_index++;
}

void csv_save(u_char *useless, const struct pcap_pkthdr* pkthdr, const u_char* packet_data){
	int length;
	int chl_num;
	int leading_edge;
	int pulse_width;	
	length=((pkthdr->len-18))/5;  //bytes  preload=14 postload=4
	for(int i=0;i<length;i++){
		chl_num=(*(packet_data+i*5+15))/8;
		// printf("chl_num=%d i=%d\n",chl_num,i);
		if(chl_num>23)continue;
		leading_edge=(int)(*(packet_data+i*5+15))%2*65536+(int)(*(packet_data+i*5+16))*256+(int)*(packet_data+i*5+17);
		pulse_width=(int)(*(packet_data+i*5+18));
		fprintf(fp_csv,"%d,%d,%d\n",chl_num,leading_edge,pulse_width);
		csv_index++;
	}	
}

void two_hit_resolve(u_char *useless, const struct pcap_pkthdr* pkthdr, const u_char* packet_data){
	int length;
	int chl_num;
	//int local_packet_count;
	//int packet_lost;
	int i=0;
	//int reverse_flag;
	length=((pkthdr->len-18))/5;  //bytes  preload=14 postload=4
	//local_packet_count=(int)(*(packet_data+length-4))*1677716+(int)(*(packet_data+length-3))*65536+(int)(*(packet_data+length-2))*256+(int)(*(packet_data+length-1));
	//packet_lost=(local_packet_count==packet_count_last+1)?0:1;
	chl_num=*(packet_data+i*5+15)/8;  //first TDC data chl_id

	if(chl_flag==0){
		chl1=chl_num;
		chl_flag=1;
	}
	
	for(int i=0;i<length;i++){
		chl_num=*(packet_data+i*5+15)/8;
        if(data_index>data_count)break;

		if(chl_num>23)continue;
		if(chl_flag==1&&chl_num!=chl1){//chl2 not initialized
			chl2=chl_num;
			chl_flag=2;
		}
		if(chl_num==chl1){
			 leading_edge1=(int)(*(packet_data+i*5+15))%2*65536+(int)(*(packet_data+i*5+16))*256+(int)*(packet_data+i*5+17);
			 //printf("chl1=%d,leading_edge1=%d\n",chl_num,leading_edge1);		 
			 if(wait_for_second_hit==0){  //chl1 is the first arrived hit
			 	wait_for_second_hit=1;
			 }
			 else if(last_chl==chl2){    //chl1 is the second arrived hit and last hit was chl2
			 	*(data_value+data_index)=(chl1<chl2)?(leading_edge2-leading_edge1):(leading_edge1-leading_edge2);
			 	if((*(data_value+data_index))>65536) {*(data_value+data_index)=-65536*2+*(data_value+data_index);}
			 	else if((*(data_value+data_index))<-65536) {*(data_value+data_index)=65536*2+*(data_value+data_index);}
			 	data_index++;
			 	wait_for_second_hit=0;						
			}
            last_chl=chl1;
		}
		if(chl_num==chl2){
			leading_edge2=(int)(*(packet_data+i*5+15))%2*65536+(int)(*(packet_data+i*5+16))*256+(int)*(packet_data+i*5+17);	
			//printf("chl2=%d,leading_edge2=%d\n",chl_num,leading_edge2);		 
			 if(wait_for_second_hit==0){  //chl2 is the first arrived hit
			 	wait_for_second_hit=1;
			 }
			 else if(last_chl==chl1){    //chl2 is the second arrived hit and last hit was chl1
			 	*(data_value+data_index)=(chl1<chl2)?(leading_edge2-leading_edge1):(leading_edge1-leading_edge2);
			 	if((*(data_value+data_index))>65536) {*(data_value+data_index)=-65536*2+*(data_value+data_index);}
			 	else if((*(data_value+data_index))<-65536) {*(data_value+data_index)=65536*2+*(data_value+data_index);}
			 	data_index++;
			 	wait_for_second_hit=0;			
			}
            last_chl=chl2;
		}


	}
	//cout<<"chl1=:"<<chl1<<"chl2=:"<<chl2<<"data_index=:"<<data_index<<endl;
}

int *chl_array;
int *chl_index;
int **chl_array_p;
int chl_count;


void binsize_record(u_char *useless, const struct pcap_pkthdr* pkthdr, const u_char* packet_data){
	int length;
	int chl_num;
	length=((pkthdr->len-18))/5;  //bytes  preload=14 postload=4
	for(int i=0;i<length;i++){
		chl_num=*(packet_data+i*5+15)/8;
		if(chl_num>23)continue;
		for(int j=0;j<chl_count;j++){ //search for the right chl
			if(chl_num!=(*(chl_array+j))&&(*(chl_array+j))!=99) {continue;} //channel ID not match
			if(*(chl_array+j)==99) {*(chl_array+j)=chl_num;} //first hit for this channel
			if(chl_num==(*(chl_array+j))){ //channel match
				*(*(chl_array_p+j)+*(chl_index+j))=(int)(*(packet_data+i*5+17))%4; //fine time data
				*(chl_index+j)=*(chl_index+j)+1;
				hit_index++;
				break;
			}
			else printf("warning:channel count wrong!\n");
		}
	}
}
void binsize_analysis(int chl_num,int *data_array,int count){
	int p[4]={0,0,0,0};
	for(int i=0;i<count;i++){
		if((*(data_array+i))==0)p[0]++;
		else if((*(data_array+i))==1)p[1]++;
		else if((*(data_array+i))==2)p[2]++;
		else if((*(data_array+i))==3)p[3]++;
	}
	double s=(double)count;
	printf("////////////////////////\n");
	printf("chl %d count=%d\n",chl_num,count);
	printf("count	for 0,1,2,3=	%d,	%d,	%d,	%d\n",p[0],p[1],p[2],p[3]);
	printf("binsize	for 0,1,2,3=	%.4f,	%.4f,	%.4f,	%.4f\n",p[0]/s*3125,p[1]/s*3125,p[2]/s*3125,p[3]/s*3125);
	fprintf(fp_csv,"chl_id,total,0,1,2,3\n");
	fprintf(fp_csv,"%d,%d,",chl_num,count);
	
	
	fprintf(fp_csv,"%.4f,%.4f,%.4f,%.4f\n",p[0]/s*3125,p[1]/s*3125,p[2]/s*3125,p[3]/s*3125);

}


void got_packet(u_char *useless, const struct pcap_pkthdr* pkthdr, const u_char* packet_data)
{
        

     if(binary_save_flag)binary_save(useless,pkthdr,packet_data);
      //csv_save(useless,pkthdr,packet_data);
     if(minus_flag)two_hit_resolve(useless,pkthdr,packet_data);
     if(binsize_flag)binsize_record(useless,pkthdr,packet_data);

     check_eth_packet_lost(useless,pkthdr,packet_data);
}


int packet_receiver(){

    int ret=0;
    int fd=0;   //define a file descriptor 
    fd_set rfds; //file descriptor sets for "select" function (it's a bit arrray)
    struct timeval tv;  // strcuture represents elapsed time (declared in sys/time.h)
    const int TIMEOUT=300;  //timeout in seconds

    fd=pcap_fileno(pcap_handle); //pcap_fileno returns the descriptor for the packet capture device
    FD_ZERO(&rfds);   //re-clears(empty) file descriptor set 
    FD_SET(fd,&rfds); //rebuild file descriptor set
    
    tv.tv_sec=TIMEOUT;
    tv.tv_usec=0;
 
    ret=select(fd+1, &rfds, NULL, NULL, &tv); 
    //select(): blocks the calling process until there is activity on file descriptor set or until the timeout period has expired
 
    if(-1==ret) cout<<"Select failed"<<endl;
    else if(ret){
        pcap_dispatch(pcap_handle,1, got_packet,NULL);
    }else{
        //#ifdef DEBUG
                cout<<"Select timeout on fd:"<<fd<<" Return code: "<<ret<<endl;
        //#endif
    }

    return ret;
}



void std_cal(int *value, int count){
	long sum=0;
	double mean=0;
	double rms=0;
	int minus2_number = 0;
	int minus1_number = 0;
	int zero_number = 0;
	int plus1_number = 0;
	int plus2_number = 0;
	int other_number = 0;
	for(int i=0;i<count;i++){
		sum+=*(value+i);
		if (*(value+i)==-2)
		{
			minus2_number = minus2_number + 1;
		}else if(*(value+i)==-1){
			minus1_number = minus1_number + 1;
		}else if(*(value+i)==0){
			zero_number = zero_number + 1;
		}else if(*(value+i)==1){
			plus1_number = plus1_number + 1;
		}else if(*(value+i)==2){
			plus2_number = plus2_number + 1;
		}else{
			other_number = other_number +1;
		}
	}
	mean=((double)sum)/count;
	for(int i=0;i<count;i++){
		rms+=((*(value+i))-mean)*((*(value+i))-mean);
	}
	rms=sqrt(rms/(count-1));
	printf("count=%d,mean = %lf, rms = %lf\n",count,mean,rms);
	printf("minus2_number=%d,minus1_number=%d,zero_number=%d,plus1_number=%d,plus2_number=%d,other_number=%d\n",minus2_number,minus1_number,zero_number,plus1_number,plus2_number,other_number);
	fprintf(stderr,"%d,%lf,%lf,",count,mean,rms);
	cerr << run_name << endl;
	fprintf(fp_csv,"mean=,%lf,rms,=%lf,count=,%d\n",mean,rms,count);

	for(int i=0;i<count;i++){
		if((*(value+i))-mean>5*rms||(*(value+i))-mean<-5*rms){
			//printf("warning:data[%d]=%d dev > 5*sigma!\n",i,*(value+i));
		}
	}




	return;
}

int main(int argc, char **argv)
{

    /* get run number*/
    cout<<"=================================================================================== "<<endl;
    int iPacket=0;  //packet counter
    string run_id;
    string verbose;
    if (argc!=5){cout<<"Usage: sudo ./mac_daq run_name -verbose on/off"<<endl; return 0;}
    else {
	run_id=argv[1];
	verbose=argv[3];
	// run_name="run_"+run_id+".bin";

	//cout<<"run_name is: "<<run_name<<endl;
    }
	run_name="run_"+run_id;
 	cout<<"run_name is: "<<run_name<<endl;
    /* prepare run: open file, get run pid, declare signal slot*/
    run_name="run_"+run_id+".bin";
    fp_binary=fopen(run_name.c_str(),"wb");//


    run_name="run_"+run_id+"binsize.csv";
    fp_csv=fopen(run_name.c_str(),"wb");//

    signal(SIGUSR1,signalHandler);

    ofstream fpid("current_run_meta.dat");
    fpid<<getpid()<<endl;
    
    cout<<"run pid is: "<<getpid()<<endl;
    run_name="run_"+run_id;
    // printf("test\n");

    //cout<<"2 channel minus? yes=1, no=0"<<endl;
    // cin>>minus_flag;
    // cout<<"bin size analysis? yes=1, no=0"<<endl;
    // cin>>binsize_flag;
    // cout<<"save binary file? yes=1, no=0"<<endl;
    // cin>>binary_save_flag;
    // cout<<"plese input data count: "<<endl;
    data_count = stoi(argv[4]);
    minus_flag =1;
    binsize_flag =1;
    binary_save_flag=1;
    // data_count=10000000;
    if(binsize_flag){
        if(minus_flag){chl_count=2;}
    	else {
            cout<<"plese input channel count: "<<endl;
    	    cin>>chl_count;
        }
    	chl_array   = new int[chl_count];
	    chl_index   = new int[chl_count];
	    chl_array_p = new int*[chl_count];

	    for(int i=0;i<chl_count;i++){
	    	*(chl_array+i)=99; //initialize chl_array all to 99
	    	*(chl_index+i)=0; //initialize chl_index all to 0
	    	*(chl_array_p+i) = new int[data_count*chl_count];

    	}
    }
    if(minus_flag){
    	data_value = new int[data_count];
    }

    


    pcap_if_t *alldevs;
    pcap_if_t *d;
    int i = 0;

    if (pcap_findalldevs( &alldevs, err_buf) == -1)
    {
        cerr <<"Error in pcap_findalldevs_ex:"<< err_buf << endl;
        exit(1);
    }
    //for(d= alldevs; d != NULL; d= d->next)
    //{
        //cout << ++i << ":"<< d->name <<"    ";
        //if (d->description)
            //cout << d->description << endl;
        //else
            //cout<< "(No description available)" << endl;
    //}
    int device_number;
    //cout << "plese select your etherner device: ";
    //cin >> device_number;
    //please set according to your host's ethernert port
    device_number=2;

    d= alldevs;
    for(i=0; i < device_number -1; i++){
        d=d->next;        
    }
    //cout << ++i << ":"<< d->name <<"    ";
    //if (d->description)
        //cout << d->description << endl;
    //else
        //cout<< "(No description available)" << endl;


    /* open the ethernet device*/
    //dev = pcap_lookupdev(err_buf);
    //if (dev==NULL){
    //    cout<<"Could not find device "<<dev<<" : "<<err_buf<<endl;
    //    return -1;
    //}else{
	//printf("Found Device %s\n",dev);
    //}
    dev = d->name;
    pcap_handle = pcap_open_live(dev,65536,1,10000, err_buf);  //device name, snap length, promiscuous mode, to_ms, error_buffer
    if (pcap_handle==NULL){
        cout<<"Could not open device "<<dev<<" : "<<err_buf<<endl;
	return -1;
    }

    /* compile the filter */
    if (pcap_compile(pcap_handle, &fcode, packet_filter, 1, netmask) < 0){
	    cout<<"Unable to compile the packet filter. Check the syntax!"<<endl;
            pcap_close(pcap_handle);
            return -1;
    }

    /* apply filter*/ 
    if (pcap_setfilter(pcap_handle, &fcode) < 0){
	    cout<<"Filter address error. Can not apply filter!"<<endl;
            pcap_close(pcap_handle);
            return -1;
    }

    /* sniff data packets*/
    //while(packet_receiver()!=0&&iPacket<N_PACKET)
    //while(packet_receiver()!=0 && daq_stop==false && data_index<data_count)
    while(packet_receiver()!=0 && daq_stop==false && data_index<data_count &&csv_index<data_count*2&&bin_index<data_count&&hit_index<data_count*chl_count)
    {
	   ++iPacket;
	   if(verbose=="on") cout<<"Packet #"<<iPacket<<endl;
    }

    /* clean up and close pcap */
    pcap_freealldevs(alldevs);
    pcap_freecode(&fcode);
    pcap_close(pcap_handle);


	if(minus_flag){
		for(int i=0;i<data_count;i++){
    		fprintf(fp_csv,"%d,%d\n",i,*(data_value+i));
    	}
		std_cal(data_value,data_index);
    	delete data_value;
    }



    if(binsize_flag){
	    for(int i=0;i<chl_count;i++){
	    	binsize_analysis(*(chl_array+i),*(chl_array_p+i),*(chl_index+i));
	    }
	    for(int i=0;i<chl_count;i++){
	    	delete *(chl_array_p+i);
	    }
	    delete chl_array;
	    delete chl_index;
	}
	
    fclose(fp_binary);
    fclose(fp_csv);
	

}
