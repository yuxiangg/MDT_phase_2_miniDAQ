`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/28/2018 01:31:11 PM
// Design Name: 
// Module Name: MMCM_TOP
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MMCM_TOP(
    input SYSCLK_P,
    input SYSCLK_N,
//    output Output_clk_0_P,
//    output Output_clk_0_N,
//    output Output_clk_1_P,
//    output Output_clk_1_N
      output  clk0_out,
      output  clk1_out
    );

   wire clk0,clk1;
    wire clk40;
    wire clk200;


reg[1:0] psen_0_r;
reg psen_0;
reg[1:0] psen_1_r;
reg psen_1;


wire psen_0_vio;
wire psincdec_0_vio;
wire psdone_0_vio;

wire psen_1_vio;
wire psincdec_1_vio;
wire psdone_1_vio;


wire[9:0] clk0_start_offset_vio;
wire[9:0] clk1_start_offset_vio;



  MMCM_input MMCM_input_inst
   (
   // Clock in ports
    .clk_in1_p(SYSCLK_P),    // input clk_in1_p
    .clk_in1_n(SYSCLK_N),    // input clk_in1_n
    // Clock out ports
    .clk_out1(clk40),     // output clk_out1
    .clk_out2(clk200));    // output clk_out2






  input_clock input_clock_inst_0
   (
   // Clock in ports
//    .clk_in1_p(SYSCLK_P),    // input clk_in1_p
//    .clk_in1_n(SYSCLK_N),    // input clk_in1_n
     // Clock in ports
    .clk_in1(clk200),
    // Clock out ports
    .clk_out1(clk0),     // output clk_out1
    //.clk_out2(),     // output clk_out2
    // Dynamic phase shift ports
    .psclk(clk40), // input psclk
    .psen(psen_0), // input psen
    .psincdec(psincdec_0_vio),     // input psincdec
    .psdone(psdone_0_vio) // output psdone
    );      

  input_clock input_clock_inst_1
   (
   // Clock in ports
//    .clk_in1_p(SYSCLK_P),    // input clk_in1_p
//    .clk_in1_n(SYSCLK_N),    // input clk_in1_n
     // Clock in ports
    .clk_in1(clk200),
    // Clock out ports
    .clk_out1(clk1),     // output clk_out1
    //.clk_out2(),     // output clk_out2
    // Dynamic phase shift ports
    .psclk(clk40), // input psclk
    .psen(psen_1), // input psen
    .psincdec(psincdec_1_vio),     // input psincdec
    .psdone(psdone_1_vio) // output psdone
    );  

vio_phase_control vio_phase_control_inst (
  .clk(clk40),                // input wire clk
  .probe_in0(psdone_0_vio),    // input wire [0 : 0] probe_in0
  .probe_in1(psdone_1_vio),    // input wire [0 : 0] probe_in0
  .probe_out0(psen_0_vio),  // output wire [0 : 0] probe_out0
  .probe_out1(psincdec_0_vio),  // output wire [0 : 0] probe_out1
  .probe_out2(psen_1_vio),  // output wire [0 : 0] probe_out2
  .probe_out3(psincdec_1_vio),  // output wire [0 : 0] probe_out3
  .probe_out4(clk0_start_offset_vio),
  .probe_out5(clk1_start_offset_vio)
);





always @(posedge clk40) begin 
  psen_0_r <= {psen_0_r[0],psen_0_vio};
  psen_0   <= psen_0_r[0]&(~psen_0_r[1]); 
end


always @(posedge clk40) begin 
  psen_1_r <= {psen_1_r[0],psen_1_vio};
  psen_1   <= psen_1_r[0]&(~psen_1_r[1]); 
end




//   OBUFDS #(
//      .IOSTANDARD("DEFAULT"), // Specify the output I/O standard
//      .SLEW("SLOW")           // Specify the output slew rate
//   ) OBUFDS_inst0 (
//      .O(Output_clk_0_P),     // Diff_p output (connect directly to top-level port)
//      .OB(Output_clk_0_N),   // Diff_n output (connect directly to top-level port)
//      .I(clk0)      // Buffer input 
//   );


//   OBUFDS #(
//      .IOSTANDARD("DEFAULT"), // Specify the output I/O standard
//      .SLEW("SLOW")           // Specify the output slew rate
//   ) OBUFDS_inst1 (
//      .O(Output_clk_1_P),     // Diff_p output (connect directly to top-level port)
//      .OB(Output_clk_1_N),   // Diff_n output (connect directly to top-level port)
//      .I(clk1)      // Buffer input 
//   );



// reg[3:0] cnt_0;

// always @(posedge clk0) begin 
//   if( cnt_0 == 4'd9 )
//     cnt_0 <= 4'b0;
//   else
//     cnt_0 <= cnt_0 + 4'b1; 
// end

// always @(posedge clk0) begin 
//   clk0_out <= cnt_0[3];
// end


// reg[3:0] cnt_1;

// always @(posedge clk1) begin 
//   if( cnt_1 == 4'd9 )
//     cnt_1 <= 4'b0;
//   else
//     cnt_1 <= cnt_1 + 4'b1; 
// end

// always @(posedge clk1) begin 
//   clk1_out <= cnt_1[3];
// end
 

Clock_div   Clock_div_0(
  .clk0(clk0),
  .start_high_input(clk0_start_offset_vio),
  .clock_out_out(clk0_out)
  );

Clock_div   Clock_div_1(
  .clk0(clk1),
  .start_high_input(clk1_start_offset_vio),
  .clock_out_out(clk1_out)
  );


endmodule



module Clock_div (
  input clk0,    // Clock
  input[9:0] start_high_input, // Clock Enable
  output reg clock_out_out
  
);

reg[9:0] cnt_0;

reg[9:0] start_high,start_high_temp0_sync_reg,start_high_temp1_sync_reg;
wire[9:0] end_high_cal0,end_high_cal1;
reg[9:0] end_high;

wire overflow;
reg clk_out;


always @(posedge clk0) begin 
  start_high_temp0_sync_reg <= start_high_input;
  start_high_temp1_sync_reg <= start_high_temp0_sync_reg;
  start_high       <= start_high_temp1_sync_reg;
end

assign end_high_cal0 = start_high + 10'd200;
assign overflow = end_high_cal0 > 10'd399;
assign end_high_cal1 = overflow ?  end_high_cal0 - 10'd400 : end_high_cal0; 

always @(posedge clk0) begin 
  end_high       <= end_high_cal1;
end



always @(posedge clk0) begin 
  if( cnt_0 == 10'd399 )
    cnt_0 <= 10'b0;
  else
    cnt_0 <= cnt_0 + 10'b1; 
end

always @(posedge clk0) begin 
  if (!overflow) begin 
    if ((cnt_0 >= start_high) & (cnt_0 <= end_high)) begin
      clk_out <= 1'b1;
    end else begin 
      clk_out <= 1'b0;
    end
  end else begin 
    if ((cnt_0 >= start_high) | (cnt_0 <= end_high)) begin
      clk_out <= 1'b1;
    end else begin 
      clk_out <= 1'b0;
    end
  end
end



always @(posedge clk0) begin 
  clock_out_out <= clk_out;
end



endmodule