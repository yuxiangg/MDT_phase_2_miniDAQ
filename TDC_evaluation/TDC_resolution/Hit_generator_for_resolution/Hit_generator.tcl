
#  _________   ________     ________       
# / U OF M  / | LSA    /   / Physics/
# /__ATLAS__/ |   ___   | |   ______/
#    |   |    |  |   /  | |  |
#    |   |    |  |___/  | |  /______     
#    |   |    |         | |         /
#    /___/    |________/   /________/

# File Name  : Hit_generator.tcl
# Author     : Xiong Xiao 
# Revision   : 
#              First created on 2018-12-24 
# Note       : 
     



#*****************************************************************************************
#################################  README FIRST ##########################################
#*****************************************************************************************

# In order to execute the tcl script and build the project, run Vivado and go to: 
# Tools -> Run Tcl Script...
#
# An alternative way would be to open a terminal, and run this command:
# vivado -mode batch -source <PATH>/build.tcl
#
# For more info on how to make further changes to the script, see: 
# http://xillybus.com/tutorials/vivado-version-control-packaging



set_param general.maxThreads 8


# Set the reference directory for source file relative paths (by default the value is script directory path)
set origin_dir [file dirname [info script]]
set source_dir "$origin_dir"
set constrain_dir "$origin_dir"
set firmware_dir "$origin_dir/../../Hit_genrator"


create_project Hit_generator $firmware_dir -part xc7k325tffg900-2  -force
set_property board_part xilinx.com:kc705:part0:1.3 [current_project]



 

add_files -norecurse $source_dir/MMCM_TOP.v
add_files -fileset constrs_1 -norecurse $constrain_dir/KC705_constrain.xdc


create_ip -name clk_wiz -vendor xilinx.com -library ip -version 5.3 -module_name MMCM_input
set_property -dict [list CONFIG.JITTER_SEL {Min_O_Jitter} CONFIG.PRIM_SOURCE {Differential_clock_capable_pin} CONFIG.PRIM_IN_FREQ {200} CONFIG.CLKOUT2_USED {true} CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {40} CONFIG.CLKOUT2_REQUESTED_OUT_FREQ {200} CONFIG.USE_LOCKED {false} CONFIG.USE_RESET {false} CONFIG.CLKIN1_JITTER_PS {50.0} CONFIG.MMCM_DIVCLK_DIVIDE {1} CONFIG.MMCM_BANDWIDTH {HIGH} CONFIG.MMCM_CLKFBOUT_MULT_F {7.000} CONFIG.MMCM_CLKIN1_PERIOD {5.0} CONFIG.MMCM_CLKOUT0_DIVIDE_F {35.000} CONFIG.MMCM_CLKOUT1_DIVIDE {7} CONFIG.NUM_OUT_CLKS {2} CONFIG.CLKOUT1_JITTER {118.269} CONFIG.CLKOUT1_PHASE_ERROR {76.281} CONFIG.CLKOUT2_JITTER {86.913} CONFIG.CLKOUT2_PHASE_ERROR {76.281}] [get_ips MMCM_input]

create_ip -name clk_wiz -vendor xilinx.com -library ip -version 5.3 -module_name input_clock
set_property -dict [list CONFIG.USE_DYN_PHASE_SHIFT {true} CONFIG.JITTER_SEL {Min_O_Jitter} CONFIG.PRIM_SOURCE {Global_buffer} CONFIG.PRIM_IN_FREQ {200} CONFIG.CLK_OUT1_USE_FINE_PS_GUI {true} CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {204.67} CONFIG.USE_LOCKED {false} CONFIG.USE_RESET {false} CONFIG.CLKIN1_JITTER_PS {50.0} CONFIG.MMCM_DIVCLK_DIVIDE {6} CONFIG.MMCM_BANDWIDTH {HIGH} CONFIG.MMCM_CLKFBOUT_MULT_F {43.000} CONFIG.MMCM_CLKIN1_PERIOD {5.0} CONFIG.MMCM_CLKOUT0_DIVIDE_F {7.000} CONFIG.MMCM_CLKOUT0_USE_FINE_PS {true} CONFIG.CLKOUT1_JITTER {116.472} CONFIG.CLKOUT1_PHASE_ERROR {159.126}] [get_ips input_clock]

create_ip -name vio -vendor xilinx.com -library ip -version 3.0 -module_name vio_phase_control
set_property -dict [list CONFIG.C_NUM_PROBE_IN {2} CONFIG.C_NUM_PROBE_OUT {6} CONFIG.C_PROBE_OUT4_WIDTH {10} CONFIG.C_PROBE_OUT5_WIDTH {10}] [get_ips vio_phase_control]


update_compile_order -fileset sources_1