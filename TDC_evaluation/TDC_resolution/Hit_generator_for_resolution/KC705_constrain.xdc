## LOC constrain for SYSCLK  (200 MHz LVDS on VC707, RefDes: U51)
set_property IOSTANDARD LVDS [get_ports SYSCLK_P]
set_property PACKAGE_PIN AD12 [get_ports SYSCLK_P]
set_property PACKAGE_PIN AD11 [get_ports SYSCLK_N]
set_property IOSTANDARD LVDS [get_ports SYSCLK_N]



#set_property IOSTANDARD LVDS [get_ports Output_clk_0_P]
#set_property PACKAGE_PIN D35 [get_ports Output_clk_0_P]
#set_property PACKAGE_PIN D36 [get_ports Output_clk_0_N]
#set_property IOSTANDARD LVDS [get_ports Output_clk_0_N]

#set_property IOSTANDARD LVDS [get_ports Output_clk_1_P]
#set_property PACKAGE_PIN AJ32 [get_ports Output_clk_1_P]
#set_property PACKAGE_PIN AK32 [get_ports Output_clk_1_N]
#set_property IOSTANDARD LVDS [get_ports Output_clk_1_N]


#set_property IOSTANDARD LVCMOS [get_ports Output_clk_0_P]
#set_property PACKAGE_PIN D35 [get_ports Output_clk_0_P]
#set_property PACKAGE_PIN D36 [get_ports Output_clk_0_N]
#set_property IOSTANDARD LVCMOS [get_ports Output_clk_0_N]

set_property IOSTANDARD LVCMOS25 [get_ports clk0_out]
set_property PACKAGE_PIN L25 [get_ports clk0_out]
set_property DRIVE 16 [get_ports clk0_out]
set_property SLEW FAST [get_ports clk0_out]


set_property PACKAGE_PIN K25 [get_ports clk1_out]
set_property IOSTANDARD LVCMOS25 [get_ports clk1_out]
set_property DRIVE 16 [get_ports clk1_out]
set_property SLEW FAST [get_ports clk1_out]

set_false_path -to [get_pins -hier -filter {NAME =~ */*_sync*/D}]