
#  _________   ________     ________       
# / U OF M  / | LSA    /   / Physics/
# /__ATLAS__/ |   ___   | |   ______/
#    |   |    |  |   /  | |  |
#    |   |    |  |___/  | |  /______     
#    |   |    |         | |         /
#    /___/    |________/   /________/

# File Name  : Firmware.tcl
# Author     : Xiong Xiao 
# Revision   : 
#              First created on 2018-12-24 
# Note       : 
     



#*****************************************************************************************
#################################  README FIRST ##########################################
#*****************************************************************************************

# In order to execute the tcl script and build the project, run Vivado and go to: 
# Tools -> Run Tcl Script...
#
# An alternative way would be to open a terminal, and run this command:
# vivado -mode batch -source <PATH>/build.tcl
#
# For more info on how to make further changes to the script, see: 
# http://xillybus.com/tutorials/vivado-version-control-packaging



set_param general.maxThreads 8


# Set the reference directory for source file relative paths (by default the value is script directory path)
set origin_dir [file dirname [info script]]
set source_dir "$origin_dir/firmware/source"
set constrain_dir "$origin_dir/firmware/constrain"
set firmware_dir "$origin_dir/../../MDT_TDC_ver1_firmware"


create_project MDT_TDC_ver1_firmware $firmware_dir -part xc7k325tffg900-2  -force

set_property board_part xilinx.com:kc705:part0:1.3 [current_project]


#*****************************************************************************************
#################################  Adding Source #########################################
#*****************************************************************************************


add_files $source_dir/Test_env

add_files $source_dir/uart

update_compile_order -fileset sources_1
#*****************************************************************************************
#################################  Adding Constrain ######################################
#*****************************************************************************************


add_files -fileset constrs_1 -norecurse $constrain_dir/constrain_test_env_position.xdc
add_files -fileset constrs_1 -norecurse $constrain_dir/constrain_test_env_timing.xdc
add_files -fileset constrs_1 -norecurse $constrain_dir/constrain_test_env_uart.xdc



#*****************************************************************************************
##############################   Generate Clock wiz ######################################
#*****************************************************************************************

create_ip -name clk_wiz -vendor xilinx.com -library ip -version 5.3 -module_name input_clock
set_property -dict [list CONFIG.PRIM_SOURCE {Differential_clock_capable_pin} CONFIG.PRIM_IN_FREQ {40} CONFIG.CLKOUT2_USED {true} CONFIG.CLKOUT3_USED {true} CONFIG.CLKOUT4_USED {true} CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {40} CONFIG.CLKOUT2_REQUESTED_OUT_FREQ {160} CONFIG.CLKOUT3_REQUESTED_OUT_FREQ {320} CONFIG.CLKOUT4_REQUESTED_OUT_FREQ {64} CONFIG.USE_LOCKED {false} CONFIG.USE_RESET {false} CONFIG.CLKIN1_JITTER_PS {250.0} CONFIG.MMCM_DIVCLK_DIVIDE {1} CONFIG.MMCM_CLKFBOUT_MULT_F {24.000} CONFIG.MMCM_CLKIN1_PERIOD {25.0} CONFIG.MMCM_CLKOUT0_DIVIDE_F {24.000} CONFIG.MMCM_CLKOUT1_DIVIDE {6} CONFIG.MMCM_CLKOUT2_DIVIDE {3} CONFIG.MMCM_CLKOUT3_DIVIDE {15} CONFIG.NUM_OUT_CLKS {4} CONFIG.CLKOUT1_JITTER {247.096} CONFIG.CLKOUT1_PHASE_ERROR {196.976} CONFIG.CLKOUT2_JITTER {169.112} CONFIG.CLKOUT2_PHASE_ERROR {196.976} CONFIG.CLKOUT3_JITTER {151.082} CONFIG.CLKOUT3_PHASE_ERROR {196.976} CONFIG.CLKOUT4_JITTER {213.278} CONFIG.CLKOUT4_PHASE_ERROR {196.976}] [get_ips input_clock]


create_ip -name clk_wiz -vendor xilinx.com -library ip -version 5.3 -module_name clk_hit
set_property -dict [list CONFIG.PRIM_SOURCE {Differential_clock_capable_pin} CONFIG.PRIM_IN_FREQ {156.25} CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {200} CONFIG.USE_LOCKED {false} CONFIG.USE_RESET {false} CONFIG.CLKIN1_JITTER_PS {64.0} CONFIG.MMCM_DIVCLK_DIVIDE {5} CONFIG.MMCM_CLKFBOUT_MULT_F {32.000} CONFIG.MMCM_CLKIN1_PERIOD {6.4} CONFIG.MMCM_CLKOUT0_DIVIDE_F {5.000} CONFIG.CLKOUT1_JITTER {172.451} CONFIG.CLKOUT1_PHASE_ERROR {204.239}] [get_ips clk_hit]



#*****************************************************************************************
##############################     Generate FIFO    ######################################
#*****************************************************************************************


create_ip -name fifo_generator -vendor xilinx.com -library ip -version 13.1 -module_name fifo_160bit
set_property -dict [list CONFIG.Fifo_Implementation {Independent_Clocks_Builtin_FIFO} CONFIG.Performance_Options {First_Word_Fall_Through} CONFIG.Input_Data_Width {160} CONFIG.Read_Clock_Frequency {40} CONFIG.Write_Clock_Frequency {40} CONFIG.Output_Data_Width {160} CONFIG.Reset_Type {Asynchronous_Reset} CONFIG.Use_Dout_Reset {false} CONFIG.Full_Threshold_Assert_Value {1017} CONFIG.Full_Threshold_Negate_Value {1016} CONFIG.Empty_Threshold_Assert_Value {6} CONFIG.Empty_Threshold_Negate_Value {7} CONFIG.write_clk.FREQ_HZ {40000000} CONFIG.read_clk.FREQ_HZ {40000000}] [get_ips fifo_160bit]

create_ip -name fifo_generator -vendor xilinx.com -library ip -version 13.1 -module_name raw_data_fifo
set_property -dict [list CONFIG.Fifo_Implementation {Independent_Clocks_Builtin_FIFO} CONFIG.Performance_Options {First_Word_Fall_Through} CONFIG.Input_Data_Width {10} CONFIG.Input_Depth {512} CONFIG.Read_Clock_Frequency {160} CONFIG.Write_Clock_Frequency {64} CONFIG.Output_Data_Width {10} CONFIG.Output_Depth {512} CONFIG.Reset_Type {Asynchronous_Reset} CONFIG.Use_Dout_Reset {false} CONFIG.Data_Count_Width {9} CONFIG.Write_Data_Count_Width {9} CONFIG.Read_Data_Count_Width {9} CONFIG.Full_Threshold_Assert_Value {505} CONFIG.Full_Threshold_Negate_Value {504} CONFIG.Empty_Threshold_Assert_Value {6} CONFIG.Empty_Threshold_Negate_Value {7} CONFIG.write_clk.FREQ_HZ {64000000} CONFIG.read_clk.FREQ_HZ {160000000}] [get_ips raw_data_fifo]

#create_ip -name fifo_generator -vendor xilinx.com -library ip -version 13.1 -module_name tdc_data_fifo
#set_property -dict [list CONFIG.Fifo_Implementation {Common_Clock_Builtin_FIFO} CONFIG.Performance_Options {First_Word_Fall_Through} CONFIG.Input_Data_Width {40} CONFIG.Input_Depth {8192} CONFIG.Output_Data_Width {40} CONFIG.Output_Depth {8192} CONFIG.Reset_Type {Asynchronous_Reset} CONFIG.Use_Dout_Reset {false} CONFIG.Data_Count_Width {13} CONFIG.Write_Data_Count_Width {13} CONFIG.Read_Data_Count_Width {13} CONFIG.Full_Threshold_Assert_Value {8191} CONFIG.Full_Threshold_Negate_Value {8190} CONFIG.Empty_Threshold_Assert_Value {4} CONFIG.Empty_Threshold_Negate_Value {5}] [get_ips tdc_data_fifo]

create_ip -name fifo_generator -vendor xilinx.com -library ip -version 13.1 -module_name tdc_data_fifo
set_property -dict [list CONFIG.Fifo_Implementation {Independent_Clocks_Builtin_FIFO} CONFIG.Performance_Options {First_Word_Fall_Through} CONFIG.Input_Data_Width {40} CONFIG.Read_Clock_Frequency {125} CONFIG.Write_Clock_Frequency {160} CONFIG.Output_Data_Width {40} CONFIG.Reset_Type {Asynchronous_Reset} CONFIG.Use_Dout_Reset {false} CONFIG.Use_Extra_Logic {false} CONFIG.Data_Count_Width {10} CONFIG.Write_Data_Count_Width {10} CONFIG.Read_Data_Count_Width {10} CONFIG.Full_Threshold_Assert_Value {1013} CONFIG.Full_Threshold_Negate_Value {1012} CONFIG.Empty_Threshold_Assert_Value {6} CONFIG.Empty_Threshold_Negate_Value {7} CONFIG.write_clk.FREQ_HZ {160000000} CONFIG.read_clk.FREQ_HZ {125000000}] [get_ips tdc_data_fifo]





create_ip -name fifo_generator -vendor xilinx.com -library ip -version 13.1 -module_name fifo_sync_coarse_counter
set_property -dict [list CONFIG.Fifo_Implementation {Independent_Clocks_Block_RAM} CONFIG.Performance_Options {First_Word_Fall_Through} CONFIG.Input_Data_Width {13} CONFIG.Input_Depth {16} CONFIG.Reset_Pin {false} CONFIG.Output_Data_Width {13} CONFIG.Output_Depth {16} CONFIG.Reset_Type {Asynchronous_Reset} CONFIG.Full_Flags_Reset_Value {0} CONFIG.Use_Dout_Reset {false} CONFIG.Data_Count_Width {4} CONFIG.Write_Data_Count_Width {4} CONFIG.Read_Data_Count_Width {4} CONFIG.Full_Threshold_Assert_Value {15} CONFIG.Full_Threshold_Negate_Value {14} CONFIG.Empty_Threshold_Assert_Value {4} CONFIG.Empty_Threshold_Negate_Value {5}] [get_ips fifo_sync_coarse_counter]

create_ip -name fifo_generator -vendor xilinx.com -library ip -version 13.1 -module_name fifo_latency_hit
set_property -dict [list CONFIG.Fifo_Implementation {Common_Clock_Builtin_FIFO} CONFIG.Performance_Options {First_Word_Fall_Through} CONFIG.Input_Data_Width {45} CONFIG.Input_Depth {512} CONFIG.Reset_Pin {true} CONFIG.Output_Data_Width {45} CONFIG.Output_Depth {512} CONFIG.Reset_Type {Asynchronous_Reset} CONFIG.Use_Dout_Reset {false} CONFIG.Use_Extra_Logic {false} CONFIG.Data_Count_Width {9} CONFIG.Write_Data_Count_Width {9} CONFIG.Read_Data_Count_Width {9} CONFIG.Full_Threshold_Assert_Value {511} CONFIG.Full_Threshold_Negate_Value {510} CONFIG.Empty_Threshold_Assert_Value {4} CONFIG.Empty_Threshold_Negate_Value {5}] [get_ips fifo_latency_hit]

create_ip -name fifo_generator -vendor xilinx.com -library ip -version 13.1 -module_name fifo_sync_hit_info
set_property -dict [list CONFIG.Fifo_Implementation {Independent_Clocks_Block_RAM} CONFIG.Performance_Options {First_Word_Fall_Through} CONFIG.Input_Data_Width {22} CONFIG.Input_Depth {16} CONFIG.Reset_Pin {false} CONFIG.Output_Data_Width {22} CONFIG.Output_Depth {16} CONFIG.Reset_Type {Asynchronous_Reset} CONFIG.Full_Flags_Reset_Value {0} CONFIG.Use_Dout_Reset {false} CONFIG.Data_Count_Width {4} CONFIG.Write_Data_Count_Width {4} CONFIG.Read_Data_Count_Width {4} CONFIG.Full_Threshold_Assert_Value {15} CONFIG.Full_Threshold_Negate_Value {14} CONFIG.Empty_Threshold_Assert_Value {4} CONFIG.Empty_Threshold_Negate_Value {5}] [get_ips fifo_sync_hit_info]

create_ip -name fifo_generator -vendor xilinx.com -library ip -version 13.1 -module_name fifo_latency_data
set_property -dict [list CONFIG.Fifo_Implementation {Common_Clock_Builtin_FIFO} CONFIG.Performance_Options {First_Word_Fall_Through} CONFIG.Input_Data_Width {47} CONFIG.Input_Depth {512} CONFIG.Output_Data_Width {47} CONFIG.Output_Depth {512} CONFIG.Reset_Type {Asynchronous_Reset} CONFIG.Use_Dout_Reset {false} CONFIG.Data_Count_Width {9} CONFIG.Write_Data_Count_Width {9} CONFIG.Read_Data_Count_Width {9} CONFIG.Full_Threshold_Assert_Value {511} CONFIG.Full_Threshold_Negate_Value {510} CONFIG.Empty_Threshold_Assert_Value {4} CONFIG.Empty_Threshold_Negate_Value {5}] [get_ips fifo_latency_data]

create_ip -name fifo_generator -vendor xilinx.com -library ip -version 13.1 -module_name fifo_hit_stat
set_property -dict [list CONFIG.Fifo_Implementation {Independent_Clocks_Block_RAM} CONFIG.Performance_Options {First_Word_Fall_Through} CONFIG.Input_Data_Width {32} CONFIG.Input_Depth {16} CONFIG.Reset_Pin {false} CONFIG.Output_Data_Width {32} CONFIG.Output_Depth {16} CONFIG.Reset_Type {Asynchronous_Reset} CONFIG.Full_Flags_Reset_Value {0} CONFIG.Use_Dout_Reset {false} CONFIG.Data_Count_Width {4} CONFIG.Write_Data_Count_Width {4} CONFIG.Read_Data_Count_Width {4} CONFIG.Full_Threshold_Assert_Value {15} CONFIG.Full_Threshold_Negate_Value {14} CONFIG.Empty_Threshold_Assert_Value {4} CONFIG.Empty_Threshold_Negate_Value {5}] [get_ips fifo_hit_stat]


#*****************************************************************************************
##############################     Generate blk     ######################################
#*****************************************************************************************

create_ip -name blk_mem_gen -vendor xilinx.com -library ip -version 8.3 -module_name blk_mem_latency
set_property -dict [list CONFIG.Write_Width_A {32} CONFIG.Write_Depth_A {512} CONFIG.Enable_A {Always_Enabled} CONFIG.Read_Width_A {32} CONFIG.Write_Width_B {32} CONFIG.Read_Width_B {32}] [get_ips blk_mem_latency]




#*****************************************************************************************
##############################     Generate ila     ######################################
#*****************************************************************************************


create_ip -name ila -vendor xilinx.com -library ip -version 6.1 -module_name jtag_ila
set_property -dict [list CONFIG.C_NUM_OF_PROBES {5}] [get_ips jtag_ila]

create_ip -name ila -vendor xilinx.com -library ip -version 6.1 -module_name ASD_config_ila
set_property -dict [list CONFIG.C_NUM_OF_PROBES {4}] [get_ips ASD_config_ila]

create_ip -name ila -vendor xilinx.com -library ip -version 6.1 -module_name d_line_ila
set_property -dict [list CONFIG.C_NUM_OF_PROBES {4}] [get_ips d_line_ila]

create_ip -name ila -vendor xilinx.com -library ip -version 6.1 -module_name tdc_data_interface_ila
set_property -dict [list CONFIG.C_PROBE7_WIDTH {40} CONFIG.C_PROBE4_WIDTH {3} CONFIG.C_PROBE1_WIDTH {8} CONFIG.C_NUM_OF_PROBES {11}] [get_ips tdc_data_interface_ila]

create_ip -name ila -vendor xilinx.com -library ip -version 6.1 -module_name io_monitor_ila
set_property -dict [list CONFIG.C_PROBE0_WIDTH {24} CONFIG.C_NUM_OF_PROBES {3}] [get_ips io_monitor_ila]

create_ip -name ila -vendor xilinx.com -library ip -version 6.1 -module_name ila_latency_check
set_property -dict [list CONFIG.C_PROBE48_WIDTH {32} CONFIG.C_PROBE47_WIDTH {32} CONFIG.C_PROBE46_WIDTH {32} CONFIG.C_PROBE45_WIDTH {32} CONFIG.C_PROBE44_WIDTH {32} CONFIG.C_PROBE43_WIDTH {32} CONFIG.C_PROBE42_WIDTH {32} CONFIG.C_PROBE41_WIDTH {32} CONFIG.C_PROBE40_WIDTH {32} CONFIG.C_PROBE39_WIDTH {32} CONFIG.C_PROBE38_WIDTH {32} CONFIG.C_PROBE37_WIDTH {32} CONFIG.C_PROBE36_WIDTH {32} CONFIG.C_PROBE35_WIDTH {32} CONFIG.C_PROBE34_WIDTH {32} CONFIG.C_PROBE33_WIDTH {32} CONFIG.C_PROBE32_WIDTH {32} CONFIG.C_PROBE31_WIDTH {32} CONFIG.C_PROBE30_WIDTH {32} CONFIG.C_PROBE29_WIDTH {32} CONFIG.C_PROBE28_WIDTH {32} CONFIG.C_PROBE27_WIDTH {32} CONFIG.C_PROBE26_WIDTH {32} CONFIG.C_PROBE25_WIDTH {32} CONFIG.C_PROBE24_WIDTH {9} CONFIG.C_PROBE23_WIDTH {9} CONFIG.C_PROBE22_WIDTH {9} CONFIG.C_PROBE21_WIDTH {9} CONFIG.C_PROBE20_WIDTH {9} CONFIG.C_PROBE19_WIDTH {9} CONFIG.C_PROBE18_WIDTH {9} CONFIG.C_PROBE17_WIDTH {9} CONFIG.C_PROBE16_WIDTH {9} CONFIG.C_PROBE15_WIDTH {9} CONFIG.C_PROBE14_WIDTH {9} CONFIG.C_PROBE13_WIDTH {9} CONFIG.C_PROBE12_WIDTH {9} CONFIG.C_PROBE11_WIDTH {9} CONFIG.C_PROBE10_WIDTH {9} CONFIG.C_PROBE9_WIDTH {9} CONFIG.C_PROBE8_WIDTH {9} CONFIG.C_PROBE7_WIDTH {9} CONFIG.C_PROBE6_WIDTH {9} CONFIG.C_PROBE5_WIDTH {9} CONFIG.C_PROBE4_WIDTH {9} CONFIG.C_PROBE3_WIDTH {9} CONFIG.C_PROBE2_WIDTH {9} CONFIG.C_PROBE1_WIDTH {9} CONFIG.C_NUM_OF_PROBES {49}] [get_ips ila_latency_check]


#*****************************************************************************************
##############################     Generate vio     ######################################
#*****************************************************************************************


create_ip -name vio -vendor xilinx.com -library ip -version 3.0 -module_name vio_uart
set_property -dict [list CONFIG.C_NUM_PROBE_IN {0} CONFIG.C_NUM_PROBE_OUT {2} CONFIG.C_PROBE_OUT0_WIDTH {2} CONFIG.C_PROBE_OUT1_WIDTH {2} CONFIG.C_PROBE_OUT1_INIT_VAL {0x3} CONFIG.C_EN_PROBE_IN_ACTIVITY {0}] [get_ips vio_uart]

create_ip -name vio -vendor xilinx.com -library ip -version 3.0 -module_name vio_Jtag
set_property -dict [list CONFIG.C_NUM_PROBE_IN {3} CONFIG.C_NUM_PROBE_OUT {9} CONFIG.C_PROBE_IN1_WIDTH {256} CONFIG.C_PROBE_IN2_WIDTH {256} CONFIG.C_PROBE_OUT1_WIDTH {12} CONFIG.C_PROBE_OUT2_WIDTH {256} CONFIG.C_PROBE_OUT3_WIDTH {256} CONFIG.C_PROBE_OUT4_WIDTH {9} CONFIG.C_PROBE_OUT5_WIDTH {5} CONFIG.C_PROBE_OUT6_INIT_VAL {0x1}] [get_ips vio_Jtag]

create_ip -name vio -vendor xilinx.com -library ip -version 3.0 -module_name vio_latency_control
set_property -dict [list CONFIG.C_NUM_PROBE_IN {0} CONFIG.C_NUM_PROBE_OUT {5} CONFIG.C_PROBE_OUT0_WIDTH {13} CONFIG.C_PROBE_OUT1_WIDTH {13} CONFIG.C_PROBE_OUT2_WIDTH {32} CONFIG.C_EN_PROBE_IN_ACTIVITY {0}] [get_ips vio_latency_control]

create_ip -name vio -vendor xilinx.com -library ip -version 3.0 -module_name vio_hit_stat_check
set_property -dict [list CONFIG.C_NUM_PROBE_IN {48} CONFIG.C_NUM_PROBE_OUT {0} CONFIG.C_PROBE_IN0_WIDTH {32} CONFIG.C_PROBE_IN1_WIDTH {32} CONFIG.C_PROBE_IN2_WIDTH {32} CONFIG.C_PROBE_IN3_WIDTH {32} CONFIG.C_PROBE_IN4_WIDTH {32} CONFIG.C_PROBE_IN5_WIDTH {32} CONFIG.C_PROBE_IN6_WIDTH {32} CONFIG.C_PROBE_IN7_WIDTH {32} CONFIG.C_PROBE_IN8_WIDTH {32} CONFIG.C_PROBE_IN9_WIDTH {32} CONFIG.C_PROBE_IN10_WIDTH {32} CONFIG.C_PROBE_IN11_WIDTH {32} CONFIG.C_PROBE_IN12_WIDTH {32} CONFIG.C_PROBE_IN13_WIDTH {32} CONFIG.C_PROBE_IN14_WIDTH {32} CONFIG.C_PROBE_IN15_WIDTH {32} CONFIG.C_PROBE_IN16_WIDTH {32} CONFIG.C_PROBE_IN17_WIDTH {32} CONFIG.C_PROBE_IN18_WIDTH {32} CONFIG.C_PROBE_IN19_WIDTH {32} CONFIG.C_PROBE_IN20_WIDTH {32} CONFIG.C_PROBE_IN21_WIDTH {32} CONFIG.C_PROBE_IN22_WIDTH {32} CONFIG.C_PROBE_IN23_WIDTH {32} CONFIG.C_PROBE_IN24_WIDTH {32} CONFIG.C_PROBE_IN25_WIDTH {32} CONFIG.C_PROBE_IN26_WIDTH {32} CONFIG.C_PROBE_IN27_WIDTH {32} CONFIG.C_PROBE_IN28_WIDTH {32} CONFIG.C_PROBE_IN29_WIDTH {32} CONFIG.C_PROBE_IN30_WIDTH {32} CONFIG.C_PROBE_IN31_WIDTH {32} CONFIG.C_PROBE_IN32_WIDTH {32} CONFIG.C_PROBE_IN33_WIDTH {32} CONFIG.C_PROBE_IN34_WIDTH {32} CONFIG.C_PROBE_IN35_WIDTH {32} CONFIG.C_PROBE_IN36_WIDTH {32} CONFIG.C_PROBE_IN37_WIDTH {32} CONFIG.C_PROBE_IN38_WIDTH {32} CONFIG.C_PROBE_IN39_WIDTH {32} CONFIG.C_PROBE_IN40_WIDTH {32} CONFIG.C_PROBE_IN41_WIDTH {32} CONFIG.C_PROBE_IN42_WIDTH {32} CONFIG.C_PROBE_IN43_WIDTH {32} CONFIG.C_PROBE_IN44_WIDTH {32} CONFIG.C_PROBE_IN45_WIDTH {32} CONFIG.C_PROBE_IN46_WIDTH {32} CONFIG.C_PROBE_IN47_WIDTH {32}] [get_ips vio_hit_stat_check]

create_ip -name vio -vendor xilinx.com -library ip -version 3.0 -module_name vio_random_hit
set_property -dict [list CONFIG.C_NUM_PROBE_IN {0} CONFIG.C_NUM_PROBE_OUT {6} CONFIG.C_PROBE_OUT0_WIDTH {25} CONFIG.C_PROBE_OUT1_WIDTH {8} CONFIG.C_PROBE_OUT2_WIDTH {32} CONFIG.C_PROBE_OUT5_WIDTH {5} CONFIG.C_EN_PROBE_IN_ACTIVITY {0}] [get_ips vio_random_hit]

create_ip -name vio -vendor xilinx.com -library ip -version 3.0 -module_name vio_edge_revise


# create_ip -name vio -vendor xilinx.com -library ip -version 3.0 -module_name vio_Jtag_monitor
# set_property -dict [list CONFIG.C_NUM_PROBE_IN {7} CONFIG.C_NUM_PROBE_OUT {0} CONFIG.C_PROBE_IN1_WIDTH {12} CONFIG.C_PROBE_IN2_WIDTH {256} CONFIG.C_PROBE_IN3_WIDTH {256} CONFIG.C_PROBE_IN4_WIDTH {9} CONFIG.C_PROBE_IN5_WIDTH {5}] [get_ips vio_Jtag_monitor]

# create_ip -name vio -vendor xilinx.com -library ip -version 3.0 -module_name ePLL_configration
# set_property -dict [list CONFIG.C_NUM_PROBE_IN {7} CONFIG.C_NUM_PROBE_OUT {0} CONFIG.C_PROBE_IN0_WIDTH {5} CONFIG.C_PROBE_IN1_WIDTH {4} CONFIG.C_PROBE_IN2_WIDTH {4} CONFIG.C_PROBE_IN3_WIDTH {4} CONFIG.C_PROBE_IN4_WIDTH {4} CONFIG.C_PROBE_IN5_WIDTH {4} CONFIG.C_PROBE_IN6_WIDTH {2}] [get_ips ePLL_configration]
source  $origin_dir/tcl/ethernet.tcl
source  $origin_dir/tcl/trigger.tcl
update_compile_order -fileset sources_1