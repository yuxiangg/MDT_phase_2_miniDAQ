#for KC705
set_property CFGBVS VCCO [current_design]
set_property CONFIG_VOLTAGE 2.5 [current_design]

#clock to test_env
set_property IOSTANDARD LVDS_25 [get_ports clk_input_p]
set_property PACKAGE_PIN D17 [get_ports clk_input_p]
set_property PACKAGE_PIN D18 [get_ports clk_input_n]
set_property IOSTANDARD LVDS_25 [get_ports clk_input_n]
set_property DIFF_TERM TRUE [get_ports clk_input_p]
set_property DIFF_TERM TRUE [get_ports clk_input_n]


#USERCLK
set_property PACKAGE_PIN K29 [get_ports USER_CLOCK_N]
set_property IOSTANDARD LVDS_25 [get_ports USER_CLOCK_N]
set_property PACKAGE_PIN K28 [get_ports USER_CLOCK_P]
set_property IOSTANDARD LVDS_25 [get_ports USER_CLOCK_P]





#============================================== JTAG ==============================================
#tck FMC J13
set_property PACKAGE_PIN A13 [get_ports tck]
set_property IOSTANDARD LVCMOS25 [get_ports tck]


#tms FMC K14
set_property PACKAGE_PIN A12 [get_ports tms]
set_property IOSTANDARD LVCMOS25 [get_ports tms]

#tdi FMC J10
set_property PACKAGE_PIN A15 [get_ports tdi]
set_property IOSTANDARD LVCMOS25 [get_ports tdi]

#trst FMC K8
set_property PACKAGE_PIN C11 [get_ports trst]
set_property IOSTANDARD LVCMOS25 [get_ports trst]

#tdo FMC K11
set_property PACKAGE_PIN C14 [get_ports tdo]
set_property IOSTANDARD LVCMOS25 [get_ports tdo]

#============================================== control ==============================================
#encoded_control_in
set_property IOSTANDARD LVDS_25 [get_ports encoded_control_out_p]
set_property PACKAGE_PIN G13 [get_ports encoded_control_out_p]
set_property PACKAGE_PIN F13 [get_ports encoded_control_out_n]
set_property IOSTANDARD LVDS_25 [get_ports encoded_control_out_n]


#reset_in
set_property PACKAGE_PIN B12 [get_ports reset_out]
set_property IOSTANDARD LVCMOS25 [get_ports reset_out]



#=========================================== output  d_line ======================================

#d_line[0]
set_property IOSTANDARD LVDS_25 [get_ports {d_line_p[0]}]
set_property PACKAGE_PIN F21 [get_ports {d_line_p[0]}]
set_property PACKAGE_PIN E21 [get_ports {d_line_n[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {d_line_n[0]}]
set_property DIFF_TERM TRUE [get_ports {d_line_p[0]}]
set_property DIFF_TERM TRUE [get_ports {d_line_n[0]}]

#d_line[1]
set_property IOSTANDARD LVDS_25 [get_ports {d_line_p[1]}]
set_property PACKAGE_PIN F20 [get_ports {d_line_p[1]}]
set_property PACKAGE_PIN E20 [get_ports {d_line_n[1]}]
set_property IOSTANDARD LVDS_25 [get_ports {d_line_n[1]}]
set_property DIFF_TERM TRUE [get_ports {d_line_p[1]}]
set_property DIFF_TERM TRUE [get_ports {d_line_n[1]}]


#============================================ Hit ================================================
set_property PACKAGE_PIN C27 [get_ports {hit[0]}]
set_property PACKAGE_PIN B25 [get_ports {hit[1]}]
set_property PACKAGE_PIN H27 [get_ports {hit[2]}]
set_property PACKAGE_PIN E30 [get_ports {hit[3]}]
set_property PACKAGE_PIN B29 [get_ports {hit[4]}]
set_property PACKAGE_PIN A27 [get_ports {hit[5]}]
set_property PACKAGE_PIN H12 [get_ports {hit[6]}]
set_property PACKAGE_PIN H25 [get_ports {hit[7]}]
set_property PACKAGE_PIN F28 [get_ports {hit[8]}]
set_property PACKAGE_PIN D28 [get_ports {hit[9]}]
set_property PACKAGE_PIN F27 [get_ports {hit[10]}]
set_property PACKAGE_PIN B24 [get_ports {hit[11]}]
set_property PACKAGE_PIN F18 [get_ports {hit[12]}]
set_property PACKAGE_PIN A21 [get_ports {hit[13]}]
set_property PACKAGE_PIN A17 [get_ports {hit[14]}]
set_property PACKAGE_PIN C16 [get_ports {hit[15]}]
set_property PACKAGE_PIN C22 [get_ports {hit[16]}]
set_property PACKAGE_PIN L13 [get_ports {hit[17]}]
set_property PACKAGE_PIN B20 [get_ports {hit[18]}]
set_property PACKAGE_PIN F17 [get_ports {hit[19]}]
set_property PACKAGE_PIN B17 [get_ports {hit[20]}]
set_property PACKAGE_PIN F22 [get_ports {hit[21]}]
set_property PACKAGE_PIN H22 [get_ports {hit[22]}]
set_property PACKAGE_PIN C21 [get_ports {hit[23]}]
set_property IOSTANDARD LVCMOS25 [get_ports hit*]



set_property PACKAGE_PIN C26 [get_ports ASD_TCK]
set_property IOSTANDARD LVCMOS25 [get_ports ASD_TCK]
set_property PACKAGE_PIN F30 [get_ports ASD_shift_out]
set_property IOSTANDARD LVCMOS25 [get_ports ASD_shift_out]
set_property PACKAGE_PIN G30 [get_ports ASD_update_out]
set_property IOSTANDARD LVCMOS25 [get_ports ASD_update_out]
set_property PACKAGE_PIN A30 [get_ports ASD_data_out]
set_property IOSTANDARD LVCMOS25 [get_ports ASD_data_out]
set_property PACKAGE_PIN C30 [get_ports ASD_data_in]
set_property IOSTANDARD LVCMOS25 [get_ports ASD_data_in]


