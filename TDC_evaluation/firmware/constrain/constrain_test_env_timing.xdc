set_false_path -through [get_nets *VIO*]
set_false_path -through [get_nets *vio*]

set_false_path -to [get_pins -hier -filter {NAME =~ *_sync*/D}]
set_false_path -to [get_pins -hier -filter {NAME =~ */*_sync*/D}]