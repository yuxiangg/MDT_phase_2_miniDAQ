`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Michigan
// Engineer: Ly
// 
// Create Date: 08/16/2017 11:55:18 AM
// Design Name: 
// Module Name: UART_interface
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module UART_interface(
	input clk40,
	input reset,
	//UART interface
	input  rxd,
	output txd,
	//command_interface
	input command_fifo_rd_en,
	output [159:0] command,
	output command_fifo_empty,
	//data interface
	input [159:0] data_back,
	input data_back_fifo_wr_en
    );



  wire [7:0] rx_data;
  wire [7:0] tx_data;
  wire rx_ready;
  wire tx_start;
  wire rx_parity_error;



  wire [1:0] parity_mode_VIO,baud_rate_VIO;



  uart_top uart_inst(
                  .clk(clk40),
                  .reset_n(~reset),
                  .parity_mode(parity_mode_VIO),
                  .baud_rate(baud_rate_VIO),
                  .rxd(rxd),
                  .rx_data(rx_data),
                  .rx_ready(rx_ready),
                  .rx_parity_error(rx_parity_error),
                  .tx_data(tx_data),
                  .txd(txd),
                  .tx_start(tx_start),
                  .tx_ready(tx_ready)
                  );
   wire frame_ready;
   wire [159:0] frame_data;                
  frame_build  frame_build_inst(
                       .clk(clk40),
                       .reset_n(~reset),
                       .rx_data(rx_data),
                       .rx_ready(rx_ready),
                       .rx_parity_error(rx_parity_error),
                       .frame_ready(frame_ready),
                       .frame_data(frame_data)
                    );
  wire command_fifo_full;
  fifo_160bit command_fifo_inst (
                      .rst(reset),        // input wire rst
                      .wr_clk(clk40),  // input wire wr_clk
                      .rd_clk(clk40),  // input wire rd_clk
                      .din(frame_data),        // input wire [159 : 0] din
                      .wr_en(frame_ready),    // input wire wr_en
                      .rd_en(command_fifo_rd_en),    // input wire rd_en
                      .dout(command),      // output wire [159 : 0] dout
                      .full(command_fifo_full),      // output wire full
                      .empty(command_fifo_empty)    // output wire empty
                    );                  
                    
  wire [159:0] data_back_out;
  wire data_back_fifo_full,data_back_fifo_empty,data_back_fifo_rd_en;
  fifo_160bit data_back_fifo (
                      .rst(reset),        // input wire rst
                      .wr_clk(clk40),  // input wire wr_clk
                      .rd_clk(clk40),  // input wire rd_clk
                      .din(data_back),        // input wire [159 : 0] din
                      .wr_en(data_back_fifo_wr_en),    // input wire wr_en
                      .rd_en(data_back_fifo_rd_en),    // input wire rd_en
                      .dout(data_back_out),      // output wire [159 : 0] dout
                      .full(data_back_fifo_full),      // output wire full
                      .empty(data_back_fifo_empty)    // output wire empty
                    );                  

  send_back_data send_back_data_inst(
    .clk(clk40),
    .reset(reset),

    .data_back_fifo_empty(data_back_fifo_empty),
    .data_back_fifo_rd(data_back_fifo_rd_en),
    .data_back(data_back_out),
    
    .send_ready(tx_ready),
    .start_send(tx_start),
    .data_send(tx_data)
    );

vio_uart vio_uart_inst (
      .clk(clk40),                // input wire clk
      .probe_out0(parity_mode_VIO),  // output wire [1 : 0] probe_out0
      .probe_out1(baud_rate_VIO)  // output wire [1 : 0] probe_out1
    );

endmodule
