/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : single_edge_data_lock_detector.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on 2018-04-20 13:44:26
//  Note       : 
//     
module single_edge_data_lock_detector(
	input clk,
	input rst,
	input relock,
	input [9:0] correct_value,
	input [9:0] correct_counter_th,
	input  data,
	output locked_out,
	output reg [9:0] data_out,
	output reg data_ready
);

reg locked=1'b0;
assign locked_out = locked;
reg [9:0] shift_reg;
always @(posedge clk  ) begin
	if (rst) begin
		// reset
		shift_reg <= 10'b0;
	end	else begin
		shift_reg <= {shift_reg,data};
	end
end
reg [3:0] shift_counter=4'd0;
always @(posedge clk  ) begin
	if (rst) begin
		// reset
		shift_counter <= 4'd0;
	end	else if (shift_counter==4'd9) begin
		shift_counter <= 4'd0;
	end else begin
		shift_counter <= shift_counter + 4'd1;
	end
end
reg [3:0] start_point=4'b0;
always @(posedge clk  ) begin
	if (rst) begin
		// reset
		start_point <=4'b0; 
	end	else if ((!locked)&(shift_counter==start_point)&(shift_reg!=correct_value)) begin		
		start_point <=start_point < 9 ? (start_point + 4'b1) : 4'b0;
	end
end

always @(posedge clk  ) begin
	if (rst) begin
		// reset
		data_ready <= 1'b0;
	end	else if (shift_counter==start_point) begin
		data_ready <= 1'b1;
	end else begin
		data_ready <= 1'b0;
	end
end


always @(posedge clk  ) begin
	if (rst) begin
		// reset
		data_out <= 10'b0;
	end
	else if (shift_counter==start_point) begin
		data_out <= shift_reg;
	end
end
reg [9:0] correct_counter=10'b0;
always @(posedge clk  ) begin
	if (rst) begin
		// reset
		correct_counter <=10'b0;
	end else if(!locked) begin
		if ((shift_counter==4'd9)&(data_out==correct_value)) begin
			correct_counter <=correct_counter + 1'b1;
		end else if ((shift_counter==4'd9)&(data_out!=correct_value)) begin
			correct_counter <=1'b0;
		end
	end else begin
		correct_counter <=10'b0;
	end
end
always @(posedge clk  ) begin
	if (rst) begin
		// reset
		locked <= 1'b0;
	end	else if (relock) begin
		locked <= 1'b0;
	end else if (correct_counter > correct_counter_th) begin
		locked <= 1'b1;
	end
end

//singl_edge_data_lock_detector_ila your_instance_name (
//	.clk(clk), // input wire clk


//	.probe0(correct_value), // input wire [9:0]  probe0  
//	.probe1(correct_counter_th), // input wire [9:0]  probe1 
//	.probe2(locked), // input wire [0:0]  probe2 
//	.probe3(shift_reg), // input wire [9:0]  probe3 
//	.probe4(shift_counter), // input wire [3:0]  probe4 
//	.probe5(start_point), // input wire [3:0]  probe5 
//	.probe6(data_out), // input wire [9:0]  probe6 
//	.probe7(correct_counter), // input wire [9:0]  probe7 
//	.probe8(data_ready) // input wire [9:0]  probe7 

//);

endmodule