/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : data_selector.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on 2018-04-20 12:18:41
//  Note       : 
//     

module data_selector(
	input clk,
	input rst,
	input reselect,
	input [9:0] correct_value,
	input [9:0] correct_counter_th,
	input [1:0] data,
	output locked,
	output reg edge_select,
	output [9:0]  data_out,
	output data_ready
);

reg selected=1'b0;
assign locked = selected;
wire locked_rising;
wire [9:0] data_out_rising;
wire data_ready_rising;
single_edge_data_lock_detector
single_edge_data_lock_detector_rising(
	.clk(clk),
	.rst(rst),
	.relock(reselect),
	.correct_value(correct_value),
	.correct_counter_th(correct_counter_th),
	.data(data[0]),
	.locked_out(locked_rising),
	.data_out(data_out_rising),
	.data_ready(data_ready_rising)
);

wire locked_faling;
wire [9:0] data_out_faling;
wire data_ready_faling;
single_edge_data_lock_detector
single_edge_data_lock_detector_faling(
	.clk(clk),
	.rst(rst),
	.relock(reselect),
	.correct_value(correct_value),
	.correct_counter_th(correct_counter_th),
	.data(data[1]),
	.locked_out(locked_faling),
	.data_out(data_out_faling),
	.data_ready(data_ready_faling)
);

always @(posedge clk  ) begin
	if (rst) begin
		// reset
		selected <= 1'b0;
	end	else if (reselect) begin
		selected <= 1'b0;
	end else if ((~selected)&(locked_rising|locked_faling)) begin
		selected <= 1'b1;
	end
end
reg selected_r=1'b0;
always @(posedge clk  ) begin
	if (rst) begin
		// reset
		selected_r <= 1'b0;
	end	else begin
	   selected_r <= selected;
	end
end
wire edge_revise_VIO;
vio_edge_revise vio_edge_revise_inst (
  .clk(clk),                // input wire clk
  .probe_in0(edge_select),    // input wire [0 : 0] probe_in0
  .probe_out0(edge_revise_VIO)  // output wire [0 : 0] probe_out0
);

//always @(posedge clk  ) begin
//	if (rst) begin
//		// reset
//		edge_select <= 1'b0;
//	end else if(selected&locked_faling&(~selected_r)) begin
//		edge_select <= ~edge_revise_VIO;
//	end
//end

always @(posedge clk  ) begin
	if (rst|reselect) begin
		// reset
		edge_select <= 1'b0;
	end else if(selected&locked_faling&(~selected_r)) begin
		edge_select <= ~edge_revise_VIO;
	end else if(selected&locked_rising&(~selected_r)) begin
		edge_select <= edge_revise_VIO;
	end
end


assign data_out = edge_select ? data_out_faling : data_out_rising;
assign data_ready = edge_select ? data_ready_faling : data_ready_rising;
endmodule
