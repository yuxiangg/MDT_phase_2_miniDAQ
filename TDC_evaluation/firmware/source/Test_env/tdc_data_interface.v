/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : tdc_data_interface.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on 2018-05-08 13:42:59
//  Note       : 
//     
module tdc_data_interface(
input clk,
input rst,
input trigger,
input K_in_input,
input enable_input,
input [2:0] length,
input [7:0] decode_data_input,
output reg fifo_data_write,
output reg [39:0]  fifo_data,
input o_DErr ,
input o_KErr ,
input o_DpErr
);
reg K_in;
reg [7:0] decode_data;
reg enable;
always @(posedge clk) begin
	K_in <= K_in_input;
	decode_data <= decode_data_input;
	enable <= enable_input;
end

reg k_last = 1'b1;
always @(posedge clk or posedge rst) begin
	if (rst) begin
		// reset
		k_last <= 1'b1;
	end	else if (trigger&enable) begin
		k_last <= K_in;
	end
end


reg [2:0] counter;
always @(posedge clk or posedge rst ) begin
	if (rst) begin
		// reset
		counter <= 3'b0;
	end	else if(enable) begin
		if (~K_in) begin
			if(trigger&k_last)begin
				counter <= length - 3'b010;
			end else begin
				counter <= (counter==length) ? 3'b1 : counter + 3'b1;
			end			
		end else begin
			counter <= 3'b0;
		end
	end 
end
reg [39:0] data;
always @(posedge clk  or posedge rst) begin
	if (rst) begin
		// reset
		data <= 40'b0;
	end	else if(enable&(~K_in))begin
		if(counter!=length)begin
			data <= {data[31:0],decode_data};
		end else begin
			data <= {32'b0,decode_data};
		end
	end 
end

wire trigger_write;
assign trigger_write = enable & ((counter==length)|(~k_last&K_in));

wire triggerless_write;
assign triggerless_write = enable & ((counter==length));


wire fifo_data_write_inner;
assign fifo_data_write_inner = trigger ? trigger_write : triggerless_write;
always @(posedge clk or posedge rst) begin
	if (rst) begin
		// reset
		fifo_data <= 40'b0;
		fifo_data_write <= 1'b0;
	end	else if (fifo_data_write_inner) begin
		fifo_data <= data;
		fifo_data_write <= fifo_data_write_inner;
	end else begin
		fifo_data_write <= 1'b0;
	end
end





tdc_data_interface_ila tdc_data_interface_ila_inst (
	.clk(clk), // input wire clk


	.probe0(K_in), // input wire [0:0]  probe0  
	.probe1(decode_data), // input wire [7:0]  probe1 
	.probe2(enable), // input wire [0:0]  probe2 
	.probe3(fifo_data_write), // input wire [0:0]  probe3 
	.probe4(counter), // input wire [2:0]  probe4 
	.probe5(triggerless_write), // input wire [0:0]  probe5 
	.probe6(trigger_write), // input wire [0:0]  probe6 
	.probe7(fifo_data), // input wire [39:0]  probe7
	.probe8(o_DErr), // input wire [0:0]  probe8
	.probe9(o_KErr), // input wire [0:0]  probe9
	.probe10(o_DpErr) // input wire [0:0]  probe10
);


endmodule
