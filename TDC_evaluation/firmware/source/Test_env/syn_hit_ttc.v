/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : syn_hit_ttc.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on 2018-01-17 11:47:00
//  Note       : 
//  
//create_ip -name vio -vendor xilinx.com -library ip -version 3.0 -module_name vio_syn_hit_ttc
//set_property -dict [list CONFIG.C_NUM_PROBE_IN {0} CONFIG.C_NUM_PROBE_OUT {29} CONFIG.C_PROBE_OUT4_WIDTH {14} CONFIG.C_PROBE_OUT5_WIDTH {14} CONFIG.C_PROBE_OUT6_WIDTH {14} CONFIG.C_PROBE_OUT7_WIDTH {14} CONFIG.C_PROBE_OUT8_WIDTH {14} CONFIG.C_PROBE_OUT9_WIDTH {14} CONFIG.C_PROBE_OUT10_WIDTH {14} CONFIG.C_PROBE_OUT11_WIDTH {14} CONFIG.C_PROBE_OUT12_WIDTH {14} CONFIG.C_PROBE_OUT13_WIDTH {14} CONFIG.C_PROBE_OUT14_WIDTH {14} CONFIG.C_PROBE_OUT15_WIDTH {14} CONFIG.C_PROBE_OUT16_WIDTH {14} CONFIG.C_PROBE_OUT17_WIDTH {14} CONFIG.C_PROBE_OUT18_WIDTH {14} CONFIG.C_PROBE_OUT19_WIDTH {14} CONFIG.C_PROBE_OUT20_WIDTH {14} CONFIG.C_PROBE_OUT21_WIDTH {14} CONFIG.C_PROBE_OUT22_WIDTH {14} CONFIG.C_PROBE_OUT23_WIDTH {14} CONFIG.C_PROBE_OUT24_WIDTH {14} CONFIG.C_PROBE_OUT25_WIDTH {14} CONFIG.C_PROBE_OUT26_WIDTH {14} CONFIG.C_PROBE_OUT27_WIDTH {14} CONFIG.C_PROBE_OUT28_WIDTH {14} CONFIG.C_EN_PROBE_IN_ACTIVITY {0}] [get_ips vio_syn_hit_ttc]

module syn_hit_ttc(
input clk,
input clk_320,
input rst,
output[23:0] hit,
output encocde_ttc,
output use_syn_hit_ttc_VIO,

input delay,

input new_ttc_mode,

input trigger,
input BCR,
input event_reset,
input master_reset,

input [3:0] new_ttc_code
	);

wire  [13:0] hit_0_position_VIO,hit_1_position_VIO,hit_2_position_VIO,hit_3_position_VIO,hit_4_position_VIO,hit_5_position_VIO,hit_6_position_VIO,hit_7_position_VIO;
wire  [13:0] hit_8_position_VIO,hit_9_position_VIO,hit_10_position_VIO,hit_11_position_VIO,hit_12_position_VIO,hit_13_position_VIO,hit_14_position_VIO,hit_15_position_VIO;
wire  [13:0] hit_16_position_VIO,hit_17_position_VIO,hit_18_position_VIO,hit_19_position_VIO,hit_20_position_VIO,hit_21_position_VIO,hit_22_position_VIO,hit_23_position_VIO;

reg [13:0] syn_counter;
always @(posedge clk ) begin
	syn_counter <= syn_counter +14'b1;
end

wire enbale_both_VIO;

wire enable_hit_VIO;
reg [3:0] enable_hit_syn;
reg enable_hit_syn_counter;
reg enable_hit_full;
always @(posedge clk) begin
	enable_hit_syn <= {(~enable_hit_syn[2])&enable_hit_syn[1],enable_hit_syn[1],enable_hit_syn[0],enable_hit_VIO|enbale_both_VIO};
end
always @(posedge clk ) begin
	if(enable_hit_syn[3])begin
		enable_hit_syn_counter <= 1'b1;
	end else if(&syn_counter)  begin
		enable_hit_syn_counter<= 1'b0;
	end
end
always @(posedge clk ) begin
	if(&syn_counter)begin
		enable_hit_full <= enable_hit_syn_counter;
	end
end
	syn_hit_generator inst_syn_hit_generator
		(
			.clk             (clk),
			.enable          (enable_hit_full),
			.global_counter  (syn_counter),
			.hit_0_position  (hit_0_position_VIO ),
			.hit_1_position  (hit_1_position_VIO ),
			.hit_2_position  (hit_2_position_VIO ),
			.hit_3_position  (hit_3_position_VIO ),
			.hit_4_position  (hit_4_position_VIO ),
			.hit_5_position  (hit_5_position_VIO ),
			.hit_6_position  (hit_6_position_VIO ),
			.hit_7_position  (hit_7_position_VIO ),
			.hit_8_position  (hit_8_position_VIO ),
			.hit_9_position  (hit_9_position_VIO ),
			.hit_10_position (hit_10_position_VIO),
			.hit_11_position (hit_11_position_VIO),
			.hit_12_position (hit_12_position_VIO),
			.hit_13_position (hit_13_position_VIO),
			.hit_14_position (hit_14_position_VIO),
			.hit_15_position (hit_15_position_VIO),
			.hit_16_position (hit_16_position_VIO),
			.hit_17_position (hit_17_position_VIO),
			.hit_18_position (hit_18_position_VIO),
			.hit_19_position (hit_19_position_VIO),
			.hit_20_position (hit_20_position_VIO),
			.hit_21_position (hit_21_position_VIO),
			.hit_22_position (hit_22_position_VIO),
			.hit_23_position (hit_23_position_VIO),
			.hit             (hit)
		);

wire enable_ttc_VIO;
reg [3:0] enable_ttc_syn;
reg enable_ttc_syn_counter;
reg enable_ttc_full;
always @(posedge clk) begin
	enable_ttc_syn <= {(~enable_ttc_syn[2])&enable_ttc_syn[1],enable_ttc_syn[1],enable_ttc_syn[0],enable_ttc_VIO|enbale_both_VIO};
end
always @(posedge clk ) begin
	if(enable_ttc_syn[3])begin
		enable_ttc_syn_counter <= 1'b1;
	end else if(&syn_counter)  begin
		enable_ttc_syn_counter<= 1'b0;
	end
end
always @(posedge clk ) begin
	if(&syn_counter)begin
		enable_ttc_full <= enable_ttc_syn_counter;
	end
end
wire  [13:0] ttc_position_VIO;
wire start_single;
assign  start_single = enable_ttc_full&(ttc_position_VIO==syn_counter);
	ttc_generator_module_single inst_ttc_generator_module_single
		(
			.clk           (clk),
			.clk_320       (clk_320),
			.rst           (rst),
			.encocde_ttc   (encocde_ttc),
			.delay         (delay),
			.start_single (start_single),
			.new_ttc_mode (new_ttc_mode),
			.trigger       (trigger),
			.BCR           (BCR),
			.event_reset   (event_reset),
			.master_reset  (master_reset),
			.new_ttc_code  (new_ttc_code)
		);



vio_syn_hit_ttc your_instance_name (
  .clk(clk),                  // input wire clk
  .probe_out0(use_syn_hit_ttc_VIO),    // output wire [0 : 0] probe_out0
  .probe_out1(enbale_both_VIO),    // output wire [0 : 0] probe_out1
  .probe_out2(enable_hit_VIO),    // output wire [0 : 0] probe_out2
  .probe_out3( enable_ttc_VIO ),    // output wire [0 : 0] probe_out3
  .probe_out4( hit_0_position_VIO ),    // output wire [13 : 0] probe_out4
  .probe_out5( hit_1_position_VIO ),    // output wire [13 : 0] probe_out5
  .probe_out6( hit_2_position_VIO ),    // output wire [13 : 0] probe_out6
  .probe_out7( hit_3_position_VIO ),    // output wire [13 : 0] probe_out7
  .probe_out8( hit_4_position_VIO ),    // output wire [13 : 0] probe_out8
  .probe_out9( hit_5_position_VIO ),    // output wire [13 : 0] probe_out9
  .probe_out10(hit_6_position_VIO ),  // output wire [13 : 0] probe_out10
  .probe_out11(hit_7_position_VIO ),  // output wire [13 : 0] probe_out11
  .probe_out12(hit_8_position_VIO ),  // output wire [13 : 0] probe_out12
  .probe_out13(hit_9_position_VIO ),  // output wire [13 : 0] probe_out13
  .probe_out14(hit_10_position_VIO),  // output wire [13 : 0] probe_out14
  .probe_out15(hit_11_position_VIO),  // output wire [13 : 0] probe_out15
  .probe_out16(hit_12_position_VIO),  // output wire [13 : 0] probe_out16
  .probe_out17(hit_13_position_VIO),  // output wire [13 : 0] probe_out17
  .probe_out18(hit_14_position_VIO),  // output wire [13 : 0] probe_out18
  .probe_out19(hit_15_position_VIO),  // output wire [13 : 0] probe_out19
  .probe_out20(hit_16_position_VIO),  // output wire [13 : 0] probe_out20
  .probe_out21(hit_17_position_VIO),  // output wire [13 : 0] probe_out21
  .probe_out22(hit_18_position_VIO),  // output wire [13 : 0] probe_out22
  .probe_out23(hit_19_position_VIO),  // output wire [13 : 0] probe_out23
  .probe_out24(hit_20_position_VIO),  // output wire [13 : 0] probe_out24
  .probe_out25(hit_21_position_VIO),  // output wire [13 : 0] probe_out25
  .probe_out26(hit_22_position_VIO),  // output wire [13 : 0] probe_out26
  .probe_out27(hit_23_position_VIO),  // output wire [13 : 0] probe_out27
  .probe_out28(ttc_position_VIO)  // output wire [13 : 0] probe_out28
);


endmodule