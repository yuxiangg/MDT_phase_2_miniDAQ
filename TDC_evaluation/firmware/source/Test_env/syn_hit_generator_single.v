/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : syn_hit_generator_single.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on 2018-01-17 11:51:00
//  Note       : 
//     
module syn_hit_generator_single(
input clk,
input enable,
input [13:0] global_counter,
input [13:0] hit_position,
output reg hit
	);
always @(posedge clk) begin
	if(enable)begin
        hit <= (global_counter == hit_position);
  end else begin
      hit <= 1'b0;
  end
end
// ila_hit_syn ila_hit_syn_inst (
//   .clk(clk), // input wire clk


//   .probe0(enable), // input wire [0:0]  probe0  
//   .probe1(hit), // input wire [0:0]  probe1 
//   .probe2(global_counter), // input wire [13:0]  probe2 
//   .probe3(hit_position) // input wire [13:0]  probe3
// );



endmodule