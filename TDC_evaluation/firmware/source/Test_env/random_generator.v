`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/06/2017 07:37:38 PM
// Design Name: 
// Module Name: random_generator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`timescale 10ns/1ns

module random_generator(clk,reset,loadseed_i,seed_i,enable,number_o);
input clk;
input reset;
input loadseed_i;
input [31:0] seed_i;
input enable;
output [31:0] number_o;

reg [31:0] number_o;

reg [42:0] LFSR_reg;
reg [36:0] CASR_reg;


//CASR:

always @(posedge clk)
   begin
   if (reset)
      begin
      CASR_reg  <= 37'b1;
      end
   else 
      begin
      if (loadseed_i )
         begin
         CASR_reg  <= {{5{1'b0}},seed_i};
         end
      else if(enable)
         begin
         CASR_reg [36]<=CASR_reg [35]^CASR_reg [0];
         CASR_reg [35]<=CASR_reg [34]^CASR_reg [36];
         CASR_reg [34]<=CASR_reg [33]^CASR_reg [35];
         CASR_reg [33]<=CASR_reg [32]^CASR_reg [34];
         CASR_reg [32]<=CASR_reg [31]^CASR_reg [33];
         CASR_reg [31]<=CASR_reg [30]^CASR_reg [32];
         CASR_reg [30]<=CASR_reg [29]^CASR_reg [31];
         CASR_reg [29]<=CASR_reg [28]^CASR_reg [30];
         CASR_reg [28]<=CASR_reg [27]^CASR_reg [29];
         CASR_reg [27]<=CASR_reg [26]^CASR_reg [27]^CASR_reg [28];
         CASR_reg [26]<=CASR_reg [25]^CASR_reg [27];
         CASR_reg [25]<=CASR_reg [24]^CASR_reg [26];
         CASR_reg [24]<=CASR_reg [23]^CASR_reg [25];
         CASR_reg [23]<=CASR_reg [22]^CASR_reg [24];
         CASR_reg [22]<=CASR_reg [21]^CASR_reg [23];
         CASR_reg [21]<=CASR_reg [20]^CASR_reg [22];
         CASR_reg [20]<=CASR_reg [19]^CASR_reg [21];
         CASR_reg [19]<=CASR_reg [18]^CASR_reg [20];
         CASR_reg [18]<=CASR_reg [17]^CASR_reg [19];
         CASR_reg [17]<=CASR_reg [16]^CASR_reg [18];
         CASR_reg [16]<=CASR_reg [15]^CASR_reg [17];
         CASR_reg [15]<=CASR_reg [14]^CASR_reg [16];
         CASR_reg [14]<=CASR_reg [13]^CASR_reg [15];
         CASR_reg [13]<=CASR_reg [12]^CASR_reg [14];
         CASR_reg [12]<=CASR_reg [11]^CASR_reg [13];
         CASR_reg [11]<=CASR_reg [10]^CASR_reg [12];
         CASR_reg [10]<=CASR_reg [9]^CASR_reg [11];
         CASR_reg [9]<=CASR_reg [8]^CASR_reg [10];
         CASR_reg [8]<=CASR_reg [7]^CASR_reg [9];
         CASR_reg [7]<=CASR_reg [6]^CASR_reg [8];
         CASR_reg [6]<=CASR_reg [5]^CASR_reg [7];
         CASR_reg [5]<=CASR_reg [4]^CASR_reg [6];
         CASR_reg [4]<=CASR_reg [3]^CASR_reg [5];
         CASR_reg [3]<=CASR_reg [2]^CASR_reg [4];
         CASR_reg [2]<=CASR_reg [1]^CASR_reg [3];
         CASR_reg [1]<=CASR_reg [0]^CASR_reg [2];
         CASR_reg [0]<=CASR_reg [36]^CASR_reg [1];
         end
      end
   end
//LFSR:
reg[42:0] LFSR_varLFSR;
wire outbitLFSR;
assign outbitLFSR=LFSR_reg[42];
always @(posedge clk)
   begin
   if (reset )
      begin
      LFSR_reg  <= 43'b1;
      end
   else 
      begin
      if (loadseed_i )
         begin
         LFSR_reg  <= {{11{1'b0}},seed_i};
         end
      else if(enable)
         begin
         LFSR_reg [42]<=LFSR_reg [41];
         LFSR_reg [41]<=LFSR_reg [40]^outbitLFSR ;
         LFSR_reg [40]<=LFSR_reg [39];
         LFSR_reg [39]<=LFSR_reg [38];
         LFSR_reg [38]<=LFSR_reg [37];
         LFSR_reg [37]<=LFSR_reg [36];
         LFSR_reg [36]<=LFSR_reg [35];
         LFSR_reg [35]<=LFSR_reg [34];
         LFSR_reg [34]<=LFSR_reg [33];
         LFSR_reg [33]<=LFSR_reg [32];
         LFSR_reg [32]<=LFSR_reg [31];
         LFSR_reg [31]<=LFSR_reg [30];
         LFSR_reg [30]<=LFSR_reg [29];
         LFSR_reg [29]<=LFSR_reg [28];
         LFSR_reg [28]<=LFSR_reg [27];
         LFSR_reg [27]<=LFSR_reg [26];
         LFSR_reg [26]<=LFSR_reg [25];
         LFSR_reg [25]<=LFSR_reg [24];
         LFSR_reg [24]<=LFSR_reg [23];
         LFSR_reg [23]<=LFSR_reg [22];
         LFSR_reg [22]<=LFSR_reg [21];
         LFSR_reg [21]<=LFSR_reg [20];
         LFSR_reg [20]<=LFSR_reg [19]^outbitLFSR ;
         LFSR_reg [19]<=LFSR_reg [18];
         LFSR_reg [18]<=LFSR_reg [17];
         LFSR_reg [17]<=LFSR_reg [16];
         LFSR_reg [16]<=LFSR_reg [15];
         LFSR_reg [15]<=LFSR_reg [14];
         LFSR_reg [14]<=LFSR_reg [13];
         LFSR_reg [13]<=LFSR_reg [12];
         LFSR_reg [12]<=LFSR_reg [11];
         LFSR_reg [11]<=LFSR_reg [10];
         LFSR_reg [10]<=LFSR_reg [9];
         LFSR_reg [9]<=LFSR_reg [8];
         LFSR_reg [8]<=LFSR_reg [7];
         LFSR_reg [7]<=LFSR_reg [6];
         LFSR_reg [6]<=LFSR_reg [5];
         LFSR_reg [5]<=LFSR_reg [4];
         LFSR_reg [4]<=LFSR_reg [3];
         LFSR_reg [3]<=LFSR_reg [2];
         LFSR_reg [2]<=LFSR_reg [1];
         LFSR_reg [1]<=LFSR_reg [0]^outbitLFSR ;
         LFSR_reg [0]<=LFSR_reg [42];
         end
      end
   end
//combinate:
always @(posedge clk)
   begin
   if (reset )
      begin
      number_o  <= 32'b0;
      end
   else if(enable)
      begin
      number_o  <= (LFSR_reg [31:0]^CASR_reg[31:0]);
      end
   end


//ila_3 your_instance_name (
//	.clk(clk), // input wire clk


//	.probe0(reset), // input wire [0:0]  probe0  
//	.probe1(enable), // input wire [0:0]  probe1 
//	.probe2(loadseed_i), // input wire [0:0]  probe2 
//	.probe3(1'b0), // input wire [0:0]  probe3 
//	.probe4(number_o), // input wire [31:0]  probe4 
//	.probe5(seed_i), // input wire [31:0]  probe5 
//	.probe6(LFSR_reg), // input wire [42:0]  probe6 
//	.probe7(CASR_reg) // input wire [36:0]  probe7
//);

endmodule
