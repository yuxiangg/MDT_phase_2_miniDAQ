/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : SPI_master.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on 2018-04-19 13:49:37
//  Note       : 
//     
module SPI_master(
    input clk,
    output TCK,
    output TMS,
    output TDI,
    input TDO,
    input start_action,
    input [11:0] config_period,
    input [511:0] SPI_bits,
    input [8:0] bit_length,
    output reg [511:0] SPI_data_out,
    output reg SPI_busy
    );

  reg [11:0] counter= 12'b0;
  wire tick;
  assign  tick = (counter == config_period);
  reg start_action_syn;
  reg start_action_r=1'b0;
  
  always @(posedge clk) begin
    start_action_r <= start_action;
  end  
  always @(posedge clk) begin
    if ((~start_action_r)&start_action) begin
       start_action_syn <= 1'b1;
    end else if(start_action_syn&tick&TCK) begin
       start_action_syn <= 1'b0;
    end
  end  


    always @(posedge clk)
    begin
        if(counter != config_period) 
           counter <= counter + 12'b1;
        else 
           counter <= 12'b0;  
   end

   
   
   reg TCK_reg=1'b0;
   assign TCK = TCK_reg;
   always @(posedge clk)
   begin
    if(tick)
        TCK_reg <= ~TCK_reg;
  end

  reg [8:0] bit_counter=9'b0;
  reg TMS_reg=1'b0;
  assign TMS = TMS_reg;
  reg [511:0] SPI_bits_reg= 512'b0;
  assign  TDI = SPI_bits_reg[0];
  always @(posedge clk)
  begin 
    if(tick&TCK)begin
        if(start_action_syn)begin
            bit_counter <= bit_length;
            TMS_reg <= 1'b1;
            SPI_busy <= 1'b1;
            SPI_bits_reg <= SPI_bits;
        end else if(|bit_counter) begin
            SPI_busy <= 1'b1;
            SPI_bits_reg <= {SPI_bits_reg[0],SPI_bits_reg[511:1]};
            bit_counter <= bit_counter - 1'b1;
        end else begin
            SPI_busy <= 1'b0;
            bit_counter <= 'b0;
            TMS_reg <= 1'b0; 
        end
    end
 end
always @(posedge clk) begin
  if(tick&TCK&start_action_syn)begin
    SPI_data_out <= 512'b0;
  end else if(tick&~TCK)begin
    if (TMS_reg) begin
      SPI_data_out <= {TDO,SPI_data_out[511:1]};
    end
  end
end
endmodule
