/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : command_resolve_jtag.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on 2018-04-12 18:55:33
//  Note       : 
//     
module command_resolve(
    input clk,
    input reset,

    input [159:0] command,
    input command_fifo_empty,
    output reg command_fifo_rd,

    output reg [159:0] data_back,
    output reg data_back_fifo_write,

    output reg trst_from_uart,

    output reg start_action,
    output reg [11:0]  config_period,
    output [511:0] jtag_bits,
    output reg [8:0]   bit_length,
    output reg [4:0]   JTAG_inst,

    input [511:0] JTAG_data,
    input JTAG_busy,

    // input [39:0] tdc_data_fifo,
    // input tdc_fifo_empty,
    // output reg tdc_fifo_read,
    // output reg tdc_fifo_reset,

	output reg tdc_master_reset,
	output reg high_speed, 
	output reg [2:0] length, 
	output reg trigger_mode, 

	output reg [9:0] correct_value_0,
	output reg [9:0] correct_value_1,
	output reg [9:0] correct_counter_th,
	output reg reselect,
	input locked_0,
	input locked_1,
	input edge_select_0,
	input edge_select_1,


	output reg [11:0] width_hit,
	output reg start_hit,
	output reg inv_hit,
	output reg [23:0] hit_mask_hit,
	output reg delay_hit,
	output reg [11:0] interval_hit,
	output reg start_single_hit,
	
	output reg statrt_single_ttc,
	output reg trigger_ttc,
	output reg BCR_ttc,
	output reg event_reset_ttc,
	output reg master_reset_ttc,
	output reg [3:0] new_ttc_code,
	output reg [11:0] roll_over_ttc,
	output reg new_ttc_mode,
	output reg enable_bcr_ttc,
	output reg start_ttc,
	output reg [11:0] interval_ttc,
	output reg delay_ttc

 //    input [26:0] ePLL_config_reg,
	// output reg  ePLL_changed_clear

    );
    
reg [127:0]  jtag_bits_0=128'b0;
reg [127:0]  jtag_bits_1=128'b0;
reg [127:0]  jtag_bits_2=128'b0;
reg [127:0]  jtag_bits_3=128'b0;
assign jtag_bits = {jtag_bits_3,jtag_bits_2,jtag_bits_1,jtag_bits_0};

wire [127:0]  jtag_data_0; assign jtag_data_0 = JTAG_data[127:  0];
wire [127:0]  jtag_data_1; assign jtag_data_1 = JTAG_data[255:128];
wire [127:0]  jtag_data_2; assign jtag_data_2 = JTAG_data[383:256];
wire [127:0]  jtag_data_3; assign jtag_data_3 = JTAG_data[511:384];

reg [159:0] reg_data;

reg [2:0] state;
reg [159:0]  command_r;
localparam IDLE=3'b000;
localparam RESOLVE_COMMAND=3'b001;
localparam COMMAND_UPDATE=3'b010;
localparam COMMAND_32BIT =3'b011;
localparam SEND_OUT_DATA =3'b110;

always @(posedge clk) begin
	if (reset) begin
		state <= IDLE;
		command_fifo_rd <= 1'b0;
		data_back_fifo_write<= 1'b0;
		//trst_from_uart <= 1'b0;
		//tdc_fifo_read <= 1'b0;
		//tdc_fifo_reset <= 1'b0;
		//ePLL_changed_clear <= 1'b0;
	end else begin
		command_fifo_rd <= 1'b0;
		data_back_fifo_write <= 1'b0;
		//tdc_fifo_read <= 1'b0;
		//ePLL_changed_clear <= 1'b0;
		case (state)
			IDLE:begin
			state <= IDLE;
			if(~command_fifo_empty)begin
				command_fifo_rd <= 1'b1;
				command_r <= command;
				state <= RESOLVE_COMMAND;
			end
			end
			RESOLVE_COMMAND:begin
			if (command_r[159]) begin
				state <= COMMAND_UPDATE;
			end else if (command_r[31])begin
				state <= COMMAND_32BIT;
			end else begin
				state <= IDLE;
			end
			end	
			COMMAND_UPDATE: begin
			state <= IDLE;
			case(command_r[143:136])
				8'b0000_0000:jtag_bits_0   <= command_r[127:0];
				8'b0000_0001:jtag_bits_1   <= command_r[127:0];
				8'b0000_0010:jtag_bits_2   <= command_r[127:0];
				8'b0000_0011:jtag_bits_3   <= command_r[127:0];
				8'b0000_0100:{correct_value_0,correct_value_1,correct_counter_th} <= command_r[29:0];
				default: begin end
			endcase	
			end
			COMMAND_32BIT: begin
			state <= IDLE;
			case(command_r[23:20])
				4'b0000: begin					
					case(command_r[19:16])
						4'b0000:start_action  		<= command_r[0];
						4'b0001:config_period 		<= command_r[11:0];
						4'b0010:bit_length    		<= command_r[8:0];
						4'b0011:JTAG_inst     		<= command_r[4:0];
						4'b0100:trst_from_uart 		<= command_r[0];
						//4'b1000:tdc_fifo_reset 		<= command_r[0]; 
						4'b1001:tdc_master_reset 	<= command_r[0]; 
						4'b1010:high_speed 			<= command_r[0]; 
						4'b1011:length 				<= command_r[2:0];
						4'b1100:trigger_mode 		<= command_r[0];
						4'b1101:reselect 			<= command_r[0];
						default: begin end
					endcase									
				end
				4'b0001: begin
					state <= SEND_OUT_DATA; 
					case(command_r[19:16])
						4'b0000:reg_data <= {8'b1111_1111,20'b0,command_r[19:16],jtag_data_0};
						4'b0001:reg_data <= {8'b1111_1111,20'b0,command_r[19:16],jtag_data_1};
						4'b0010:reg_data <= {8'b1111_1111,20'b0,command_r[19:16],jtag_data_2};
						4'b0011:reg_data <= {8'b1111_1111,20'b0,command_r[19:16],jtag_data_3};
						//4'b0100:begin 
						//	reg_data <= {8'b1111_1111,20'b0,command_r[19:16],101'b0,ePLL_config_reg}; 
						//	ePLL_changed_clear <= 1'b1; end
						4'b0101:begin
							reg_data <= {8'b1111_1111,20'b0,command_r[19:16],124'b0,{locked_0,locked_1,edge_select_0,edge_select_1}}; end
						//4'b1000:begin
						//	reg_data <= tdc_fifo_empty ? {8'b1111_1111,20'b0,command_r[19:16],{88{1'b1}},40'b0} : {8'b1111_1111,20'b0,command_r[19:16],88'b0,tdc_data_fifo};
						//	tdc_fifo_read <= 1'b1; end
						default:reg_data <= {8'b1111_1111,20'b0,command_r[19:16],127'b0,JTAG_busy};
					endcase
				end
				4'b0010: begin
					case(command_r[19:16])
						4'b0000:width_hit 			<= command_r[11:0];
						4'b0001:start_hit 			<= command_r[0];
						4'b0010:inv_hit 			<= command_r[0];
						4'b0011:hit_mask_hit[11:0] 	<= command_r[11:0];
						4'b0100:hit_mask_hit[23:12] <= command_r[11:0];
						4'b0101:delay_hit			<= command_r[0];
						4'b0110:interval_hit 		<= command_r[11:0];
						4'b0111:start_single_hit 	<= command_r[0];
					endcase
				end
				4'b0011: begin
					case(command_r[19:16])
						4'b0000:statrt_single_ttc 	<= command_r[0];
						4'b0001:trigger_ttc			<= command_r[0];
						4'b0010:BCR_ttc 			<= command_r[0];
						4'b0011:event_reset_ttc 	<= command_r[0];
						4'b0100:master_reset_ttc 	<= command_r[0];
						4'b0101:new_ttc_mode 		<= command_r[0];
						4'b0110:new_ttc_code 		<= command_r[3:0];
						4'b0111:delay_ttc           <= command_r[0];
					endcase
				end
				4'b0100: begin
					case(command_r[19:16])
						4'b0000:roll_over_ttc 		<= command_r[11:0];
						4'b0001:enable_bcr_ttc		<= command_r[0];
						4'b0010:interval_ttc 		<= command_r[11:0];
						4'b0011:start_ttc 			<= command_r[0];
					endcase
				end
			endcase
			end
			SEND_OUT_DATA:begin
			state <= IDLE;
			data_back <=reg_data;
			data_back_fifo_write <= 1'b1;
			end
			default:begin state <= IDLE; end
		endcase
	end
end

endmodule