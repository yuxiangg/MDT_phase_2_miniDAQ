`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/09/2017 02:24:01 PM
// Design Name: 
// Module Name: hit_recieve_latency_statistic
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module hit_recieve_latency_statistic(
input clk_hit,
input clk_recieve,
input clear_hit,
input clear_recive,
input data_valid,
input [23:0] hit,
input [4:0] interest_channel,
input [4:0] channel_number,
input rising_edge,
input [14:0] coarse_counter,


input [12:0]coarse_counter_limit_down,
input [12:0] coarse_counter_limit_up,

// input [31:0] interest_latency,
output hit_chosen_out,
output [31:0] time_latency_out
);


     wire[23:0] hit_mask;
    assign hit_mask = 1 << interest_channel;
    reg [2:0] hit_reg=3'b0;
    wire hit_inner;
    assign hit_inner=|(hit_mask&hit);
    always @(posedge  clk_hit)begin
        hit_reg<={hit_reg[1:0],hit_inner};
    end

  

reg [13:0] coarse_counter_ref=13'b0;
wire [12:0] coarse_counter_ref_syn_hit;
always@(posedge clk_recieve)begin
    coarse_counter_ref<=coarse_counter_ref+13'b1;
end

  

 
reg [31:0] time_tag=16'b0;
always@(posedge  clk_hit) begin
if(clear_hit) time_tag<=32'b0;
else 
   time_tag <= time_tag +32'b1;
end   


wire sync_full;
wire sync_empty;
fifo_sync_coarse_counter fifo_sync_coarse_counter_inst(
 .wr_clk(clk_recieve),  // input wire wr_clk
 .rd_clk(clk_hit),  // input wire rd_clk
 .din(coarse_counter_ref[13:1]),        // input wire [12 : 0] din
 .wr_en(1'b1),    // input wire wr_en
 .rd_en(1'b1),    // input wire rd_en
 .dout(coarse_counter_ref_syn_hit),      // output wire [12 : 0] dout
 .full(sync_full),      // output wire full
 .empty(sync_empty)    // output wire empty
);
wire [31:0] time_tag_hit;
wire [12:0] coarse_counter_hit;
wire hit_fifo_full,hit_fifo_empty;
reg hit_fifo_rd_en=1'b0;
fifo_latency_hit fifo_latency_hit_inst (
  .clk(clk_hit),      // input wire clk
  .rst(clear_hit),      // input wire rst
  .din({time_tag,coarse_counter_ref_syn_hit}),      // input wire [44 : 0] din
  .wr_en((~hit_inner)&(hit_reg[0])),  // input wire wr_en
  .rd_en(hit_fifo_rd_en),  // input wire rd_en
  .dout({time_tag_hit,coarse_counter_hit}),    // output wire [44 : 0] dout
  .full(hit_fifo_full),    // output wire full
  .empty(hit_fifo_empty)  // output wire empty
);




wire sync_full_2;
wire sync_empty_2;
wire data_valid_hit;
wire rising_edge_hit;
wire [4:0] channel_number_hit;
wire [14:0] coarse_counter_data_hit;

fifo_sync_hit_info fifo_sync_hit_info_inst (
    .wr_clk(clk_recieve),  // input wire wr_clk
    .rd_clk(clk_hit),  // input wire rd_clk
    .din({data_valid,rising_edge,channel_number,coarse_counter}),        // input wire [21 : 0] din
    .wr_en(1'b1),    // input wire wr_en
    .rd_en(1'b1),    // input wire rd_en
    .dout({data_valid_hit,rising_edge_hit,channel_number_hit,coarse_counter_data_hit}),      // output wire [21 : 0] dout
    .full(sync_full_2),      // output wire full
    .empty(sync_empty_2)    // output wire empty
);
reg data_valid_hit_r=1'b0;
always@(posedge clk_hit) begin
    data_valid_hit_r<=data_valid_hit;
end
reg data_fifo_rd=1'b0;
wire data_fifo_full,data_fifo_empty;
wire [31:0] time_tag_data;
wire [14:0] coarse_counter_data;
wire data_fifo_wr_en;
assign data_fifo_wr_en=(~data_valid_hit_r)&data_valid_hit&rising_edge_hit&(channel_number_hit==interest_channel);
fifo_latency_data fifo_latency_data_inst (
  .clk(clk_hit),      // input wire clk
  .rst(clear_hit),    // input wire srst
  .din({time_tag,coarse_counter_data_hit}),      // input wire [46 : 0] din
  .wr_en(data_fifo_wr_en),  // input wire wr_en
  .rd_en(data_fifo_rd),  // input wire rd_en
  .dout({time_tag_data,coarse_counter_data}),    // output wire [46 : 0] dout
  .full(data_fifo_full),    // output wire full
  .empty(data_fifo_empty)  // output wire empty
);



reg [12:0] coarse_counter_diff;

reg [12:0] coarse_counter_hit_r;
reg  [31:0] time_tag_hit_r;


reg [31:0] time_tag_data_r;
reg [14:0] coarse_counter_data_r;

reg [31:0] time_latency=32'b0;

reg [2:0] state=3'b000;
reg [2:0] nextstate;
localparam IDLE=3'b000;
localparam GET_DATA=3'b001;
localparam CALCULATION =3'b011;
localparam CHOSEN_HIT=3'b010;
localparam WAIT_FIFO_1=3'b110;
localparam WAIT_FIFO_2=3'b100;
localparam FINISH_STATE=3'b101;
reg hit_chosen=1'b0;




always@(posedge clk_hit)begin
    if(clear_hit) state<= IDLE;
    else state<=nextstate;
end
always@(*)begin

    case(state)
    IDLE:nextstate = data_fifo_empty?IDLE:GET_DATA;
    GET_DATA:nextstate = CALCULATION;
    CALCULATION:nextstate = CHOSEN_HIT;
    CHOSEN_HIT:  nextstate = (hit_chosen||hit_fifo_empty) ? FINISH_STATE :WAIT_FIFO_1;
    WAIT_FIFO_1: nextstate = WAIT_FIFO_2;
    WAIT_FIFO_2: nextstate = CALCULATION;
    FINISH_STATE: nextstate= IDLE;
    default : nextstate= IDLE;
    endcase
end
always@(posedge clk_hit)begin
    if(clear_hit) begin 
        hit_chosen<=1'b0;
        time_latency<=32'b0;
        data_fifo_rd<=1'b0;
        hit_fifo_rd_en<=1'b0;
        time_tag_data_r<=32'b0;
        coarse_counter_data_r<=15'b0;
        time_tag_hit_r<=32'b0;
        coarse_counter_hit_r<=13'b0;
        coarse_counter_diff<=13'b0;
    end else begin
       hit_chosen<=1'b0;
       data_fifo_rd<=1'b0;
       hit_fifo_rd_en<=1'b0;
       case( nextstate)
       IDLE:begin end
       GET_DATA: begin
            
            data_fifo_rd<=1'b1;
            time_tag_data_r<=time_tag_data;
            coarse_counter_data_r<=coarse_counter_data;

            coarse_counter_hit_r<=coarse_counter_hit;
            time_tag_hit_r<=time_tag_hit;
       end
       CALCULATION:begin
            coarse_counter_diff<=coarse_counter_data_r[14:2]-coarse_counter_hit_r;
       end
       CHOSEN_HIT:begin
          hit_fifo_rd_en<=~hit_fifo_empty;
           if(((coarse_counter_diff>coarse_counter_limit_down)&(coarse_counter_diff<coarse_counter_limit_up)))begin
              hit_chosen<=1'b1; 
           end
      end
      WAIT_FIFO_1:begin end
      WAIT_FIFO_2:begin 
        coarse_counter_hit_r<=coarse_counter_hit; 
        time_tag_hit_r<=time_tag_hit;
      end
      FINISH_STATE:begin
           time_latency<=time_tag_data_r-time_tag_hit_r;
      end
      endcase
 end
 end
             
reg hit_chosen_r;
always@(posedge clk_hit)begin
  hit_chosen_r<=hit_chosen;
end

reg [31:0] latency_min;
reg [31:0] latency_max;
always@(posedge clk_hit)begin
  if(clear_hit)begin
    latency_min<=32'hffffffff;
    latency_max<=32'b0;
  end else if(hit_chosen_r)begin
    latency_min<=(time_latency<latency_min) ? time_latency : latency_min;
    latency_max<=(time_latency>latency_max) ? time_latency : latency_max;
  end
end

// reg [31:0] latency_number;
// always@(posedge clk_hit)begin
//   if(clear_hit)begin
//     latency_number <= 32'b0;
//   end else begin
//     if(hit_chosen_r&(time_latency==interest_latency))begin
//       latency_number <= latency_number + 32'b1; 
//     end
//   end
// end


// ila_latecny_hit_receive ila_latecny_hit_receive_inst (
//   .clk(clk_hit), // input wire clk


//   .probe0(time_tag), // input wire [31:0]  probe0  
//   .probe1(0), // input wire [31:0]  probe1 
//   .probe2(hit_reg), // input wire [2:0]  probe2 
//   .probe3(hit_inner), // input wire [0:0]  probe3 
//   .probe4(coarse_counter_ref_syn_hit), // input wire [12:0]  probe4 
//   .probe5(coarse_counter_hit), // input wire [12:0]  probe5 
//   .probe6(time_tag_hit), // input wire [31:0]  probe6 
//   .probe7(hit_fifo_rd_en), // input wire [0:0]  probe7
//   .probe8(hit_fifo_full),// input wire [0:0]  probe8
//   .probe9(hit_fifo_empty),// input wire [0:0]  probe9
//   .probe10(data_valid_hit),// input wire [0:0]  probe10
//   .probe11(rising_edge_hit), // input wire [0:0]  probe11
//   .probe12(channel_number_hit),// input wire [4:0]  probe12
//   .probe13(coarse_counter_data_hit),// input wire [14:0]  probe13
//   .probe14(data_fifo_wr_en),// input wire [0:0]  probe14
//   .probe15(data_fifo_rd),// input wire [0:0]  probe15
//   .probe16(time_tag_data), // input wire [31:0]  probe16
//   .probe17(coarse_counter_data),// input wire [14:0]  probe17
//   .probe18(data_fifo_full),// input wire [0:0]  probe18
//   .probe19(data_fifo_empty),// input wire [0:0]  probe19
//   .probe20(state),// input wire [1:0]  probe20
//   .probe21(nextstate),// input wire [1:0]  probe21
//   .probe22(hit_chosen), // input wire [0:0]  probe22
//   .probe23(time_tag_data_r),// input wire [31:0]  probe23
//   .probe24(coarse_counter_data_r),// input wire [14:0]  probe24
//   .probe25(time_latency),// input wire [31:0]  probe25 
//   .probe26(coarse_counter_diff),// input wire [12:0]  probe26
//   .probe27(hit),// input wire [11:0]  probe27  
//   .probe28(hit_mask),// input wire [11:0]  probe28 
//   .probe29(hit_inner), // input wire [0:0]  probe29
//   .probe30(latency_min),// input wire [31:0]  probe30
//   .probe31(latency_max),// input wire [31:0]  probe31  
//   .probe32(0)// input wire [31:0]  probe32 
//   // .probe33(latency_number) // input wire [31:0]  probe33
// );

assign hit_chosen_out = hit_chosen;
assign time_latency_out = time_latency;

endmodule
