/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : input_deserial.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on 2018-04-23 10:16:30
//  Note       : 
//     
module input_deserial(
input clk,
input rst,
input data_even,
input data_odd,
input high_speed,
output raw_data_fifo_write,
output reg [9:0] data
);

reg tick=1'b1;
always @(posedge clk  ) begin
	if (rst) begin
		// reset
		tick <=1'b1;
	end	else if (~high_speed) begin
		tick <= ~tick;
	end else begin
		tick <=1'b1;
	end
end
assign raw_data_fifo_write = tick;

reg [9:0] data_shift=10'b0;
always @(posedge clk  ) begin
	if (rst) begin
		// reset
		data_shift <=10'b0;
	end	else if(tick) begin
		data_shift <= {data_shift[7:0],data_odd,data_even};
	end
end

reg [2:0] data_shift_counter=3'b0;
always @(posedge clk  ) begin
	if (rst) begin
		// reset
		data_shift_counter <= 3'b0;
	end	else if(tick) begin
		if (data_shift_counter==3'b100) begin
			data_shift_counter <= 3'b0;
		end else begin
			data_shift_counter <= data_shift_counter + 3'b1;
		end
	end
end

reg [2:0] data_update_position=3'b0;
always @(posedge clk  ) begin
	if (rst) begin
		// reset
		data_update_position <=3'b0;
	end	else if (tick&(data_shift[5:1]==5'b11111)||(data_shift[5:1]==5'b00000)) begin
		data_update_position <= (data_shift_counter==3'b100) ? 3'b000 : (data_shift_counter + 3'b1);
	end
end

always @(posedge clk  ) begin
	if (rst) begin
		// reset
		data <= 10'b0;
	end	else if (tick&(data_shift_counter == data_update_position)) begin
		data <= data_shift;
	end
end

//input_deserial_ila input_deserial_ila_inst (
//	.clk(clk), // input wire clk


//	.probe0(data_even), // input wire [0:0]  probe0  
//	.probe1(data_odd), // input wire [0:0]  probe1 
//	.probe2(data_shift), // input wire [9:0]  probe2 
//	.probe3(data), // input wire [9:0]  probe3 
//	.probe4(data_shift_counter), // input wire [2:0]  probe4 
//	.probe5(data_update_position), // input wire [2:0]  probe5
//	.probe6(tick) // input wire [0:0]  probe6
//);


endmodule