/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : Test_env_top_trigger_test.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on 2018-04-11 14:34:03
//  Note       : 
//     

module Test_env_top_trigger_test(

input clk_input_p,
input clk_input_n,

input USER_CLOCK_P,
input USER_CLOCK_N,

input rxd,
output txd,

output tck,
output tms,
output tdi,
output trst,
input tdo,

input ASD_TCK,
input ASD_shift_out,
input ASD_update_out,
input ASD_data_out,
output  ASD_data_in,

output [23:0] hit,

output encoded_control_out_p,
output encoded_control_out_n,

output reset_out,

input [1:0] d_line_p,
input [1:0] d_line_n
    );



wire clk40_pre, clk160_pre, clk320_pre,clk64_pre;
input_clock 
    input_clock_inst(
        // Clock in ports
        .clk_in1_p(clk_input_p),  // input clk_in1_p
        .clk_in1_n(clk_input_n),  // input clk_in1_n
        // Clock out ports
        .clk_out1(clk40_pre),     // output clk_out1
        .clk_out2(clk160_pre),    // output clk_out2
        .clk_out3(clk320_pre),     // output clk_out3
        .clk_out4(clk64_pre)     // output clk_out3
    );    


wire clk_hit;
wire [23:0] hit_detect;

clk_hit clk_hit_inst
   (
   // Clock in ports
    .clk_in1_p(USER_CLOCK_P),    // input clk_in1_p
    .clk_in1_n(USER_CLOCK_N),    // input clk_in1_n
    // Clock out ports
    .clk_out1(clk_hit));    // output clk_out1

wire encoded_control;
wire reset_out_UART;
jtag_ila jtag_ila_inst (
	.clk(clk160_pre), // input wire clk


	.probe0(tck), // input wire [0:0]  probe0  
	.probe1(tms), // input wire [0:0]  probe1 
	.probe2(tdi), // input wire [0:0]  probe2 
	.probe3(trst), // input wire [0:0]  probe3 
	.probe4(tdo) // input wire [0:0]  probe4
);

io_monitor_ila io_monitor_inst (
	.clk(clk160_pre), // input wire clk
	
	.probe0(hit_detect), // input wire [23:0]  probe0  
	.probe1(encoded_control), // input wire [0:0]  probe1 
	.probe2(reset_out) // input wire [0:0]  probe2 
);

wire [1:0] d_line;
   IBUFDS #(
      .DIFF_TERM("FALSE"),       // Differential Termination
      .IBUF_LOW_PWR("TRUE"),     // Low power="TRUE", Highest performance="FALSE" 
      .IOSTANDARD("DEFAULT")     // Specify the input I/O standard
   ) IBUFDS_d_line_0 (
      .O(d_line[0]),  // Buffer output
      .I(d_line_p[0]),  // Diff_p buffer input (connect directly to top-level port)
      .IB(d_line_n[0]) // Diff_n buffer input (connect directly to top-level port)
   );

    IBUFDS #(
      .DIFF_TERM("FALSE"),       // Differential Termination
      .IBUF_LOW_PWR("TRUE"),     // Low power="TRUE", Highest performance="FALSE" 
      .IOSTANDARD("DEFAULT")     // Specify the input I/O standard
   ) IBUFDS_d_line_1 (
      .O(d_line[1]),  // Buffer output
      .I(d_line_p[1]),  // Diff_p buffer input (connect directly to top-level port)
      .IB(d_line_n[1]) // Diff_n buffer input (connect directly to top-level port)
   );




OBUFDS #(.IOSTANDARD("DEFAULT"), // Specify the output I/O standard
 .SLEW("SLOW")           // Specify the output slew rate
 )
    OBUFDS_inst (
        .O (encoded_control_out_p),     // Diff_p output (connect directly to top-level port)
        .OB(encoded_control_out_n),   // Diff_n output (connect directly to top-level port)
        .I(encoded_control)      // Buffer input 
    );

assign ASD_data_in             =1'b0;
ASD_config_ila ASD_config_inst (
	.clk(clk320_pre), // input wire clk
	.probe0(ASD_TCK), // input wire [0:0]  probe0  
	.probe1(ASD_shift_out), // input wire [0:0]  probe1 
	.probe2(ASD_update_out), // input wire [0:0]  probe2 
	.probe3(ASD_data_out) // input wire [0:0]  probe3
);




wire trigger_VIO;
wire high_speed_VIO;
wire [2:0] length_VIO;
wire reset_tdc_data_fifo;



wire JTAG_busy;
wire [511:0] JTAG_data_out;
wire start_action;
wire [11:0] config_period;
wire [511:0] JTAG_bits;
wire [8:0] bit_length;
wire [4:0] JTAG_inst;

JTAG_master 
    JTAG_master_inst(
        .clk (clk40_pre),
        .TCK (tck_jtag),
        .TMS (tms_jtag),
        .TDI (tdi_jtag),
        .TDO (tdo),
        .start_action  (start_action),
        .config_period (config_period),
        .JTAG_bits     (JTAG_bits),
        .bit_length    (bit_length),
        .JTAG_inst     (JTAG_inst),
        .JTAG_data_out (JTAG_data_out),
        .JTAG_busy     (JTAG_busy)
    );


wire SPI_busy;
wire [511:0] SPI_data_out;
reg  [511:0] SPI_bits;

wire tck_spi,tms_spi,tdi_spi;
SPI_master 
    SPI_master_inst(
        .clk           (clk40_pre),
        .TCK           (tck_spi),
        .TMS           (tms_spi),
        .TDI           (tdi_spi),
        .TDO           (tdo),
        .start_action  (start_action),
        .config_period (config_period),
        .SPI_bits      (JTAG_bits),
        .bit_length    (bit_length),
        .SPI_data_out  (SPI_data_out),
        .SPI_busy      (SPI_busy)
    );
assign tck = trst ? tck_jtag : tck_spi;
assign tms = trst ? tms_jtag : tms_spi;
assign tdi = trst ? tdi_jtag : tdi_spi;

wire enable_VIO;

wire trst_VIO,trst_UART;
assign trst = enable_VIO ? trst_VIO : trst_UART;


wire JTAG_busy_VIO;
wire [511:0] JTAG_data_out_VIO;
wire start_action_VIO,start_action_UART;
wire [11:0] config_period_VIO,config_period_UART;
wire [511:0] JTAG_bits_VIO,JTAG_bits_UART;
wire [8:0] bit_length_VIO,bit_length_UART;
wire [4:0] JTAG_inst_VIO,JTAG_inst_UART;
assign  JTAG_busy_VIO     =trst ? JTAG_busy : SPI_busy;
assign  JTAG_data_out_VIO =trst ? JTAG_data_out : SPI_data_out ;
assign  start_action      =enable_VIO ? start_action_VIO : start_action_UART;
assign  config_period     =enable_VIO ? config_period_VIO : config_period_UART;
assign  JTAG_bits         =enable_VIO ? JTAG_bits_VIO : JTAG_bits_UART;
assign  bit_length        =enable_VIO ? bit_length_VIO : bit_length_UART;
assign  JTAG_inst         =enable_VIO ? JTAG_inst_VIO : JTAG_inst_UART;

wire reset_VIO;
vio_Jtag 
  vio_Jtag_inst (
      .clk(clk40_pre),                // input wire clk
      .probe_in0(JTAG_busy_VIO),    // input wire [0 : 0] probe_in0
      .probe_in1(JTAG_data_out_VIO[511:256]),    // input wire [255 : 0] probe_in1
      .probe_in2(JTAG_data_out_VIO[255:0]),    // input wire [255 : 0] probe_in2
      .probe_out0(start_action_VIO),  // output wire [0 : 0] probe_out0
      .probe_out1(config_period_VIO),  // output wire [11 : 0] probe_out1
      .probe_out2(JTAG_bits_VIO[511:256]),  // output wire [255 : 0] probe_out2
      .probe_out3(JTAG_bits_VIO[255:0]),  // output wire [255 : 0] probe_out3
      .probe_out4(bit_length_VIO),  // output wire [8 : 0] probe_out4
      .probe_out5(JTAG_inst_VIO),  // output wire [4 : 0] probe_out5
      .probe_out6(trst_VIO),  // output wire [0 : 0] probe_out6
      .probe_out7(enable_VIO),  // output wire [0 : 0] probe_out7
      .probe_out8(reset_VIO)  // output wire [0 : 0] probe_out7
  );



wire [11:0] width_hit;
wire start_hit;
wire inv_hit;
wire [23:0] hit_mask_hit;
wire delay_hit_VIO;
wire [11:0] interval_hit;
wire start_single_hit;


wire statrt_single_ttc;
wire trigger_ttc;
wire BCR_ttc;
wire event_reset_ttc;
wire master_reset_ttc;
wire [3:0] new_ttc_code;
wire [11:0] roll_over_ttc;
wire new_ttc_mode;
wire enable_bcr_ttc;
wire start_ttc;
wire [11:0] interval_ttc;




wire command_fifo_rd_en,command_fifo_empty,data_back_fifo_wr_en;
wire [159:0] command,data_back;
  UART_interface 
    UART_interface_inst(
      .clk40                (clk40_pre),
      .reset                (reset_VIO),
      .rxd                  (rxd),
      .txd                  (txd),
      .command_fifo_rd_en   (command_fifo_rd_en),
      .command              (command),
      .command_fifo_empty   (command_fifo_empty),
      .data_back            (data_back),
      .data_back_fifo_wr_en (data_back_fifo_wr_en)
    );

  wire tdc_data_fifo_full,tdc_data_fifo_empty,tdc_data_read; 
  wire [39:0] fifo_data_out;
  wire tdc_fifo_reset_UART;


wire locked_0_VIO,locked_1_VIO;
wire edge_select_0,edge_select_1;
wire reselect_VIO;
wire [9:0] correct_value_0_VIO,correct_value_1_VIO,correct_counter_th_VIO;
wire d_line_0_selected,d_line_1_selected;
wire data_ready_even,data_ready_odd;
  data_sampling data_sampling_inst
    (
      .clk                (clk320_pre),
      .rst                (reset_VIO),
      .d_line_0           (d_line[0]),
      .d_line_0_out       (d_line_0_selected),
      .d_line_1           (d_line[1]),
      .d_line_1_out       (d_line_1_selected),
      .reselect           (reselect_VIO),
      .correct_value_0    (correct_value_0_VIO),
      .correct_value_1    (correct_value_1_VIO),
      .correct_counter_th (correct_counter_th_VIO),
      .locked_0           (locked_0_VIO),
      .locked_1           (locked_1_VIO),
      .edge_select_0      (edge_select_0),
      .edge_select_1      (edge_select_1),
      .data_ready_even    (data_ready_even),
      .data_ready_odd     (data_ready_odd)
    );

wire d_line_0_aligned,d_line_1_aligned;
even_odd_aligan
even_odd_aligan_inst(
.clk(clk320_pre),
.rst(reset_VIO),

.data_ready_0(data_ready_even),
.d_line_0(d_line_0_selected),

.data_ready_1(data_ready_odd),
.d_line_1(d_line_1_selected),

.d_line_0_out(d_line_0_aligned),
.d_line_1_out(d_line_1_aligned)
);
wire [9:0] deserial_raw_data;
wire raw_data_fifo_write;
  input_deserial input_deserial_inst (
    .clk(clk320_pre), 
    .rst(reset_VIO), 
    .data_even(d_line_0_aligned), 
    .data_odd(d_line_1_aligned), 
    .high_speed(high_speed_VIO),
    .raw_data_fifo_write(raw_data_fifo_write),
    .data(deserial_raw_data));
wire empty_raw_data_fifo;
wire [9:0] deserial_raw_data_fifo;
raw_data_fifo raw_data_fifo_inst (
  .rst(reset_VIO),        // input wire rst
  .wr_clk(clk64_pre),  // input wire wr_clk
  .rd_clk(clk160_pre),  // input wire rd_clk
  .din(deserial_raw_data),        // input wire [9 : 0] din
  .wr_en(raw_data_fifo_write),    // input wire wr_en
  .rd_en(1'b1),    // input wire rd_en
  .dout(deserial_raw_data_fifo),      // output wire [9 : 0] dout
  .full(),      // output wire full
  .empty(empty_raw_data_fifo)    // output wire empty
);

wire [7:0] deserial_data;
wire o_Kout,o_DErr,o_KErr,o_DpErr;
  mDec8b10bMem_tb  inst_mDec8b10bMem_tb (
      .o8_Dout          (deserial_data),
      .o_Kout           (o_Kout),
      .o_DErr           (o_DErr),
      .o_KErr           (o_KErr),
      .o_DpErr          (o_DpErr),
      .i_ForceDisparity (1'b0),
      .i_Disparity      (1'b0),
      .i10_Din          (deserial_raw_data_fifo),
      .o_Rd             (),
      .i_Clk            (clk160_pre),
      .i_ARst_L         (~reset_VIO),
      .soft_reset_i     (1'b0),
      .i_enable         (~empty_raw_data_fifo)
    );



wire fifo_data_write;
wire [39:0] fifo_data;
tdc_data_interface tdc_data_interface_inst
  (
    .clk             (clk160_pre),
    .rst             (reset_VIO),
    .trigger         (trigger_VIO),
    .K_in_input      (o_Kout),
    .enable_input    (~empty_raw_data_fifo),
    .length          (length_VIO),
    .decode_data_input(deserial_data),
    .fifo_data_write (fifo_data_write),
    .fifo_data       (fifo_data),
    //.hit             (hit[0])
     .o_DErr           (o_DErr),
     .o_KErr           (o_KErr),
     .o_DpErr          (o_DpErr)
  );




wire[23:0] hit_func_inner;

hit_generator
hit_generator_inst(
.clk(clk160_pre),
.clk_320(clk320_pre),
.rst(reset_VIO),
.hit(hit_func_inner),

.width(width_hit),
.start(start_hit),
.inv(inv_hit),
.hit_mask(hit_mask_hit),
.delay(delay_hit_VIO),
.interval(interval_hit),
.start_single(start_single_hit)
);

wire reset_hit;
wire[24:0] random_hit_enable_vio;
wire[7:0]  dead_time_vio;
wire[31:0] hit_rate_vio;
wire[24:0] random_hit_inner;
wire Using_random_hit_vio;
wire [31:0] hit_number;
wire [31:0] reciver_number_r;
wire [31:0] reciver_number_f;
wire [23:0] hit_random_statistic;
wire [4:0]  interest_channel_VIO;
wire hit_stat_clear_VIO;


reset_sync reset_sync_hit_resst(
    .reset_in(reset_VIO),
    .clk(clk_hit),
    .enable(1'b1),
    .reset_out(reset_hit)
);

vio_random_hit vio_random_hit_inst (
  .clk(clk_hit),                // input wire clk
  .probe_out0(random_hit_enable_vio),  // output wire [24 : 0] probe_out0
  .probe_out1(dead_time_vio),  // output wire [7 : 0] probe_out1
  .probe_out2(hit_rate_vio),  // output wire [31 : 0] probe_out2
  .probe_out3(Using_random_hit_vio),
  .probe_out4(hit_stat_clear_VIO),
  .probe_out5(interest_channel_VIO)
);



random_hit_generator_chnls random_hit_generator_chnls_inst(
                .clk(clk_hit),
                .rst(reset_hit),
                .hit_out(random_hit_inner),
                .enable_out(random_hit_enable_vio),
                .RATE(hit_rate_vio),
                .dead_time(dead_time_vio)
                );
reg [23:0] random_hit_inner_sync;
always @(posedge clk160_pre)begin 
   random_hit_inner_sync <= random_hit_inner[23:0];
end



wire ttc_delay_VIO;
wire encoded_control_no_syn;
  ttc_generator_module 
  ttc_generator_module_inst (
    .clk(clk160_pre), 
    .clk_320(clk320_pre),
    .rst(reset_VIO), 
    .encocde_ttc(encoded_control_no_syn), 
    .bcr(bunch_reset_direct_out),

    .delay(ttc_delay_VIO),
    .statrt_single(statrt_single_ttc),
    .trigger(trigger_ttc),
    .BCR(BCR_ttc),
    .event_reset(event_reset_ttc),
    .master_reset(master_reset_ttc),
    .new_ttc_code(new_ttc_code),
    .roll_over(roll_over_ttc),
    .new_ttc_mode(new_ttc_mode),
    .enable_bcr(enable_bcr_ttc),
    .start(start_ttc),
    .interval(interval_ttc)
  );

wire [23:0] syn_hit;
wire syn_encocde_ttc;
wire use_syn_hit_ttc_VIO;
  syn_hit_ttc inst_syn_hit_ttc
    (
      .clk                 (clk160_pre),
      .clk_320             (clk320_pre),
      .rst                 (reset_VIO),
      .hit                 (syn_hit),
      .encocde_ttc         (syn_encocde_ttc),
      .use_syn_hit_ttc_VIO (use_syn_hit_ttc_VIO),
      .delay               (ttc_delay_VIO),
      .new_ttc_mode        (new_ttc_mode),
      .trigger             (trigger_ttc),
      .BCR                 (BCR_ttc),
      .event_reset         (event_reset_ttc),
      .master_reset        (master_reset_ttc),
      .new_ttc_code        (new_ttc_code)
    );


assign hit = use_syn_hit_ttc_VIO ? syn_hit : (Using_random_hit_vio ? random_hit_inner[23:0] : hit_func_inner);
assign hit_detect =  use_syn_hit_ttc_VIO ? syn_hit : (Using_random_hit_vio ? random_hit_inner_sync: hit_func_inner);

reg  encoded_control_r;
assign encoded_control = encoded_control_r;
always @(posedge clk320_pre ) begin
  encoded_control_r <= use_syn_hit_ttc_VIO ? syn_encocde_ttc : encoded_control_no_syn;
end

  command_resolve 
    command_resolve_inst(
      .clk                  (clk40_pre),
      .reset                (reset_VIO),
      .command              (command),
      .command_fifo_empty   (command_fifo_empty),
      .command_fifo_rd      (command_fifo_rd_en),
      .data_back            (data_back),
      .data_back_fifo_write (data_back_fifo_wr_en),
      .trst_from_uart       (trst_UART),
      .start_action         (start_action_UART),
      .config_period        (config_period_UART),
      .jtag_bits            (JTAG_bits_UART),
      .bit_length           (bit_length_UART),
      .JTAG_inst            (JTAG_inst_UART),
      .JTAG_data            (JTAG_data_out_VIO),
      .JTAG_busy            (JTAG_busy_VIO),


      // .tdc_data_fifo(fifo_data_out),
      // .tdc_fifo_empty(tdc_data_fifo_empty),
      // .tdc_fifo_read(tdc_data_read),
      // .tdc_fifo_reset(tdc_fifo_reset_UART),

      .tdc_master_reset(reset_out_UART),
      .high_speed(high_speed_VIO), 
      .length(length_VIO), 
      .trigger_mode(trigger_VIO), 
      
      .correct_value_0(correct_value_0_VIO),
      .correct_value_1(correct_value_1_VIO),
      .correct_counter_th(correct_counter_th_VIO),
      .reselect(reselect_VIO),
      .locked_0(locked_0_VIO),
      .locked_1(locked_1_VIO),
      .edge_select_0(edge_select_0),
      .edge_select_1(edge_select_1),
      
      
      .width_hit(width_hit),
      .start_hit(start_hit),
      .inv_hit(inv_hit),
      .hit_mask_hit(hit_mask_hit),
      .delay_hit(delay_hit_VIO),
      .interval_hit(interval_hit),
      .start_single_hit(start_single_hit),
      
      .statrt_single_ttc(statrt_single_ttc),
      .trigger_ttc(trigger_ttc),
      .BCR_ttc(BCR_ttc),
      .event_reset_ttc(event_reset_ttc),
      .master_reset_ttc(master_reset_ttc),
      .new_ttc_code(new_ttc_code),
      .roll_over_ttc(roll_over_ttc),
      .new_ttc_mode(new_ttc_mode),
      .enable_bcr_ttc(enable_bcr_ttc),
      .start_ttc(start_ttc),
      .interval_ttc(interval_ttc),
      .delay_ttc(ttc_delay_VIO)      
    );
    assign reset_out = reset_out_UART;



endmodule
