`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/07/2017 06:53:25 PM
// Design Name: 
// Module Name: random_hit_generator_12
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module random_hit_generator_chnls(
    input clk, //156.25MHz
    input rst,
    input [24:0] enable_out,
    input [31:0] RATE,  // Rate = (wanted f) / (clk f) *(2^32 -1 ); For 156.25Mhz clock: 32'h0053E2D7 -> 200k, 32'h00A7C5AD -> 400k,  
                                                                  //For 200Mhz    clock: 32'h00418937 -> 200k, 32'h0083126F -> 400k, 
    input [7:0] dead_time,
    output [24:0] hit_out
    );
    random_hit_generator random_hit_generator_up_0(
        .clk(clk),
        .rst(rst),
        .hit_out(hit_out[0]),
        .enable_out(enable_out[0]),
        .RATE(RATE),
        .seed1(32'hAD1659EF),
        .seed2(32'h3D9EF9ED),
        .dead_time_input(dead_time)
        );
    random_hit_generator random_hit_generator_up_1(
        .clk(clk),
        .rst(rst),
        .hit_out(hit_out[1]),
        .enable_out(enable_out[1]),
        .RATE(RATE),
        .seed1(32'hF52B6E17),
        .seed2(32'hA340E748),
        .dead_time_input(dead_time)
        );
    random_hit_generator random_hit_generator_up_2(
        .clk(clk),
        .rst(rst),
        .hit_out(hit_out[2]),
        .enable_out(enable_out[2]),
        .RATE(RATE),
        .seed1(32'hF1914098),
        .seed2(32'h849F101F),
        .dead_time_input(dead_time)
        );
    random_hit_generator random_hit_generator_up_3(
        .clk(clk),
        .rst(rst),
        .hit_out(hit_out[3]),
        .enable_out(enable_out[3]),
        .RATE(RATE),
        .seed1(32'hE0185A63),
        .seed2(32'h7519C363),
        .dead_time_input(dead_time)
        );
    random_hit_generator random_hit_generator_up_4(
        .clk(clk),
        .rst(rst),
        .hit_out(hit_out[4]),
        .enable_out(enable_out[4]),
        .RATE(RATE),
        .seed1(32'h86F2D3A4),
        .seed2(32'h70A96B6F),
        .dead_time_input(dead_time)
        );
    random_hit_generator random_hit_generator_up_5(
        .clk(clk),
        .rst(rst),
        .hit_out(hit_out[5]),
        .enable_out(enable_out[5]),
        .RATE(RATE),
        .seed1(32'h47EFF6A8),
        .seed2(32'h27560BF0),
        .dead_time_input(dead_time)
        );
    random_hit_generator random_hit_generator_up_6(
        .clk(clk),
        .rst(rst),
        .hit_out(hit_out[6]),
        .enable_out(enable_out[6]),
        .RATE(RATE),
        .seed1(32'h443779C2),
        .seed2(32'h174031E2),
        .dead_time_input(dead_time)
        );
    random_hit_generator random_hit_generator_up_7(
        .clk(clk),
        .rst(rst),
        .hit_out(hit_out[7]),
        .enable_out(enable_out[7]),
        .RATE(RATE),
        .seed1(32'h6C676916),
        .seed2(32'hC75F956E),
        .dead_time_input(dead_time)
        );
    random_hit_generator random_hit_generator_up_8(
        .clk(clk),
        .rst(rst),
        .hit_out(hit_out[8]),
        .enable_out(enable_out[8]),
        .RATE(RATE),
        .seed1(32'h9C67C962),
        .seed2(32'h1B0A83F5),
        .dead_time_input(dead_time)
        );
    random_hit_generator random_hit_generator_up_9(
        .clk(clk),
        .rst(rst),
        .hit_out(hit_out[9]),
        .enable_out(enable_out[9]),
        .RATE(RATE),
        .seed1(32'h533CBF55),
        .seed2(32'h88057FB7),
        .dead_time_input(dead_time)
        );
    random_hit_generator random_hit_generator_up_10(
        .clk(clk),
        .rst(rst),
        .hit_out(hit_out[10]),
        .enable_out(enable_out[10]),
        .RATE(RATE),
        .seed1(32'hF7F9670A),
        .seed2(32'hB7E65F27),
        .dead_time_input(dead_time)
        );
    random_hit_generator random_hit_generator_up_11(
        .clk(clk),
        .rst(rst),
        .hit_out(hit_out[11]),
        .enable_out(enable_out[11]),
        .RATE(RATE),
        .seed1(32'hD1EA4A17),
        .seed2(32'h6859BB50),
        .dead_time_input(dead_time)
        ); 
    random_hit_generator random_hit_generator_up_12(
        .clk(clk),
        .rst(rst),
        .hit_out(hit_out[12]),
        .enable_out(enable_out[12]),
        .RATE(RATE),
        .seed1(32'h544372e7),
        .seed2(32'h20719c01),
        .dead_time_input(dead_time)
        );
    random_hit_generator random_hit_generator_up_13(
        .clk(clk),
        .rst(rst),
        .hit_out(hit_out[13]),
        .enable_out(enable_out[13]),
        .RATE(RATE),
        .seed1(32'hfc86d94b),
        .seed2(32'h8f4275d3),
        .dead_time_input(dead_time)
        );
    random_hit_generator random_hit_generator_up_14(
        .clk(clk),
        .rst(rst),
        .hit_out(hit_out[14]),
        .enable_out(enable_out[14]),
        .RATE(RATE),
        .seed1(32'h85e41035),
        .seed2(32'h214424c3),
        .dead_time_input(dead_time)
        );
    random_hit_generator random_hit_generator_up_15(
        .clk(clk),
        .rst(rst),
        .hit_out(hit_out[15]),
        .enable_out(enable_out[15]),
        .RATE(RATE),
        .seed1(32'hcd9c7ef1),
        .seed2(32'h59004e0c),
        .dead_time_input(dead_time)
        );
    random_hit_generator random_hit_generator_up_16(
        .clk(clk),
        .rst(rst),
        .hit_out(hit_out[16]),
        .enable_out(enable_out[16]),
        .RATE(RATE),
        .seed1(32'h86d5121a),
        .seed2(32'h79648c70),
        .dead_time_input(dead_time)
        );
    random_hit_generator random_hit_generator_up_17(
        .clk(clk),
        .rst(rst),
        .hit_out(hit_out[17]),
        .enable_out(enable_out[17]),
        .RATE(RATE),
        .seed1(32'hf1a70c99),
        .seed2(32'hddf991a4),
        .dead_time_input(dead_time)
        );
    random_hit_generator random_hit_generator_up_18(
        .clk(clk),
        .rst(rst),
        .hit_out(hit_out[18]),
        .enable_out(enable_out[18]),
        .RATE(RATE),
        .seed1(32'hb85c1025),
        .seed2(32'h8d53b982),
        .dead_time_input(dead_time)
        );
    random_hit_generator random_hit_generator_up_19(
        .clk(clk),
        .rst(rst),
        .hit_out(hit_out[19]),
        .enable_out(enable_out[19]),
        .RATE(RATE),
        .seed1(32'h7e249272),
        .seed2(32'hb29f3b90),
        .dead_time_input(dead_time)
        );
    random_hit_generator random_hit_generator_up_20(
        .clk(clk),
        .rst(rst),
        .hit_out(hit_out[20]),
        .enable_out(enable_out[20]),
        .RATE(RATE),
        .seed1(32'h8ad83373),
        .seed2(32'h4cae3943),
        .dead_time_input(dead_time)
        );
    random_hit_generator random_hit_generator_up_21(
        .clk(clk),
        .rst(rst),
        .hit_out(hit_out[21]),
        .enable_out(enable_out[21]),
        .RATE(RATE),
        .seed1(32'hd61e6ab2),
        .seed2(32'h00165121),
        .dead_time_input(dead_time)
        );   
    random_hit_generator random_hit_generator_up_22(
        .clk(clk),
        .rst(rst),
        .hit_out(hit_out[22]),
        .enable_out(enable_out[22]),
        .RATE(RATE),
        .seed1(32'hcd808fbd),
        .seed2(32'h74574a86),
        .dead_time_input(dead_time)
        );
    random_hit_generator random_hit_generator_up_23(
        .clk(clk),
        .rst(rst),
        .hit_out(hit_out[23]),
        .enable_out(enable_out[23]),
        .RATE(RATE),
        .seed1(32'hc1c575b8),
        .seed2(32'hdc7525c0),
        .dead_time_input(dead_time)
        );   
    random_hit_generator random_hit_generator_up_24(
        .clk(clk),
        .rst(rst),
        .hit_out(hit_out[24]),
        .enable_out(enable_out[24]),
        .RATE(RATE),
        .seed1(32'h86fdfed5),
        .seed2(32'h84b3df3c),
        .dead_time_input(dead_time)
        );   
endmodule
