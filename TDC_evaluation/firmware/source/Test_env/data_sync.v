
`timescale 1ps/1ps

(* dont_touch = "yes" *)
module data_sync #(
  parameter INITIALISE = 1'b1,
  parameter DEPTH = 3
)
(
   input       data_in,
   input       clk,
   input       enable,
   output      data_out
);


  wire     reset_sync_reg0;
  wire     reset_sync_reg1;
  wire     reset_sync_reg2;

  (* ASYNC_REG = "TRUE", SHREG_EXTRACT = "NO" *)
  FDPE #(
   .INIT (INITIALISE[0])
  ) reset_sync0 (
  .C  (clk),
  .CE (enable),
  .PRE(1'b0),
  .D  (data_in),
  .Q  (reset_sync_reg0)
  );

  (* ASYNC_REG = "TRUE", SHREG_EXTRACT = "NO" *)
  FDPE #(
   .INIT (INITIALISE[0])
  ) reset_sync1 (
  .C  (clk),
  .CE (enable),
  .PRE(1'b0),
  .D  (reset_sync_reg0),
  .Q  (reset_sync_reg1)
  );

  (* ASYNC_REG = "TRUE", SHREG_EXTRACT = "NO" *)
  FDPE #(
   .INIT (INITIALISE[0])
  ) reset_sync2 (
  .C  (clk),
  .CE (enable),
  .PRE(1'b0),
  .D  (reset_sync_reg1),
  .Q  (reset_sync_reg2)
  );


assign data_out = reset_sync_reg2;


endmodule
