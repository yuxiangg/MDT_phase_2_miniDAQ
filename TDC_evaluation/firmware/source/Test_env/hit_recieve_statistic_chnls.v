`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/08/2017 10:42:21 AM
// Design Name: 
// Module Name: hit_recieve_statistic
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////




module hit_recieve_statistic_chnls(
    input clk_hit,
    input clk_recieve,
    input [23:0] hit,
    // input [11:0] hit_mask,
    input data_valid,
    input [4:0] channel_number,
    input rising_edge,
    input [14:0] coarse_counter,
    input [4:0] interest_channel,
    // output [31:0] hit_number ,
    // output  reg  [31:0] reciver_number_r =32'b0,
    // output  reg  [31:0] reciver_number_f =32'b0,
    input clear
    );

wire[31:0] hit_number[23:0];
wire[31:0] reciver_number_r[23:0];
// wire[31:0] reciver_number_f[23:0];



    wire clear_hit;
     data_sync clear_clk_hit_sync_inst
     (
        .data_in(clear),
        .clk(clk_hit),
        .enable(1'b1),
        .data_out(clear_hit)
     );
     
      wire clear_recive;
      data_sync clk_recieve_sync_inst
      (
         .data_in(clear),
         .clk(clk_recieve),
         .enable(1'b1),
         .data_out(clear_recive)
      );









   wire [12:0] coarse_counter_limit_down_vio;
   wire [12:0] coarse_counter_limit_up_vio;
   // wire [31:0] interest_latency_vio;
   wire [31:0] latency_start_vio;
   wire start_read_vio;
   wire rst_content_vio;


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   // wire hit_chosen;
   // wire [31:0] time_latency;
   // wire [8:0] latency_addra;
   // wire [31:0] latency_douta;



    // hit_recieve_latency_statistic hit_recieve_latency_statistic_inst(
    //     .clk_hit(clk_hit),
    //     .clk_recieve(clk_recieve),
    //     .clear_hit(clear_hit),
    //     .clear_recive(clear_recive),
    //     .hit(hit),

    //     .data_valid(data_valid),
    //     .interest_channel(interest_channel),
    //     .channel_number(channel_number),
    //     .rising_edge(rising_edge),
    //     .coarse_counter(coarse_counter),


    //     .coarse_counter_limit_down(coarse_counter_limit_down_vio),
    //     .coarse_counter_limit_up(coarse_counter_limit_up_vio),
    //     // .interest_latency(interest_latency_vio),
    //     .hit_chosen_out(hit_chosea),
    //     .time_latency_out(time_latency)
    //     );




    // hit_receive_latency_counter_statistics hit_receive_latency_counter_statistics_isnt(
    //     .clk(clk_hit),
    //     .rst(clear_hit),
    //     .rst_content(rst_content_vio),
    //     .hit_chosen_in(hit_chosea),
    //     .hit_latency(time_latency),
    //     .latency_start(latency_start_vio),
    //     .start_read(start_read_vio),
    //     .addra(latency_addra),
    //     .douta(latency_douta)
    //     );


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   wire hit_chosen[23:0];
   wire [31:0] time_latency[23:0];
   wire [8:0] latency_addra[23:0];
   wire [31:0] latency_douta[23:0];


generate
  genvar j;
  for (j = 0; j < 24; j = j + 1) begin: hit_latency_blocks
    hit_recieve_latency_statistic hit_recieve_latency_statistic_inst(
        .clk_hit(clk_hit),
        .clk_recieve(clk_recieve),
        .clear_hit(clear_hit),
        .clear_recive(clear_recive),
        .hit(hit),

        .data_valid(data_valid),
        .interest_channel(j),
        .channel_number(channel_number),
        .rising_edge(rising_edge),
        .coarse_counter(coarse_counter),


        .coarse_counter_limit_down(coarse_counter_limit_down_vio),
        .coarse_counter_limit_up(coarse_counter_limit_up_vio),
        // .interest_latency(interest_latency_vio),
        .hit_chosen_out(hit_chosen[j]),
        .time_latency_out(time_latency[j])
        );




    hit_receive_latency_counter_statistics hit_receive_latency_counter_statistics_isnt(
        .clk(clk_hit),
        .rst(clear_hit),
        .rst_content(rst_content_vio),
        .hit_chosen_in(hit_chosen[j]),
        .hit_latency(time_latency[j]),
        .latency_start(latency_start_vio),
        .start_read(start_read_vio),
        .addra(latency_addra[j]),
        .douta(latency_douta[j])
    );
  end
endgenerate




ila_latency_check ila_latency_check_inst (
  .clk(clk_hit), // input wire clk


  .probe0( start_read_vio ), // input wire [0:0]  probe0  
  .probe1( latency_addra[0 ]), // input wire [9:0]  probe1 
  .probe2( latency_addra[1 ]), // input wire [9:0]  probe2 
  .probe3( latency_addra[2 ]), // input wire [9:0]  probe3 
  .probe4( latency_addra[3 ]), // input wire [9:0]  probe4 
  .probe5( latency_addra[4 ]), // input wire [9:0]  probe5 
  .probe6( latency_addra[5 ]), // input wire [9:0]  probe6 
  .probe7( latency_addra[6 ]), // input wire [9:0]  probe7 
  .probe8( latency_addra[7 ]), // input wire [9:0]  probe8 
  .probe9( latency_addra[8 ]), // input wire [9:0]  probe9 
  .probe10(latency_addra[9 ]), // input wire [9:0]  probe10 
  .probe11(latency_addra[10]), // input wire [9:0]  probe11 
  .probe12(latency_addra[11]), // input wire [9:0]  probe12 
  .probe13(latency_addra[12]), // input wire [9:0]  probe13 
  .probe14(latency_addra[13]), // input wire [9:0]  probe14 
  .probe15(latency_addra[14]), // input wire [9:0]  probe15 
  .probe16(latency_addra[15]), // input wire [9:0]  probe16 
  .probe17(latency_addra[16]), // input wire [9:0]  probe17 
  .probe18(latency_addra[17]), // input wire [9:0]  probe18 
  .probe19(latency_addra[18]), // input wire [9:0]  probe19 
  .probe20(latency_addra[19]), // input wire [9:0]  probe20 
  .probe21(latency_addra[20]), // input wire [9:0]  probe21 
  .probe22(latency_addra[21]), // input wire [9:0]  probe22 
  .probe23(latency_addra[22]), // input wire [9:0]  probe23 
  .probe24(latency_addra[23]), // input wire [9:0]  probe24 
  .probe25(latency_douta[0 ]), // input wire [31:0]  probe25 
  .probe26(latency_douta[1 ]), // input wire [31:0]  probe26 
  .probe27(latency_douta[2 ]), // input wire [31:0]  probe27 
  .probe28(latency_douta[3 ]), // input wire [31:0]  probe28 
  .probe29(latency_douta[4 ]), // input wire [31:0]  probe29 
  .probe30(latency_douta[5 ]), // input wire [31:0]  probe30 
  .probe31(latency_douta[6 ]), // input wire [31:0]  probe31 
  .probe32(latency_douta[7 ]), // input wire [31:0]  probe32 
  .probe33(latency_douta[8 ]), // input wire [31:0]  probe33 
  .probe34(latency_douta[9 ]), // input wire [31:0]  probe34 
  .probe35(latency_douta[10]), // input wire [31:0]  probe35 
  .probe36(latency_douta[11]), // input wire [31:0]  probe36 
  .probe37(latency_douta[12]), // input wire [31:0]  probe37 
  .probe38(latency_douta[13]), // input wire [31:0]  probe38 
  .probe39(latency_douta[14]), // input wire [31:0]  probe39 
  .probe40(latency_douta[15]), // input wire [31:0]  probe40 
  .probe41(latency_douta[16]), // input wire [31:0]  probe41 
  .probe42(latency_douta[17]), // input wire [31:0]  probe42 
  .probe43(latency_douta[18]), // input wire [31:0]  probe43 
  .probe44(latency_douta[19]), // input wire [31:0]  probe44 
  .probe45(latency_douta[20]), // input wire [31:0]  probe45 
  .probe46(latency_douta[21]), // input wire [31:0]  probe46 
  .probe47(latency_douta[22]), // input wire [31:0]  probe47 
  .probe48(latency_douta[23]) // input wire [31:0]  probe48
);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   






vio_latency_control vio_latency_control_inst (
  .clk(clk_hit),                // input wire clk
  .probe_out0(coarse_counter_limit_down_vio),  // output wire [12 : 0] probe_out0
  .probe_out1(coarse_counter_limit_up_vio),  // output wire [12 : 0] probe_out1
  .probe_out2(latency_start_vio),  // output wire [31 : 0] probe_out2
  .probe_out3(start_read_vio),  // output wire [0 : 0] probe_out3
  .probe_out4(rst_content_vio)  // output wire [0 : 0] probe_out4
);





































generate
  genvar i;
  for (i = 0; i < 24; i = i + 1) begin: hit_stat_blocks
    hit_recieve_statistic hit_recieve_statistic_inst(
    .clk_hit(clk_hit),
    .clk_recieve(clk_recieve),
    .hit(hit),
    .data_valid(data_valid),
    .channel_number(channel_number),
    .rising_edge(rising_edge),
    .interest_channel(i),
    .hit_number(hit_number[i]),
    .reciver_number_r(reciver_number_r[i]),
    // .reciver_number_f(reciver_number_f[i]),
    .clear_hit(clear_hit),
    .clear_recive(clear_recive)
    );
  end
endgenerate

vio_hit_stat_check vio_hit_stat_check_inst (
  .clk(clk_recieve),                // input wire clk
  .probe_in0( hit_number[0]  ),    // input wire [31 : 0] probe_in0
  .probe_in1( reciver_number_r[0]),    // input wire [31 : 0] probe_in1
  .probe_in2( hit_number[1]  ),    // input wire [31 : 0] probe_in2
  .probe_in3( reciver_number_r[1]),    // input wire [31 : 0] probe_in3
  .probe_in4( hit_number[2]  ),    // input wire [31 : 0] probe_in4
  .probe_in5( reciver_number_r[2]),    // input wire [31 : 0] probe_in5
  .probe_in6( hit_number[3]  ),    // input wire [31 : 0] probe_in6
  .probe_in7( reciver_number_r[3]),    // input wire [31 : 0] probe_in7
  .probe_in8( hit_number[4]  ),    // input wire [31 : 0] probe_in8
  .probe_in9( reciver_number_r[4]),    // input wire [31 : 0] probe_in9
  .probe_in10(hit_number[5]  ),  // input wire [31 : 0] probe_in10
  .probe_in11(reciver_number_r[5]),  // input wire [31 : 0] probe_in11
  .probe_in12(hit_number[6]  ),  // input wire [31 : 0] probe_in12
  .probe_in13(reciver_number_r[6]),  // input wire [31 : 0] probe_in13
  .probe_in14(hit_number[7]  ),  // input wire [31 : 0] probe_in14
  .probe_in15(reciver_number_r[7]),  // input wire [31 : 0] probe_in15
  .probe_in16(hit_number[8]  ),  // input wire [31 : 0] probe_in16
  .probe_in17(reciver_number_r[8]),  // input wire [31 : 0] probe_in17
  .probe_in18(hit_number[9]  ),  // input wire [31 : 0] probe_in18
  .probe_in19(reciver_number_r[9]),  // input wire [31 : 0] probe_in19
  .probe_in20(hit_number[10] ),  // input wire [31 : 0] probe_in20
  .probe_in21(reciver_number_r[10]),  // input wire [31 : 0] probe_in21
  .probe_in22(hit_number[11] ),  // input wire [31 : 0] probe_in22
  .probe_in23(reciver_number_r[11]),  // input wire [31 : 0] probe_in23
  .probe_in24(hit_number[12] ),  // input wire [31 : 0] probe_in24
  .probe_in25(reciver_number_r[12]),  // input wire [31 : 0] probe_in25
  .probe_in26(hit_number[13] ),  // input wire [31 : 0] probe_in26
  .probe_in27(reciver_number_r[13]),  // input wire [31 : 0] probe_in27
  .probe_in28(hit_number[14] ),  // input wire [31 : 0] probe_in28
  .probe_in29(reciver_number_r[14]),  // input wire [31 : 0] probe_in29
  .probe_in30(hit_number[15] ),  // input wire [31 : 0] probe_in30
  .probe_in31(reciver_number_r[15]),  // input wire [31 : 0] probe_in31
  .probe_in32(hit_number[16] ),  // input wire [31 : 0] probe_in32
  .probe_in33(reciver_number_r[16]),  // input wire [31 : 0] probe_in33
  .probe_in34(hit_number[17] ),  // input wire [31 : 0] probe_in34
  .probe_in35(reciver_number_r[17]),  // input wire [31 : 0] probe_in35
  .probe_in36(hit_number[18] ),  // input wire [31 : 0] probe_in36
  .probe_in37(reciver_number_r[18]),  // input wire [31 : 0] probe_in37
  .probe_in38(hit_number[19] ),  // input wire [31 : 0] probe_in38
  .probe_in39(reciver_number_r[19]),  // input wire [31 : 0] probe_in39
  .probe_in40(hit_number[20] ),  // input wire [31 : 0] probe_in40
  .probe_in41(reciver_number_r[20]),  // input wire [31 : 0] probe_in41
  .probe_in42(hit_number[21] ),  // input wire [31 : 0] probe_in42
  .probe_in43(reciver_number_r[21]),  // input wire [31 : 0] probe_in43
  .probe_in44(hit_number[22] ),  // input wire [31 : 0] probe_in44
  .probe_in45(reciver_number_r[22]),  // input wire [31 : 0] probe_in45
  .probe_in46(hit_number[23] ),  // input wire [31 : 0] probe_in46
  .probe_in47(reciver_number_r[23])  // input wire [31 : 0] probe_in47
);
    
endmodule
