/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : ttc_generator_module_single.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on 2018-05-03 10:05:05
//  Note       : 
//     

module ttc_generator_module_single
(
	input clk,
	input clk_320,
	input rst,

	output encocde_ttc,

    input delay,
	input start_single,

	input new_ttc_mode,

	input trigger,
	input BCR,
	input event_reset,
	input master_reset,

	input [3:0] new_ttc_code
);

wire encode_ttc_legacy;
wire encode_ttc_new;
//wire new_ttc_mode_VIO;
reg encocde_ttc_r;
always @(posedge clk) begin
		encocde_ttc_r <= new_ttc_mode ? encode_ttc_new : encode_ttc_legacy;

end
reg encocde_ttc_r1;
always @(posedge clk_320) begin
    encocde_ttc_r1 <= encocde_ttc_r;
end
assign encocde_ttc = delay ? encocde_ttc_r1 : encocde_ttc_r;


reg  VIO_start_syn = 1'b0;
reg VIO_start_syn_160M = 1'b0;
reg [1:0] VIO_statrt_r;


assign  start_pulse = start_single ;
always @(posedge clk) begin
	VIO_statrt_r <= {VIO_statrt_r[0],start_pulse};
end

reg [1:0] syn_40M_count;
always @(posedge clk) begin
	syn_40M_count <= (&syn_40M_count) ? 2'b00 : syn_40M_count + 2'b01;
end
wire syn_40M = &syn_40M_count;

always @(posedge clk ) begin
	if (rst) begin
		// reset
		VIO_start_syn <= 1'b0;
	end	else if ((VIO_statrt_r[0])&(~VIO_statrt_r[1])) begin
		VIO_start_syn <= 1'b1;
	end else if(syn_40M) begin
		VIO_start_syn <= 1'b0;
	end
end



//wire trigger_VIO;
//wire BCR_VIO;
//wire event_VIO;
//wire master_reset_VIO;

reg [2:0] legacy_ttc;
always @(posedge clk or posedge rst) begin
	if (rst) begin
		// reset
		legacy_ttc <= 3'b000;
	end	else if (syn_40M) begin
		if(VIO_start_syn) begin
			if(BCR)begin
				legacy_ttc <= 3'b110; 
			end else if(trigger) begin
				legacy_ttc <= 3'b100; 
			end else if(event_reset)  begin
				legacy_ttc <= 3'b111; 
			end else if(master_reset) begin
				legacy_ttc <= 3'b101; 
			end else begin
				legacy_ttc <= 3'b0000; 
			end			
		end else begin
			legacy_ttc <= {legacy_ttc[1:0],1'b0};
		end
	end
end
assign encode_ttc_legacy = legacy_ttc[2];



always @(posedge clk ) begin
	if (rst) begin
		// reset
		VIO_start_syn_160M <= 1'b0;
	end	else if ((~VIO_statrt_r[0])&(VIO_statrt_r[1])) begin
		VIO_start_syn_160M <= 1'b1;
	end else begin
		VIO_start_syn_160M <= 1'b0;
	end
end

reg [3:0] ttc_r;
//wire [3:0] ttc_VIO;
always @(posedge clk or posedge rst) begin
	if (rst) begin
		// reset
		ttc_r <= 4'b0000;
	end	else if(VIO_start_syn_160M) begin
			ttc_r <= new_ttc_code;		
	end else begin
			ttc_r <= {ttc_r[2:0],1'b0};
	end
end
assign encode_ttc_new = ttc_r[3];


endmodule
