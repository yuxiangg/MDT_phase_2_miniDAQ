`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/07/2017 01:46:18 PM
// Design Name: 
// Module Name: random_hit_generator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module random_hit_generator(
    input clk,
    input rst,
    output hit_out,
    input enable_out,
    input [31:0] RATE,
    input [31:0] seed1,
    input [31:0] seed2,
    input [7:0]dead_time_input
    );
    reg loaded=1'b0;
    reg load_seed=1'b0;
    wire[31:0] time_value;
    wire[31:0] time_width;
    wire [31:0] RATE_vio;
    reg hit=1'b0;
    reg [7:0] dead_time=8'b0;
    reg hit_inner=1'b0;
    assign hit_out=hit_inner;
    
    always @(posedge clk)
    begin hit_inner<=hit&enable_out;end
    
    random_generator random_generator_hit_time(
        .clk(clk),
        .reset(rst),
        .loadseed_i(load_seed),
        .seed_i(seed1),
        .enable(1'b1),
        .number_o(time_value)
        );
    reg width_enable=1'b0;
    
    random_generator random_generator_hit_width(
            .clk(clk),
            .reset(rst),
            .loadseed_i(load_seed),
            .seed_i(seed2),
            .enable(width_enable),
            .number_o(time_width)
            );  
    always @(posedge clk)begin
        if(rst)     
            loaded<=1'b0; 
        else if(~loaded) 
            loaded<=1'b1;
    end
    
    always @(posedge clk)begin
        if(rst)     
            load_seed<=1'b0; 
        else if(~loaded) 
            load_seed<=1'b1;
        else load_seed<=1'b0;
    end        
    
    reg [4:0] width_counter=5'h0a;
    
   
    always @(posedge clk)begin
        if(rst)
            hit<=1'b0;
        else if((time_value<RATE)&(~(|dead_time)))
            hit<=1'b1;
        else if(~(|width_counter))
            hit<=1'b0;
   end
   
    always @(posedge clk)begin
       if(rst)
           width_enable<=1'b0;
       else if(time_value<RATE)
           width_enable<=1'b1;
       else begin
        width_enable<=1'b0;end
  end     
  
   always @(posedge clk)begin
     if(rst)
         width_counter=5'h0a;
     else if((time_value<RATE)&(hit==1'b0))
         width_counter<=time_width[4:0]|5'b01000;
     else begin
        width_counter<=(|width_counter)?(width_counter-5'b1):width_counter;end
end          
     
     always@(posedge clk)begin
        if(rst)dead_time<=8'b0;
        else if((time_value<RATE)&(hit==1'b0))
            dead_time <= dead_time_input;
        else begin
            dead_time <= (|dead_time)?dead_time-8'b1:dead_time; end 
     end 
     
//  ila_2 your_instance_name (
//	.clk(clk), // input wire clk


//	.probe0(time_value), // input wire [31:0]  probe0  
//	.probe1(time_width), // input wire [31:0]  probe1 
//	.probe2(hit), // input wire [0:0]  probe2 
//	.probe3(width_counter), // input wire [4:0]  probe3 
//	.probe4(width_enable), // input wire [0:0]  probe4 
//	.probe5(load_seed), // input wire [0:0]  probe5 
//	.probe6(loaded), // input wire [0:0]  probe6 
//	.probe7(rst) // input wire [0:0]  probe7
//);

//vio_1 hit_vio (
//  .clk(clk),                // input wire clk
//  .probe_out0(RATE_vio)  // output wire [31 : 0] probe_out0
//);
endmodule
