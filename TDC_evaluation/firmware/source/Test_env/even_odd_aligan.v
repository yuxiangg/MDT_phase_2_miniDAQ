/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : even_odd_aligan.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on 2018-04-21 11:41:29
//  Note       : 
//     
module even_odd_aligan(
input clk,
input rst,

input data_ready_0,
input d_line_0,



input data_ready_1,
input d_line_1,


output reg d_line_0_out,
output reg d_line_1_out
);


reg [3:0] ready_distance_counter=4'b0;

always @(posedge clk  ) begin
	if (rst) begin
		// reset
		ready_distance_counter <= 4'b0;
	end	else if(ready_distance_counter==4'b1001) begin
		ready_distance_counter <= 4'b0;
	end else begin
		ready_distance_counter <= ready_distance_counter + 4'b1;
	end
end

reg [3:0] ready_0_position = 4'b0;
reg [3:0] ready_1_position = 4'b0;
always @(posedge clk  ) begin
	if (rst) begin
		// reset
		ready_0_position <= 4'b0;
	end	else if (data_ready_0) begin
		ready_0_position <= ready_distance_counter;
	end
end

always @(posedge clk  ) begin
	if (rst) begin
		// reset
		ready_1_position <= 4'b0;
	end	else if (data_ready_1) begin
		ready_1_position <= ready_distance_counter;
	end
end

wire [3:0] ready_0_1_dostance;
assign ready_0_1_dostance = (ready_0_position >= ready_1_position) ? (ready_0_position - ready_1_position) : (ready_0_position + 4'd10- ready_1_position);
wire [3:0] ready_1_0_dostance;
assign ready_1_0_dostance = (ready_1_position >= ready_0_position) ? (ready_1_position - ready_0_position) : (ready_1_position + 4'd10- ready_0_position);

wire d_line_0_selected;
assign d_line_0_selected = d_line_0;
wire d_line_1_selected;
assign d_line_1_selected = d_line_1;

reg [3:0] d_line_0_selected_delay;
reg [3:0] d_line_1_selected_delay;
always @(posedge clk  ) begin
	if (rst) begin
		// reset
		d_line_0_selected_delay <= 4'b0;
		d_line_1_selected_delay <= 4'b0;
	end	else begin
		d_line_0_selected_delay <= {d_line_0_selected_delay[2:0],d_line_0_selected};
		d_line_1_selected_delay <= {d_line_1_selected_delay[2:0],d_line_1_selected};
	end
end

always @(posedge clk  ) begin
	if (rst) begin
		// reset
		d_line_0_out <= 1'b0;
	end
	else if (ready_0_1_dostance <= ready_1_0_dostance) begin
		d_line_0_out <=d_line_0_selected;
	end else begin
		case(ready_1_0_dostance)
			4'b0001: d_line_0_out <= d_line_0_selected_delay[0];
			4'b0010: d_line_0_out <= d_line_0_selected_delay[1];
			4'b0011: d_line_0_out <= d_line_0_selected_delay[2];
			4'b0100: d_line_0_out <= d_line_0_selected_delay[3];
			default: d_line_0_out <=d_line_0_selected;
		endcase
	end
end

always @(posedge clk  ) begin
	if (rst) begin
		// reset
		d_line_1_out <= 1'b0;
	end
	else if (ready_1_0_dostance <= ready_0_1_dostance) begin
		d_line_1_out <=d_line_1_selected;
	end else begin
		case(ready_0_1_dostance)
			4'b0001: d_line_1_out <= d_line_1_selected_delay[0];
			4'b0010: d_line_1_out <= d_line_1_selected_delay[1];
			4'b0011: d_line_1_out <= d_line_1_selected_delay[2];
			4'b0100: d_line_1_out <= d_line_1_selected_delay[3];
			default: d_line_1_out <=d_line_1_selected;
		endcase
	end
end

//even_odd_align even_odd_align_inst (
//	.clk(clk), // input wire clk


//	.probe0(ready_distance_counter), // input wire [3:0]  probe0  
//	.probe1(ready_0_position), // input wire [3:0]  probe1 
//	.probe2(ready_1_position), // input wire [3:0]  probe2 
//	.probe3(ready_0_1_dostance), // input wire [3:0]  probe3 
//	.probe4(ready_1_0_dostance), // input wire [3:0]  probe4 
//	.probe5(d_line_0_selected_delay), // input wire [3:0]  probe5 
//	.probe6(d_line_1_selected_delay), // input wire [3:0]  probe6 
//	.probe7(data_ready_0), // input wire [0:0]  probe7 
//	.probe8(data_ready_1), // input wire [0:0]  probe8 
//	.probe9(d_line_0), // input wire [0:0]  probe9 
//	.probe10(d_line_1), // input wire [0:0]  probe10 
//	.probe11(d_line_0_out), // input wire [0:0]  probe11 
//	.probe12(d_line_1_out), // input wire [0:0]  probe12 
//	.probe13(1'b0) // input wire [0:0]  probe13
//);


endmodule