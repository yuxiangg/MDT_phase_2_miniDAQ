/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : ttc_generator_module.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on 2018-05-03 10:05:05
//  Note       : 
//     

module ttc_generator_module
(
	input clk,
	input clk_320,
	input rst,
	output encocde_ttc,
	output bcr,

    input delay,
	input statrt_single,
	input trigger,
	input BCR,
	input event_reset,
	input master_reset,
	input [3:0] new_ttc_code,
	input [11:0] roll_over,
	input new_ttc_mode,
	input enable_bcr,
	input start,
	input [11:0] interval
);

wire encode_ttc_legacy;
wire encode_ttc_new;
//wire new_ttc_mode_VIO;
reg encocde_ttc_r;
always @(posedge clk) begin
		encocde_ttc_r <= new_ttc_mode ? encode_ttc_new : encode_ttc_legacy;

end
reg encocde_ttc_r1;
always @(posedge clk_320) begin
    encocde_ttc_r1 <= encocde_ttc_r;
end
assign encocde_ttc = delay ? encocde_ttc_r1 : encocde_ttc_r;

//wire statrt_single;
reg  VIO_start_syn = 1'b0;
reg VIO_start_syn_160M = 1'b0;
reg [1:0] VIO_statrt_r;

//wire [11:0] interval_VIO;
reg  [11:0] interval_counter = 12'b0;
always @(posedge clk or posedge rst) begin
	if (rst) begin
		// reset
		interval_counter <= 12'b0;
	end else begin
		interval_counter <= (interval_counter == interval) ? 12'b0 : interval_counter +12'b1;
	end
end



assign  start_pulse = statrt_single | (start&(interval_counter == interval));
always @(posedge clk) begin
	VIO_statrt_r <= {VIO_statrt_r[0],start_pulse};
end

reg [1:0] syn_40M_count;
always @(posedge clk) begin
	syn_40M_count <= (&syn_40M_count) ? 2'b00 : syn_40M_count + 2'b01;
end
wire syn_40M = &syn_40M_count;

always @(posedge clk ) begin
	if (rst) begin
		// reset
		VIO_start_syn <= 1'b0;
	end	else if ((VIO_statrt_r[0])&(~VIO_statrt_r[1])) begin
		VIO_start_syn <= 1'b1;
	end else if(syn_40M) begin
		VIO_start_syn <= 1'b0;
	end
end



//wire trigger_VIO;
//wire BCR_VIO;
//wire event_VIO;
//wire master_reset_VIO;

reg [2:0] legacy_ttc;
always @(posedge clk or posedge rst) begin
	if (rst) begin
		// reset
		legacy_ttc <= 3'b000;
	end	else if (syn_40M) begin
		if(VIO_start_syn) begin
			if(BCR)begin
				legacy_ttc <= 3'b110; 
			end else if(trigger) begin
				legacy_ttc <= 3'b100; 
			end else if(event_reset)  begin
				legacy_ttc <= 3'b111; 
			end else if(master_reset) begin
				legacy_ttc <= 3'b101; 
			end else begin
				legacy_ttc <= 3'b0000; 
			end			
		end else begin
			legacy_ttc <= {legacy_ttc[1:0],1'b0};
		end
	end
end
assign encode_ttc_legacy = legacy_ttc[2];



always @(posedge clk ) begin
	if (rst) begin
		// reset
		VIO_start_syn_160M <= 1'b0;
	end	else if ((~VIO_statrt_r[0])&(VIO_statrt_r[1])) begin
		VIO_start_syn_160M <= 1'b1;
	end else begin
		VIO_start_syn_160M <= 1'b0;
	end
end

reg [3:0] ttc_r;
//wire [3:0] ttc_VIO;
always @(posedge clk or posedge rst) begin
	if (rst) begin
		// reset
		ttc_r <= 4'b0000;
	end	else if(VIO_start_syn_160M) begin
			ttc_r <= new_ttc_code;		
	end else begin
			ttc_r <= {ttc_r[2:0],1'b0};
	end
end
assign encode_ttc_new = ttc_r[3];



reg [11:0] counter_bcr;
//wire [11:0] roll_over_VIO;
always @(posedge clk ) begin
	if(syn_40M)begin
		counter_bcr <= (counter_bcr== roll_over) ? 12'b0 : (counter_bcr +12'b1);
	end
end
//wire enable_bcr_VIO;
assign bcr= enable_bcr&(counter_bcr == roll_over);

// ttc_generator ttc_generator_vio_inst (
//   .clk(clk),                // input wire clk
//   .probe_out0(VIO_statrt_single),  // output wire [0 : 0] probe_out0
//   .probe_out1(trigger_VIO),  // output wire [0 : 0] probe_out1
//   .probe_out2(BCR_VIO),  // output wire [0 : 0] probe_out2
//   .probe_out3(event_VIO),  // output wire [0 : 0] probe_out3
//   .probe_out4(master_reset_VIO),  // output wire [0 : 0] probe_out4
//   .probe_out5(ttc_VIO),  // output wire [3 : 0] probe_out5
//   .probe_out6(roll_over_VIO),  // output wire [11 : 0] probe_out6
//   .probe_out7(new_ttc_mode_VIO),  // output wire [11 : 0] probe_out7
//   .probe_out8(enable_bcr_VIO),
//   .probe_out9(VIO_start),
//   .probe_out10(interval_VIO)
// );
// ttc_generator_ila ttc_generator_ila_inst (
// 	.clk(clk), // input wire clk


// 	.probe0(encocde_ttc), // input wire [0:0]  probe0  
// 	.probe1(bcr), // input wire [0:0]  probe1 
// 	.probe2(syn_40M), // input wire [0:0]  probe2 
// 	.probe3(VIO_start_syn_160M), // input wire [0:0]  probe3 
// 	.probe4(legacy_ttc), // input wire [2:0]  probe4 
// 	.probe5(ttc_r) // input wire [3:0]  probe5
// );


endmodule
