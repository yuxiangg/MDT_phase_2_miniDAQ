`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/08/2017 10:42:21 AM
// Design Name: 
// Module Name: hit_recieve_statistic
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////




module hit_recieve_statistic(
    input clk_hit,
    input clk_recieve,
    input [23:0] hit,
    // input [11:0] hit_mask,
    input data_valid,
    input [4:0] channel_number,
    input rising_edge,
    // input [14:0] coarse_counter,
    input [4:0] interest_channel,
    output [31:0] hit_number ,
    output  reg  [31:0] reciver_number_r =32'b0,
    // output  reg  [31:0] reciver_number_f =32'b0,
    input clear_hit,
    input clear_recive
    );

    wire[23:0] hit_mask;
    assign hit_mask = 1 << interest_channel;
    reg [2:0] hit_reg=3'b0;
    wire hit_inner;
    assign hit_inner=|(hit_mask&hit);
    always @(posedge  clk_hit)begin
        hit_reg<={hit_reg[1:0],hit_inner};
    end
    
     
    reg [31:0] hit_number_hit_region=32'b0;
    always @(posedge  clk_hit)begin
        if(clear_hit) hit_number_hit_region<=32'b0;
        else begin
            if((~hit_reg[1])&hit_reg[0])
                hit_number_hit_region<=hit_number_hit_region+32'b1;
        end
   end
   wire sync_full;
   wire sync_empty;
   fifo_hit_stat fifo_hit_stat_isnt (
     .wr_clk(clk_hit),  // input wire wr_clk
     .rd_clk(clk_recieve),  // input wire rd_clk
     .din(hit_number_hit_region),        // input wire [31 : 0] din
     .wr_en((~hit_reg[2])&hit_reg[1]),    // input wire wr_en
     .rd_en(1'b1),    // input wire rd_en
     .dout(hit_number),      // output wire [31 : 0] dout
     .full(sync_full),      // output wire full
     .empty(sync_empty)    // output wire empty
   );
  



  always @(posedge clk_recieve)begin
       if(clear_recive) reciver_number_r<=32'b0;
       else begin
           if(rising_edge&data_valid&(channel_number==interest_channel))
               reciver_number_r<=reciver_number_r+32'b1;
       end
  end


////////////////////////////////////////////////////  uncomment if you want see difference  /////////////////////////////////////////

  // reg [32:0] difference; 
  // reg [32:0] difference_filter;
  // always @(posedge clk_recieve)begin
  //       difference<=hit_number-reciver_number_r;
  // end  
   
   // reg [9:0] wait_counter=10'b0;
   // always @(posedge clk_recieve)begin
   //      if((hit_number-reciver_number_r)!=difference) wait_counter<=10'h060;
   //      else if(|wait_counter) wait_counter<= wait_counter-10'b1;
   // end  
   
   // always @(posedge clk_recieve)begin
   //       if(~(|wait_counter))difference_filter<=difference;
   // end 
   
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    
 // always @(posedge clk_recieve)begin
 //       if(clear_recive) reciver_number_f<=32'b0;
 //       else begin
 //           if((~rising_edge&data_valid)&(channel_number==interest_channel))
 //               reciver_number_f<=reciver_number_f+32'b1;
 //       end
 //  end
  
  
  // ila_hit_stat1 ila_hit_stat1_inst (
  //     .clk(clk_recieve), // input wire clk
  
  
  //     .probe0(reciver_number_r), // input wire [31:0]  probe0  
  //     .probe1(reciver_number_f), // input wire [31:0]  probe1 
  //     .probe2(hit_number), // input wire [31:0]  probe2 
  //     .probe3(interest_channel), // input wire [4:0]  probe3 
  //     .probe4(sync_empty), // input wire [0:0]  probe4 
  //     .probe5(data_valid), // input wire [0:0]  probe5 
  //     .probe6(rising_edge), // input wire [0:0]  probe6 
  //     .probe7(channel_number), // input wire [4:0]  probe7
  //     .probe8(difference), // [32:0]
  //     .probe9(wait_counter),//[9:0]
  //     .probe10(difference_filter) //[32:0]
  // );
  
  //  ila_hit_stat2 ila_hit_stat2_inst (
  //      .clk(clk_hit), // input wire clk
  
  
  //      .probe0(hit), // input wire [23:0]  probe0  
  //      .probe1(hit_mask), // input wire [23:0]  probe1 
  //      .probe2(hit_inner) // input wire [23:0]  probe2
  //  );
    
endmodule
