/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : packet_recombine.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on 2018-04-27 17:05:08
//  Note       : 
//     

module packet_recombine(
input clk,
input rst,
input enable,
input [2:0] length,
input K_in,
input [7:0] decode_data,
output reg [39:0] data_out,
input [23:0] hit_ref
);
reg [39:0] data;

reg [2:0] counter;
always @(posedge clk  ) begin
	if (rst) begin
		// reset
		counter <= 3'b0;
	end	else if(enable) begin
		if (~K_in) begin
			counter <= (counter==length) ? 3'b1 : counter + 3'b1;
		end else begin
			counter <= 3'b0;
		end
	end
end

always @(posedge clk  ) begin
	if (rst) begin
		// reset
		data <= 40'b0;
	end	else if(enable)begin
		if (~K_in) begin
			if(counter!=length)begin
				data <= {data[31:0],decode_data};
			end else begin
				data <= {32'b0,decode_data};
			end
		end else begin
			data <= 40'b0;
		end
	end
end

always @(posedge clk  ) begin
	if (rst) begin
		// reset
		data_out <= 40'b0;
	end	else if(enable&(counter==length))begin
		data_out <= data;
	end
end

packet_recombine_ila packet_recombine_ila_inst (
	.clk(clk), // input wire clk


	.probe0(enable), // input wire [0:0]  probe0  
	.probe1(length), // input wire [2:0]  probe1 
	.probe2(decode_data), // input wire [7:0]  probe2 
	.probe3(data_out), // input wire [39:0]  probe3 
	.probe4(counter), // input wire [2:0]  probe4 
	.probe5(data), // input wire [39:0]  probe5
	.probe6(K_in), // input wire [0:0]  probe6
	.probe7('b0)
);

endmodule