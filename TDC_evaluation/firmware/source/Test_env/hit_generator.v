/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : hit_generator.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on 2018-04-23 18:26:22
//  Note       : 
//     
module hit_generator(
input clk,
input clk_320,
input rst,
output [23:0] hit,

input [11:0] width,
input start,
input inv,
input [23:0] hit_mask,
input delay,
input [11:0] interval,
input start_single
);
	wire hit_inner;
//	wire [11:0] width;
	wire start_pulse;
	pulse_hit_generator 
		pulse_hit_generator_inst (
			.clk(clk), 
			.rst(rst), 
			.width(width), 
			.start(start_pulse), 
			.inv(inv), 
			.hit(hit_inner)
		);
	//wire [23:0] hit_mask;
	reg  [23:0] hit_160;
	always @(posedge clk) begin
		hit_160 <= hit_mask & {24{hit_inner}};
	end

	//wire delay;
	reg [23:0] hit_320_inner=24'b0;
//	always @(posedge clk_320)begin
//	    hit_320_inner <= hit_160;
//	end
//	reg [23:0] hit_320=24'b0;
	
//	always @(posedge clk_320)begin
//        hit_320 <= delay ? hit_320_inner:hit_160;
//     end
    	always @(posedge clk_320)begin
         hit_320_inner <= hit_160;
     end
     reg [23:0] hit_320=24'b0;
     
     always @(posedge clk_320)begin
         hit_320 <= delay ? hit_320_inner:hit_160;
      end
	// reg [23:0] hit_320_neg=24'b0;
	// always @(negedge clk_320)begin
 //       hit_320_neg <= hit_320;
 //    end
 //   assign hit = delay_VIO[0]? hit_320_neg : hit_320;
	assign hit = hit_320;

	reg  [11:0] interval_counter = 12'b0;
	always @(posedge clk or posedge rst) begin
		if (rst) begin
			// reset
			interval_counter <= 12'b0;
		end else begin
			interval_counter <= (interval_counter == interval) ? 12'b0 : interval_counter +12'b1;
		end
	end
	
	//wire start_single;
	assign start_pulse = (start & ( interval_counter == interval ))|start_single;
	
	// hit_generator_vio 
	// 	hit_generator_vio_inst (
 //  			.clk(clk),                // input wire clk
 //  			.probe_out0(width),  // output wire [11 : 0] probe_out0
 //  			.probe_out1(start),  // output wire [0 : 0] probe_out1
 //  			.probe_out2(inv),  // output wire [0 : 0] probe_out2
 //  			.probe_out3(hit_mask),  // output wire [23 : 0] probe_out3
 //  			.probe_out4(delay_VIO),
 //  			.probe_out5(interval_VIO),
 //  			.probe_out6(start_single_VIO)
	// 	);
endmodule