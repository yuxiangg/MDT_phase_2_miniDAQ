`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/11/2017 02:08:54 PM
// Design Name: 
// Module Name: hit_counter_statistics
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module hit_receive_latency_counter_statistics(
    input clk,
    input rst,
    input rst_content,
    input hit_chosen_in,
    input [31:0] hit_latency,
    input [31:0] latency_start,
    input start_read,
    output reg [8:0] addra = 9'b0,
    output [31:0] douta
    );
    reg wea = 1'b0;
    // reg [9:0] addra = 10'b0;
    reg [31:0] dina=32'b0;
    // wire [31:0] douta;
    blk_mem_latency blk_mem_latency_inst (
      .clka(clk),    // input wire clka
      .wea(wea),      // input wire [0 : 0] wea
      .addra(addra),  // input wire [14 : 0] addra
      .dina(dina),    // input wire [31 : 0] dina
      .douta(douta)  // output wire [31 : 0] douta
    );
    reg [8:0] state = 9'b0;
    reg [8:0] nextstate = 9'b0;
    localparam IDLE           = 9'b000000001; localparam IDLE_bit            =0;
    localparam RESET_CYCLE    = 9'b000000010; localparam RESET_CYCLE_bit     =1;    
    localparam RESET_CYCLE_2  = 9'b000000100; localparam RESET_CYCLE_2_bit   =2;
    localparam WRITE_CYCLE    = 9'b000001000; localparam WRITE_CYCLE_bit     =3;
    localparam WRITE_CYCLE_2  = 9'b000010000; localparam WRITE_CYCLE_2_bit   =4;
    localparam WRITE_CYCLE_3  = 9'b000100000; localparam WRITE_CYCLE_3_bit   =5;
    localparam WRITE_CYCLE_4  = 9'b001000000; localparam WRITE_CYCLE_4_bit   =6;
    localparam READ_CYCLE     = 9'b010000000; localparam READ_CYCLE_bit      =7;
    localparam READ_CYCLE_2   = 9'b100000000; localparam READ_CYCLE_2_bit    =8;
    always@(posedge clk)begin
        if(rst)state <= 3'b0;
        else state <= nextstate;
    end
    
    reg start_read_r = 1'b0;
    always @(posedge clk ) begin
      start_read_r <= start_read;
    end
    reg hit_add_done=1'b0;
    reg hit_latency_level=1'b0;
    reg [31:0] hit_latency_r=32'b0;
    
    reg hit_chosen=1'b0;
    always @(posedge clk) begin
        hit_chosen<=hit_chosen_in;
    end
    always @(posedge clk) begin
      if (hit_chosen) begin
        hit_latency_level<=1'b1;
        hit_latency_r<=hit_latency-latency_start;
      end
      else if (hit_add_done) begin
        hit_latency_level<=1'b0;
      end
    end
    reg write_done=1'b0;
    reg rst_content_r=1'b0;
    always @(posedge clk ) begin
      rst_content_r <= rst_content;
    end
    always@(*)begin
      case(1'b1)
      state[IDLE_bit]: begin
        if (rst_content&(~rst_content_r)) begin
           nextstate = RESET_CYCLE;
        end else if(start_read&(~start_read_r))begin
          nextstate = READ_CYCLE;
        end else if(hit_latency_level)begin
          nextstate = WRITE_CYCLE;
        end else begin
          nextstate = IDLE;
        end
      end
      state[RESET_CYCLE_bit]: nextstate = RESET_CYCLE_2;
      state[RESET_CYCLE_2_bit]:begin
        if(addra==9'h1ff)begin
          nextstate = IDLE;
        end else begin
          nextstate = RESET_CYCLE_2;
        end
      end
      state[READ_CYCLE_bit]: nextstate = READ_CYCLE_2;
      state[READ_CYCLE_2_bit]:begin
        if(addra==9'h1ff)begin
          nextstate = IDLE;
        end else begin
          nextstate = READ_CYCLE_2;
        end
      end
      state[WRITE_CYCLE_bit]: nextstate = WRITE_CYCLE_2;
      state[WRITE_CYCLE_2_bit]: nextstate = WRITE_CYCLE_3;
      state[WRITE_CYCLE_3_bit]: nextstate = WRITE_CYCLE_4;
      state[WRITE_CYCLE_4_bit]: nextstate = IDLE;
      default: nextstate = IDLE;
      endcase
    end

    always @(posedge clk) begin
      if(rst)begin
      addra <= 9'b0;
      wea <= 1'b0;
      dina <= 32'b0;
      hit_add_done <= 1'b0;end
      else begin
      addra <= 9'b0;
      wea <= 1'b0;
      dina <= 32'b0;
      hit_add_done <= 1'b0;
      case(1'b1)
      nextstate[IDLE_bit]:begin end
      nextstate[RESET_CYCLE_bit]:begin
        addra <= 9'b0;
        wea <= 1'b1;
        dina <= 32'b0;
      end
      nextstate[RESET_CYCLE_2_bit]:begin
        addra <= addra + 9'b1;
        wea <= 1'b1; 
        dina <= 32'b0;       
      end
      nextstate[READ_CYCLE_bit]:begin
      end
      nextstate[READ_CYCLE_2_bit]:begin
        addra <= addra + 9'b1;
      end
      nextstate[WRITE_CYCLE_bit]:begin
        addra <= hit_latency_r[8:0];
      end
      nextstate[WRITE_CYCLE_2_bit]:begin 
        addra <= hit_latency_r[8:0];
      end
      nextstate[WRITE_CYCLE_3_bit]:begin 
        addra <= hit_latency_r[8:0];
      end
      nextstate[WRITE_CYCLE_4_bit]:begin
        addra <= hit_latency_r[8:0];
        wea <= 1'b1;
        dina <= douta+1'b1;
        hit_add_done <= 1'b1;
      end
      endcase
      end
  end
  
  
 // ila_latency_counter ila_latency_counter_inst (
 //   .clk(clk), // input wire clk
   
 //   .probe0(rst_content), // input wire [0:0]  probe0  
 //   .probe1(hit_chosen), // input wire [0:0]  probe1 
 //   .probe2(hit_latency), // input wire [31:0]  probe2         
 //   .probe3(start_read), // input wire [0:0]  probe2 
 //   .probe4(wea), // input wire [0:0]  probe2 
 //   .probe5(addra),// input wire [14:0]  probe2 
 //   .probe6(dina),// input wire [31:0]  probe2 
 //   .probe7(douta),// input wire [31:0]  probe2 
 //   .probe8(nextstate),//input wire [2:0]  probe2 
 //   .probe9(state),//input wire [2:0]  probe2 
 //   .probe10(hit_latency_level),//input wire [0:0]  probe2 
 //   .probe11(hit_latency_r[14:0])//input wire [14:0]  probe2 
 //   );
endmodule
