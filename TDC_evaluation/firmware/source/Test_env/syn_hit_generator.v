/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : syn_hit_generator.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on 2018-01-17 11:47:00
//  Note       : 
//     
module syn_hit_generator(
input clk,
input enable,
input [13:0] global_counter,
input [13:0] hit_0_position,hit_1_position,hit_2_position,hit_3_position,hit_4_position,hit_5_position,hit_6_position,hit_7_position,
input [13:0] hit_8_position,hit_9_position,hit_10_position,hit_11_position,hit_12_position,hit_13_position,hit_14_position,hit_15_position,
input [13:0] hit_16_position,hit_17_position,hit_18_position,hit_19_position,hit_20_position,hit_21_position,hit_22_position,hit_23_position,
output[23:0] hit
	);

syn_hit_generator_single syn_hit_generator_single_inst[23:0] 
(  .clk(clk),
   .enable(enable),
   .global_counter(global_counter),
   .hit_position({hit_23_position,
				  hit_22_position,
				  hit_21_position,
				  hit_20_position,
				  hit_19_position,
				  hit_18_position,
				  hit_17_position,
				  hit_16_position,
				  hit_15_position,
				  hit_14_position,
				  hit_13_position,
				  hit_12_position,
				  hit_11_position,
				  hit_10_position,
				  hit_9_position,
				  hit_8_position,
				  hit_7_position,
				  hit_6_position,
				  hit_5_position,
				  hit_4_position,
				  hit_3_position,
				  hit_2_position,
				  hit_1_position,
				  hit_0_position}),
   .hit(hit)
	);

endmodule