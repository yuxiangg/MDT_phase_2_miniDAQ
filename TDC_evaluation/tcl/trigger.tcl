

set_property top Test_env_top_trigger_test [current_fileset]

add_files -fileset constrs_1 -norecurse $constrain_dir/constrain_test_env_trigger_position.xdc

create_ip -name vio -vendor xilinx.com -library ip -version 3.0 -module_name vio_syn_hit_ttc
set_property -dict [list CONFIG.C_NUM_PROBE_IN {0} CONFIG.C_NUM_PROBE_OUT {29} CONFIG.C_PROBE_OUT4_WIDTH {14} CONFIG.C_PROBE_OUT5_WIDTH {14} CONFIG.C_PROBE_OUT6_WIDTH {14} CONFIG.C_PROBE_OUT7_WIDTH {14} CONFIG.C_PROBE_OUT8_WIDTH {14} CONFIG.C_PROBE_OUT9_WIDTH {14} CONFIG.C_PROBE_OUT10_WIDTH {14} CONFIG.C_PROBE_OUT11_WIDTH {14} CONFIG.C_PROBE_OUT12_WIDTH {14} CONFIG.C_PROBE_OUT13_WIDTH {14} CONFIG.C_PROBE_OUT14_WIDTH {14} CONFIG.C_PROBE_OUT15_WIDTH {14} CONFIG.C_PROBE_OUT16_WIDTH {14} CONFIG.C_PROBE_OUT17_WIDTH {14} CONFIG.C_PROBE_OUT18_WIDTH {14} CONFIG.C_PROBE_OUT19_WIDTH {14} CONFIG.C_PROBE_OUT20_WIDTH {14} CONFIG.C_PROBE_OUT21_WIDTH {14} CONFIG.C_PROBE_OUT22_WIDTH {14} CONFIG.C_PROBE_OUT23_WIDTH {14} CONFIG.C_PROBE_OUT24_WIDTH {14} CONFIG.C_PROBE_OUT25_WIDTH {14} CONFIG.C_PROBE_OUT26_WIDTH {14} CONFIG.C_PROBE_OUT27_WIDTH {14} CONFIG.C_PROBE_OUT28_WIDTH {14} CONFIG.C_EN_PROBE_IN_ACTIVITY {0}] [get_ips vio_syn_hit_ttc]


set_property is_enabled false [get_files  */MDT_TDC_ver1_evaluation/filmware/source/Test_env/Test_env_top.v]
set_property is_enabled false [get_files  */MDT_TDC_ver1_evaluation/filmware/source/Test_env/hit_recieve_statistic_chnls.v]
set_property is_enabled false [get_files  */MDT_TDC_ver1_evaluation/filmware/source/ethernet_mac_interface/ethernet_mac_interface.v]
set_property is_enabled false [get_files  */MDT_TDC_ver1_evaluation/filmware/source/ethernet_mac_interface/tri_mode_ethernet_mac_0_example_design_resets.v]
set_property is_enabled false [get_files  */MDT_TDC_ver1_evaluation/filmware/source/Test_env/hit_recieve_latency_statistic.v]
set_property is_enabled false [get_files  */MDT_TDC_ver1_evaluation/filmware/source/ethernet_mac_interface/tri_mode_ethernet_mac_0_fifo_block.v]
set_property is_enabled false [get_files  */MDT_TDC_ver1_evaluation/filmware/source/ethernet_mac_interface/clock/tri_mode_ethernet_mac_0_example_design_clocks.v]
set_property is_enabled false [get_files  */MDT_TDC_ver1_evaluation/filmware/source/ethernet_mac_interface/tri_mode_ethernet_mac_0_support.v]
set_property is_enabled false [get_files  */MDT_TDC_ver1_evaluation/filmware/source/ethernet_mac_interface/tri_mode_ethernet_mac_0_support_resets.v]
set_property is_enabled false [get_files  */MDT_TDC_ver1_evaluation/filmware/source/ethernet_mac_interface/fifo/tri_mode_ethernet_mac_0_ten_100_1g_eth_fifo.v]
set_property is_enabled false [get_files  */MDT_TDC_ver1_evaluation/filmware/source/ethernet_mac_interface/fifo/tri_mode_ethernet_mac_0_tx_client_fifo.v]
set_property is_enabled false [get_files  */MDT_TDC_ver1_evaluation/filmware/source/ethernet_mac_interface/fifo/tri_mode_ethernet_mac_0_rx_client_fifo.v]
set_property is_enabled false [get_files  */MDT_TDC_ver1_evaluation/filmware/source/Test_env/packet_interface.v]
set_property is_enabled false [get_files  */MDT_TDC_ver1_evaluation/filmware/source/Test_env/hit_recieve_latency_counter_statistics.v]
set_property is_enabled false [get_files  */MDT_TDC_ver1_evaluation/filmware/source/Test_env/hit_recieve_statistic.v]
set_property is_enabled false [get_files  */MDT_TDC_ver1_evaluation/filmware/source/Test_env/packet_recombine.v]
set_property is_enabled false [get_files  */MDT_TDC_ver1_evaluation/filmware/source/ethernet_mac_interface/tri_mode_ethernet_mac_0_axi_lite_sm.v]
set_property is_enabled false [get_files  */MDT_TDC_ver1_evaluation/filmware/source/ethernet_mac_interface/fifo/tri_mode_ethernet_mac_0_bram_tdp.v]
set_property is_enabled false [get_files  */MDT_TDC_ver1_evaluation/filmware/source/ethernet_mac_interface/clock/tri_mode_ethernet_mac_0_clk_wiz.v]
set_property is_enabled false [get_files  */MDT_TDC_ver1_evaluation/filmware/source/ethernet_mac_interface/common/tri_mode_ethernet_mac_0_reset_sync.v]
set_property is_enabled false [get_files  */MDT_TDC_ver1_evaluation/filmware/source/ethernet_mac_interface/tri_mode_ethernet_mac_0_support_clocking.v]
set_property is_enabled false [get_files  */MDT_TDC_ver1_evaluation/filmware/source/ethernet_mac_interface/common/tri_mode_ethernet_mac_0_sync_block.v]
set_property is_enabled false [get_files  */MDT_TDC_ver1_evaluation/filmware/source/Test_env/data_sync.v]
set_property is_enabled false [get_files  */MDT_TDC_ver1_firmware/MDT_TDC_ver1_firmware.srcs/sources_1/ip/blk_mem_latency/blk_mem_latency.xci]
set_property is_enabled false [get_files  */MDT_TDC_ver1_firmware/MDT_TDC_ver1_firmware.srcs/sources_1/ip/fifo_hit_stat/fifo_hit_stat.xci]
set_property is_enabled false [get_files  */MDT_TDC_ver1_firmware/MDT_TDC_ver1_firmware.srcs/sources_1/ip/fifo_latency_data/fifo_latency_data.xci]
set_property is_enabled false [get_files  */MDT_TDC_ver1_firmware/MDT_TDC_ver1_firmware.srcs/sources_1/ip/fifo_latency_hit/fifo_latency_hit.xci]
set_property is_enabled false [get_files  */MDT_TDC_ver1_firmware/MDT_TDC_ver1_firmware.srcs/sources_1/ip/fifo_sync_coarse_counter/fifo_sync_coarse_counter.xci]
set_property is_enabled false [get_files  */MDT_TDC_ver1_firmware/MDT_TDC_ver1_firmware.srcs/sources_1/ip/fifo_sync_hit_info/fifo_sync_hit_info.xci]
set_property is_enabled false [get_files  */MDT_TDC_ver1_firmware/MDT_TDC_ver1_firmware.srcs/sources_1/ip/ila_latency_check/ila_latency_check.xci]
set_property is_enabled false [get_files  */MDT_TDC_ver1_firmware/MDT_TDC_ver1_firmware.srcs/sources_1/ip/tdc_data_fifo/tdc_data_fifo.xci]
set_property is_enabled false [get_files  */MDT_TDC_ver1_firmware/MDT_TDC_ver1_firmware.srcs/sources_1/ip/tri_mode_ethernet_mac_0/tri_mode_ethernet_mac_0.xci]
set_property is_enabled false [get_files  */MDT_TDC_ver1_firmware/MDT_TDC_ver1_firmware.srcs/sources_1/ip/vio_ethernet_enable/vio_ethernet_enable.xci]
set_property is_enabled false [get_files  */MDT_TDC_ver1_firmware/MDT_TDC_ver1_firmware.srcs/sources_1/ip/vio_hit_stat_check/vio_hit_stat_check.xci]
set_property is_enabled false [get_files  */MDT_TDC_ver1_firmware/MDT_TDC_ver1_firmware.srcs/sources_1/ip/vio_latency_control/vio_latency_control.xci]


set_property is_enabled false [get_files  */MDT_TDC_ver1_evaluation/filmware/constrain/constrain_test_env_position.xdc]
set_property is_enabled false [get_files  */MDT_TDC_ver1_evaluation/filmware/source/ethernet_mac_interface/constrain/tri_mode_ethernet_mac_0_example_design.xdc]
set_property is_enabled false [get_files  */MDT_TDC_ver1_evaluation/filmware/source/ethernet_mac_interface/constrain/tri_mode_ethernet_mac_0_user_phytiming.xdc]

