set_param general.maxThreads 8


# Set the reference directory for source file relative paths (by default the value is script directory path)
set tcl_dir [file dirname [info script]]
set origin_dir "$tcl_dir/.."
set source_dir "$origin_dir/filmware/source"
set constrain_dir "$origin_dir/filmware/constrain"
set firmware_dir "$origin_dir/../MDT_TDC_ver1_firmware"


add_file $source_dir/ethernet_mac_interface
add_file $source_dir/Test_env/packet_interface.v
update_compile_order -fileset sources_1

add_files -fileset constrs_1 -norecurse $source_dir/ethernet_mac_interface/constrain/tri_mode_ethernet_mac_0_example_design.xdc
add_files -fileset constrs_1 -norecurse $source_dir/ethernet_mac_interface/constrain/tri_mode_ethernet_mac_0_user_phytiming.xdc

create_ip -name vio -vendor xilinx.com -library ip -version 3.0 -module_name vio_ethernet_enable
set_property -dict [list CONFIG.C_NUM_PROBE_IN {0} CONFIG.C_NUM_PROBE_OUT {2} CONFIG.C_EN_PROBE_IN_ACTIVITY {0}] [get_ips vio_ethernet_enable]

create_ip -name tri_mode_ethernet_mac -vendor xilinx.com -library ip -version 9.0 -module_name tri_mode_ethernet_mac_0
set_property -dict [list CONFIG.ETHERNET_BOARD_INTERFACE {rgmii} CONFIG.MDIO_BOARD_INTERFACE {mdio_io} CONFIG.Management_Frequency {100} CONFIG.Frame_Filter {false} CONFIG.Statistics_Counters {true} CONFIG.ETHERNET_BOARD_INTERFACE {rgmii} CONFIG.MDIO_BOARD_INTERFACE {mdio_io} CONFIG.Physical_Interface {RGMII} CONFIG.MAC_Speed {Tri_speed} CONFIG.Make_MDIO_External {true} CONFIG.Number_of_Table_Entries {0}] [get_ips tri_mode_ethernet_mac_0]
