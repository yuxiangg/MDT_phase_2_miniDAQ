import serial
from TDC_config_low_level_function import *
from serial_config_tdc import *
from TDCreg import *
ser = serial.Serial( port='COM9', baudrate = 115200, bytesize = serial.EIGHTBITS,parity =serial.PARITY_EVEN, stopbits = serial.STOPBITS_ONE, timeout=0.01)
TDC0 = TDCreg(ser)

tdc_high_speed(ser)
update_data_length(ser, 4)
tdc_triggerless_mode(ser)
update_input_deserial_para(ser)
reselect_input_deserial(ser)


verificate_pass_TD_CODE = verificate_ID_CODE(ser)
verificate_pass_setup0 = verificate_setup0(ser)
verificate_pass_setup1 = verificate_setup1(ser)
verificate_pass_setup2 = verificate_setup2(ser)
verificate_pass_control0 = verificate_control0(ser)
verificate_pass_control1 = verificate_control1(ser)
# verificate_pass_spi = verificate_spi(ser)


trst_0(ser)
trst_1(ser)

tdc_master_reset_1(ser)
tdc_master_reset_0(ser)


update_hit_width(ser,1)
update_inv_hit(ser,'notinv')
update_hit_delay(ser,0)
update_hit_mask_hit(ser,'\xff\xff\xff')


#===========================    Leading  edge    =============================================

update_data_length(ser, 3)

TDC0.enable_leading[0] = '1'
TDC0.updata_setup_0()
tdc_master_reset_1(ser)
tdc_master_reset_0(ser)






start_single_hit(ser)

# Note:
# 1. now the data have 24 bit. 


# fifodata[23:19] : Chnl ID
# fifodata[16:0] : measurment


#===========================    TDC ID    =============================================

update_data_length(ser, 3)
trst_0(ser)
trst_1(ser)


TDC0.enable_TDC_ID[0] = '1'
TDC0.updata_setup_0()


# Check the original ID


TDC0.TDC_ID[0] = '1111111111111111111'
TDC0.updata_setup_0()

tdc_master_reset_1(ser)
tdc_master_reset_0(ser)

# Check the updated ID

#datafifo[18:0] = TDC_ID

TDC0.TDC_ID[0] = '0000000000000000000'

#===========================    Leading and faling edge combined output   =============================================

update_data_length(ser, 5)

TDC0.enable_leading[0] = '0'
TDC0.enable_pair[0] = '0'
TDC0.updata_setup_0()
tdc_master_reset_1(ser)
tdc_master_reset_0(ser)






start_single_hit(ser)


# Note:
# 1. now the data have 40 bit. 
# 2. the first 24 bits ate the same 24bits  as the deafult mode. 
# 3. the last 16 bits are the measurment of trailing edge

# fifodata[39:35] : Chnl ID
# fifodata[32:16] : leading measurment (you may want to ignore the highest bit if you want to compare with the trailing edge result)
# fifodata[15:0]  : traling edge measurment



#===========================    Full resolution mode   =============================================
start_single_hit(ser)

update_hit_width(ser,40)
start_single_hit(ser)










TDC0.full_width_res[0] = '1'
TDC0.updata_setup_0()
tdc_master_reset_1(ser)
tdc_master_reset_0(ser)
update_data_length(ser, 5)



start_single_hit(ser)

update_hit_width(ser,42)
start_single_hit(ser)


TDC0.combine_time_out_config[0] = '0000110010'
# TDC0.combine_time_out_config[0] = '1000110010'
TDC0.update_setup_1()
tdc_master_reset_1(ser)
tdc_master_reset_0(ser)
update_data_length(ser, 5)

start_single_hit(ser)











#===========================    neg pulse output     =============================================


update_hit_width(ser,1)
update_inv_hit(ser,'inv')
update_hit_delay(ser,0)
update_hit_mask_hit(ser,'\xff\xff\xff')

start_single_hit(ser)


# Check the hit if is high most of the time. 
# give one hit and see if the width is ff


TDC0.rising_is_leading[0] = '000000000000000000000000'
TDC0.updata_setup_0()



# updating using falling as leading 
# give hit one more check if the width is not ff.

start_single_hit(ser)

#===========================    full band width witn insert idle packet     =============================================

update_hit_interval(ser,7)
hit_start(ser)
# hit_stop(ser)

# check K code. Shouldn't find any k = 1 at that moment.

TDC0.enable_insert[0] = '1'
TDC0.updata_setup_0()