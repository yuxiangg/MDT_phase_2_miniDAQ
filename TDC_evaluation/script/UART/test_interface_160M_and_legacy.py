import serial
from TDC_config_low_level_function import *
from serial_config_tdc import *
from TDCreg import *
ser = serial.Serial( port='COM5', baudrate = 115200, bytesize = serial.EIGHTBITS,parity =serial.PARITY_EVEN, stopbits = serial.STOPBITS_ONE, timeout=0.1)
TDC0 = TDCreg(ser)

verificate_pass_TD_CODE = verificate_ID_CODE(ser)
verificate_pass_setup0 = verificate_setup0(ser)
verificate_pass_setup1 = verificate_setup1(ser)
verificate_pass_setup2 = verificate_setup2(ser)
verificate_pass_control0 = verificate_control0(ser)
verificate_pass_control1 = verificate_control1(ser)
verificate_pass_spi = verificate_spi(ser)



##########################################  320M   #####################################################################
trst_0(ser)
trst_1(ser)
TDC0.phase_clk160[0]='10000'
TDC0.phase_clk320_0[0]='1100'
TDC0.phase_clk320_1[0]='1000'
TDC0.update_control_1()
tdc_master_reset_1(ser)
tdc_master_reset_0(ser)

tdc_high_speed(ser)
update_data_length(ser, 4)
tdc_triggerless_mode(ser)
update_input_deserial_para(ser)
reselect_input_deserial(ser)
##########################################  160M   #####################################################################
trst_0(ser)
trst_1(ser)
TDC0.enable_high_speed[0]='0'
TDC0.updata_setup_0()

tdc_master_reset_1(ser)
tdc_master_reset_0(ser)

tdc_low_speed(ser)
update_data_length(ser, 4)
tdc_triggerless_mode(ser)
update_input_deserial_para(ser)
reselect_input_deserial(ser)

#check the idle data line. check if the data width doubled.

update_hit_width(ser,1)
update_inv_hit(ser,'notinv')
update_hit_delay(ser,0)
update_hit_mask_hit(ser,'\xff\xff\xff')
start_single_hit(ser)

#trigger data packet to see if all 24 were hitted.

##########################################  Legacy  #####################################################################

trst_0(ser)
trst_1(ser)
TDC0.enable_legacy[0]='1'
TDC0.updata_setup_0()

tdc_master_reset_1(ser)
tdc_master_reset_0(ser)


# take a look at the data line, they should be constant


update_hit_width(ser,1)
update_inv_hit(ser,'notinv')
update_hit_delay(ser,0)
update_hit_mask_hit(ser,'\x80\x00\x00')
start_single_hit(ser)

# Check the AMT manual for the decoding 
# Sending out one channel each time and check the channel ID.