import serial

from TDC_config_low_level_function import *


ser = serial.Serial( port='COM6', baudrate = 115200, bytesize = serial.EIGHTBITS,parity =serial.PARITY_EVEN, stopbits = serial.STOPBITS_ONE, timeout=0.01)
get_fifo_reg(ser)