import serial
from TDC_config_low_level_function import *
from serial_config_tdc import *
from TDCreg import *
ser = serial.Serial( port='COM5', baudrate = 115200, bytesize = serial.EIGHTBITS,parity =serial.PARITY_EVEN, stopbits = serial.STOPBITS_ONE, timeout=0.1)
TDC0 = TDCreg(ser)

verificate_pass_TD_CODE = verificate_ID_CODE(ser)
verificate_pass_setup0 = verificate_setup0(ser)
verificate_pass_setup1 = verificate_setup1(ser)
verificate_pass_setup2 = verificate_setup2(ser)
verificate_pass_control0 = verificate_control0(ser)
verificate_pass_control1 = verificate_control1(ser)
# verificate_pass_spi = verificate_spi(ser)


#ASD_config(ser, 5, '\x15')


trst_0(ser)
trst_1(ser)
TDC0.phase_clk160[0]='10000'
TDC0.phase_clk320_0[0]='1100'
TDC0.phase_clk320_1[0]='1000'
TDC0.update_control_1()
tdc_master_reset_1(ser)
tdc_master_reset_0(ser)

tdc_high_speed(ser)
update_data_length(ser, 4)
tdc_triggerless_mode(ser)
update_input_deserial_para(ser)
reselect_input_deserial(ser)

# 14 15
TDC0.channel_enable_r[0]='000000001100000000000000'
TDC0.channel_enable_f[0]='000000001100000000000000'
TDC0.updata_setup_0()
TDC0.updata_setup_0()
tdc_master_reset_1(ser)
tdc_master_reset_0(ser)

# 13 14
TDC0.channel_enable_r[0]='000000000110000000000000'
TDC0.channel_enable_f[0]='000000000110000000000000'
TDC0.updata_setup_0()
TDC0.updata_setup_0()
tdc_master_reset_1(ser)
tdc_master_reset_0(ser)

# 12 13
TDC0.channel_enable_r[0]='000000000011000000000000'
TDC0.channel_enable_f[0]='000000000011000000000000'
TDC0.updata_setup_0()
TDC0.updata_setup_0()
tdc_master_reset_1(ser)
tdc_master_reset_0(ser)

# 11 12
TDC0.channel_enable_r[0]='000000000001100000000000'
TDC0.channel_enable_f[0]='000000000001100000000000'
TDC0.updata_setup_0()
TDC0.updata_setup_0()
tdc_master_reset_1(ser)
tdc_master_reset_0(ser)

# 10 11
TDC0.channel_enable_r[0]='000000000000110000000000'
TDC0.channel_enable_f[0]='000000000000110000000000'
TDC0.updata_setup_0()
TDC0.updata_setup_0()
tdc_master_reset_1(ser)
tdc_master_reset_0(ser)

#9 10
TDC0.channel_enable_r[0]='000000000000011000000000'
TDC0.channel_enable_f[0]='000000000000011000000000'
TDC0.updata_setup_0()
TDC0.updata_setup_0()
tdc_master_reset_1(ser)
tdc_master_reset_0(ser)

#8  9
TDC0.channel_enable_r[0]='000000000000001100000000'
TDC0.channel_enable_f[0]='000000000000001100000000'
TDC0.updata_setup_0()
TDC0.updata_setup_0()
tdc_master_reset_1(ser)
tdc_master_reset_0(ser)

TDC0.channel_enable_r[0]='000000000000000000000000'
TDC0.channel_enable_f[0]='000000000000000000000000'
TDC0.updata_setup_0()
TDC0.updata_setup_0()
tdc_master_reset_1(ser)
tdc_master_reset_0(ser)


TDC0.channel_enable_r[0]='000000001111111100000000'
TDC0.channel_enable_f[0]='000000001111111100000000'
TDC0.updata_setup_0()
TDC0.updata_setup_0()
tdc_master_reset_1(ser)
tdc_master_reset_0(ser)





TDC0.rising_is_leading[0]='111111111111111111111111'#rising
# TDC0.rising_is_leading[0]='111111111111111111111111'#falling
TDC0.channel_enable_r[0] ='111111111111111111111111'
TDC0.channel_enable_f[0] ='111111111111111111111111'
TDC0.updata_setup_0()
TDC0.updata_setup_0()
tdc_master_reset_1(ser)
tdc_master_reset_0(ser)

# TDC0.rising_is_leading[0]='111111111111111111111111'#rising
TDC0.rising_is_leading[0]='000000000000000000000000'#falling
TDC0.channel_enable_r[0] ='111111111111111111111111'
TDC0.channel_enable_f[0] ='111111111111111111111111'
TDC0.updata_setup_0()
TDC0.updata_setup_0()
tdc_master_reset_1(ser)
tdc_master_reset_0(ser)






#for debug mode
TDC0.channel_enable_r[0]='000000000000000000000000'
TDC0.channel_enable_f[0]='000000000000000000000000'
TDC0.channel_data_debug[0]='1'
TDC0.updata_setup_0()
TDC0.updata_setup_0()
update_data_length(ser, 3)
tdc_master_reset_1(ser)
tdc_master_reset_0(ser)


#for timging
TDC0.channel_enable_r[0]='000000000000000000000001'
TDC0.channel_enable_f[0]='000000000000000000000001'
TDC0.channel_data_debug[0]='1'
update_data_length(ser, 3)
TDC0.updata_setup_0()
TDC0.updata_setup_0()

#TDC0.phase_clk320_0[0]='0100'
#TDC0.phase_clk320_1[0]='0000'



tdc_master_reset_1(ser)
tdc_master_reset_0(ser)

# get_ePLLconfig_reg(ser)
# get_ePLLconfig_reg(ser)
# #for ePLL_control
# TDC0.phase_clk160[0] = '00000'
# TDC0.phase_clk320_0[0] = '0000'
# TDC0.phase_clk320_1[0] = '0000'
# TDC0.phase_clk320_2[0] = '0000'
# TDC0.ePllRes[0] = '0000'
# TDC0.ePllIcp[0] = '0000'
# TDC0.ePllCap[0] = '00'
# ePLLconfig_reg_ref = {}
# ePLLconfig_reg_ref['ePllCapA'] = TDC0.ePllCap[0]
# ePLLconfig_reg_ref['ePllIcpA'] = TDC0.ePllIcp[0]
# ePLLconfig_reg_ref['ePllResA'] = TDC0.ePllRes[0]
# ePLLconfig_reg_ref['phase_clk320_2'] = TDC0.phase_clk320_2[0]
# ePLLconfig_reg_ref['phase_clk320_1'] = TDC0.phase_clk320_1[0]
# ePLLconfig_reg_ref['phase_clk320_0'] = TDC0.phase_clk320_0[0]
# ePLLconfig_reg_ref['phase_clk160'] = TDC0.phase_clk160[0]
# TDC0.update_control_1()
# ePLL_config=get_ePLLconfig_reg(ser)
# print(ePLL_config)
# print(ePLLconfig_reg_ref)
# trst_0(ser)
# trst_1(ser)
#
#
tdc_high_speed(ser)
update_data_length(ser, 4)
tdc_triggerless_mode(ser)
#tdc_trigger_mode(ser)
update_input_deserial_para(ser)
reselect_input_deserial(ser)
#
update_hit_width(ser,3)
update_inv_hit(ser,'notinv')
update_hit_delay(ser,0)
update_hit_mask_hit(ser,'\xFF\xFF\xFF')
# start_single_hit(ser)


update_hit_interval(ser,500)
hit_start(ser)
hit_stop(ser)
#
# new TTC master reset single
TDC0.reset_setup_0()
TDC0.enable_new_ttc[0]='1'
TDC0.enable_master_reset_code[0] = '1'
TDC0.updata_setup_0()
update_ttc_delay(ser, 1)
ttc_mode_select(ser, 1)
update_new_ttc_code(ser, '\x09')
single_TTC(ser)
# new TTC master reset repeat
update_ttc_interval(ser, 50)
start_ttc(ser)
stop_ttc(ser)

# legacy TTC master reset single
TDC0.reset_setup_0()
TDC0.enable_new_ttc[0]='0'
TDC0.enable_master_reset_code[0] = '1'
TDC0.updata_setup_0()
update_ttc_delay(ser, 1)
ttc_mode_select(ser, 0)
enable_ttc_master_reset(ser)
single_TTC(ser)
# legacy TTC master reset repeat
update_ttc_interval(ser, 50)
start_ttc(ser)
stop_ttc(ser)


#bcr auto roll over
TDC0.reset_setup_0()
TDC0.auto_roll_over[0] = '1'
TDC0.roll_over[0] = '000001000000'
TDC0.bunch_offset[0] = '000000000000'
TDC0.updata_setup_0()
TDC0.update_setup_1()

update_hit_width(ser,1)
update_inv_hit(ser,'notinv')
update_hit_delay(ser,0)
update_hit_mask_hit(ser,'\x00\x00\x01')
# start_single_hit(ser)


update_hit_interval(ser,18)
hit_start(ser)
hit_stop(ser)


#
# trst_1(ser)
# trst_0(ser)
# get_ePLLconfig_reg(ser)
# get_ePLLconfig_reg(ser)
# #spi_test='\x04\x2f\xff\xff\xff\xff\xff\xff\xff\xff\xf0\xaa\xaa\x82\x01\x40\x80\x7f\xff\xff\x80\x07\xce\x00\x00\x0f\x98\xce\x50\x0e\x60\x02\x10\x80\x92\x24\x89\x20'
# update_reg(0, '\xce\x00\x00\x0f\x98\xce\x50\x0e\x60\x02\x10\x80\x92\x24\x89\x20', ser)
# update_reg(1, '\xff\xff\xff\xff\xf0\xaa\xaa\x82\x01\x40\x80\x7f\xff\xff\x80\x07', ser)
# update_reg(2, '\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x04\x2f\xff\xff\xff\xff', ser)
# update_reg(3, '\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00', ser)
# update_config_period(0, ser)
# update_bit_length(300,ser)
# start_action(ser)