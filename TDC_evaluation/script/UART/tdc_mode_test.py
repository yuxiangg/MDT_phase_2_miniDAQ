import serial

from TDC_config_low_level_function import *


ser = serial.Serial( port='COM6', baudrate = 115200, bytesize = serial.EIGHTBITS,parity =serial.PARITY_EVEN, stopbits = serial.STOPBITS_ONE, timeout=0.01)

#trst_0(ser)
#trst_1(ser)
#setup_0_bin='0000010000101111111111111111111111111111111111111111111111111111111111111111111111111111010101010101010010000010000'#pairmode defalut
#setup_0_bin='0000010001001111111111111111111111111111111111111111111111111111111111111111111111111111010101010101010010000010000'#edgemode
#setup_0_bin='0000010000001111111111111111111111111111111111111111111111111111111111111111111111111111010101010101010010000010000'#combine
#setup_0_bin='0000010000101111111111111111111111111111111111111111111111111111111111111111111111111111010101010101010010100010000'#combine
#setup_0_bin ='0000010011001111111111111111111111111111111111111111111111111111111111111111111111111111010101010101010010000010000'#debug

enable_new_ttc = '0'
enable_master_reset_code ='1'
enable_direct_bunch_reset = '1'#'0'
enable_direct_event_reset = '0'
enable_direct_trigger = '0'
TTC_setup=enable_new_ttc+enable_master_reset_code+enable_direct_bunch_reset+enable_direct_event_reset+enable_direct_trigger
auto_roll_over = '1'#'1'
bypass_bcr_distribution = '0'
bcr_distribute=auto_roll_over+bypass_bcr_distribution
enable_trigger = '0'
channel_data_debug = '0'
enable_leading = '0'
enable_pair = '1'
enbale_fake_hit = '0'
rising_is_leading = '111111111111111111111111'
channel_enable_r = '111111111111111111111111'
channel_enable_f = '111111111111111111111111'
tdc_mode = enable_trigger + channel_data_debug + enable_leading + enable_pair + enbale_fake_hit + rising_is_leading + channel_enable_r + channel_enable_f
TDC_ID = '1111010101010101010'
enable_trigger_timeout = '0'
enable_high_speed = '1'
enable_legacy = '0'
full_width_res = '0'
width_select = '000'
enable_8b10b = '1'
enable_insert = '0'#0
enable_error_packet = '0'#0
enable_TDC_ID = '0'
enable_error_notify = '0'
readout = enable_trigger_timeout + enable_high_speed + enable_legacy + full_width_res + width_select + enable_8b10b + enable_insert + enable_error_packet + enable_TDC_ID + enable_error_notify
setup_0_bin= TTC_setup + bcr_distribute + tdc_mode + TDC_ID + readout
setup_0_align='00000'
setup_0=bin_to_hex(setup_0_align+setup_0_bin)

print(str_to_hex(setup_0,'_'))
update_reg(0,'\x00'+setup_0,ser)
update_reg(1,'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00',ser)
update_reg(2,'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00',ser)
update_reg(3,'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00',ser)
update_config_period(0,ser)
update_JTAG_inst('\x12',ser)
update_bit_length(115,ser)
start_action(ser)
start_action(ser)

##
#setup_1_bin='0000101000000100000000111111111111111111111111000000000000111110011100000000000000000000011111'#default
combine_time_out_config = '0000101000'#0000101000:40
fake_hit_time_interval ='000100000000'
syn_packet_number = '111111111111'#111111111111
timer_out = combine_time_out_config + fake_hit_time_interval + syn_packet_number
roll_over = '111111111111' #'111111111111'
coarse_count_offset = '000000000000'
bunch_offset = '000000000000'#111110011100
event_offset = '000000000000'
match_window = '111111111111'#000000011111
coarse_out = roll_over + coarse_count_offset + bunch_offset + event_offset + match_window
setup_1_bin = timer_out + coarse_out
setup_1_align='00'
setup_1=bin_to_hex(setup_1_align+setup_1_bin)
print(str_to_hex(setup_1,'_'))
update_reg(0,'\x00\x00\x00\x00'+setup_1,ser)
update_reg(1,'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00',ser)
update_reg(2,'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00',ser)
update_reg(3,'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00',ser)
update_config_period(0,ser)
update_JTAG_inst('\x03',ser)
update_bit_length(94,ser)
start_action(ser)
start_action(ser)
#ser.close()
##
# verificate control0
#verificate control0 default
rst_ePLL = '0'
reset_jtag_in = '0'
event_teset_jtag_in = '0'
chnl_fifo_overflow_clear = '0'
debug_port_select ='1001'
reset_control0 = rst_ePLL + reset_jtag_in + event_teset_jtag_in + chnl_fifo_overflow_clear + debug_port_select
control0 = bin_to_hex(reset_control0)
print(str_to_hex(control0, '_'))
update_reg(0,'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'+control0,ser)
update_reg(1,'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00',ser)
update_reg(2,'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00',ser)
update_reg(3,'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00',ser)
update_config_period(0,ser)
update_JTAG_inst('\x05',ser)
update_bit_length(8,ser)
start_action(ser)
start_action(ser)


#for ePLL_control
phase_clk160 = '10000'
phase_clk320_0 = '0100'
phase_clk320_1 = '0000'
phase_clk320_2 = '0010'
ePllRes = '1111'
ePllIcp = '0100'
ePllCap = '10'
ePLL_control = phase_clk160 + phase_clk320_0 + phase_clk320_1 + phase_clk320_2 + ePllRes + ePllIcp + ePllCap+ ePllRes + ePllIcp + ePllCap+ ePllRes + ePllIcp + ePllCap
control1_bin = ePLL_control
control1_align='0'
control1=bin_to_hex(control1_align+control1_bin)
print(str_to_hex(control1, '_'))
update_reg(0,'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'+control1,ser)
update_reg(1,'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00',ser)
update_reg(2,'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00',ser)
update_reg(3,'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00',ser)
update_config_period(0,ser)
update_JTAG_inst('\x06',ser)
update_bit_length(47,ser)
start_action(ser)
#start_action(ser)


ser.close()