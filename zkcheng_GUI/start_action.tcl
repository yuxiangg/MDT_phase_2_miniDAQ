open_hw
connect_hw_server
open_hw_target {localhost:3121/xilinx_tcf/Xilinx/000017def52c01}


refresh_hw_device -update_hw_probes false [lindex [get_hw_devices] 0]

set_property PROBES.FILE {C:/Users/gyuxiang/Desktop/gyx_work/TDC/02_CSM/newmezz_firmware/newmezz_firmware.runs/impl_1/top.ltx} [get_hw_devices xc7a200t_0]

current_hw_device [lindex [get_hw_devices] 0]
refresh_hw_device [lindex [get_hw_devices] 0]

startgroup
set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/start_action_asd -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_asd_setup_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/start_action_asd} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_asd_setup_inst"}]]
endgroup
startgroup
set_property OUTPUT_VALUE 0 [get_hw_probes Mezz_config_inst/start_action_asd -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_asd_setup_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/start_action_asd} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_asd_setup_inst"}]]
endgroup



exit