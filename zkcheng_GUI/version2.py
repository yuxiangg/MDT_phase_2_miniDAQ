# -*- coding: utf-8 -*-
# ...........
# @author: ZekangCheng
# email: zkcheng@umich.edu
#............




from UI_define import *

import sys
from scapy.all import *


vivado_path = 'C:\\Xilinx\\Vivado\\2017.2\\bin\\vivado.bat'
GUI_path = 'C:\\Users\\gyuxiang\\Desktop\\gyx_work\\TDC\\02_CSM\\zkcheng_GUI'
probe_path = '{C:/Users/gyuxiang/Desktop/gyx_work/TDC/02_CSM/newmezz_firmware/newmezz_firmware.runs/impl_1/top.ltx}'



class Main(QtWidgets.QMainWindow):
    ui = Ui_MainWindow() # import GUI
    def __init__(self):
        super().__init__()
        self.ui.setupUi(self)
        self.ui.retranslateUi(self)
        self.ui.actionOpen.triggered.connect(self.FILEOPEN)
        self.ui.actionsave_as.triggered.connect(self.FILESAVE)
        self.ui.load.clicked.connect(self.LOAD)
        self.ui.setvalue.clicked.connect(self.Setvalue)
        self.ui.setvalue_2.clicked.connect(self.Setvalue)
        self.ui.setvalue_3.clicked.connect(self.Setvalue)
        self.ui.setvalue_4.clicked.connect(self.Setvalue)
        self.ui.setvalue_5.clicked.connect(self.Setvalue)
        self.ui.default_1.clicked.connect(self.default_1)
        self.ui.default_2.clicked.connect(self.default_2)
        self.ui.default_3.clicked.connect(self.default_3)
        self.ui.default_4.clicked.connect(self.default_4)
        self.ui.default_5.clicked.connect(self.default_5)
        self.ui.Select_1.clicked.connect(self.Select_1)
        self.ui.Select_2.clicked.connect(self.Select_2)
        self.ui.Select_3.clicked.connect(self.Select_3)
        self.ui.operation.clicked.connect(self.datacapture)
        self.ui.showresult.clicked.connect(self.showresult)
        self.ui.user.clicked.connect(self.Fuser)
        self.ui.hptdc_asd.clicked.connect(self.Fhptdc_asd)
        self.ui.hptdc_asdread.clicked.connect(self.Fhptdc_asdread)
        self.ui.asd.clicked.connect(self.Fasd)
        self.ui.control_0.clicked.connect(self.Fcontrol0)
        self.ui.control_1.clicked.connect(self.Fcontrol1)
        self.ui.setup_0.clicked.connect(self.Fsetup0)
        self.ui.setup_1.clicked.connect(self.Fsetup1)
        self.ui.setup_2.clicked.connect(self.Fsetup2)
        self.ui.hptdc_oldasd.clicked.connect(self.Fhptdc_oldasd)
        self.ui.oldasd.clicked.connect(self.Foldasd)
        self.ui.hptdc_oldasdread.clicked.connect(self.Fhptdc_oldasdread)
        # connect pushbutton with functions


    def Fuser(self):
        f = open("start_action.txt",'r')
        fl = open("start_action.tcl",'w')
        start_action_list = ['label_1', 'label_2', 'tabname', 'FPGA_NAME0', 'name']
        locallist_pos = ['0', '1', 'vio_Jtag_inst', self.ui.FPGA_1.text(), 'start_action_user']
        for line in f:
            for j in range(5):
                line = line.replace(start_action_list[j], locallist_pos[j])
            line = line.replace('probe_path', probe_path)
            fl.write(line)
        fl.close()
        import os
        os.system(vivado_path + " -mode tcl -source " + GUI_path + "\\start_action.tcl")
            # "C:\\Users\\zkcheng\\Xilinx\\Vivado\\2017.2\\bin\\vivado.bat -mode tcl -source C:\\Users\\zkcheng\\Desktop\\GUI\\start_action.tcl ")

    def Fhptdc_asd(self):
        f = open("start_action.txt", 'r')
        fl = open("start_action.tcl", 'w')
        start_action_list = ['label_1', 'label_2', 'tabname', 'FPGA_NAME0', 'name']
        locallist_pos = ['0', '1', 'vio_Jtag_inst', self.ui.FPGA_1.text(), 'start_action_hptdc_asd']
        for line in f:
            for j in range(5):
                line = line.replace(start_action_list[j], locallist_pos[j])
            line = line.replace('probe_path', probe_path)
            fl.write(line)
        fl.close()
        import os
        os.system(vivado_path + " -mode tcl -source " + GUI_path + "\\start_action.tcl")

    def Fhptdc_asdread(self):
        f = open("start_action.txt", 'r')
        fl = open("start_action.tcl", 'w')
        start_action_list = ['label_1', 'label_2', 'tabname', 'FPGA_NAME0', 'name']
        locallist_pos = ['0', '1', 'vio_Jtag_inst', self.ui.FPGA_1.text(), 'start_action_hptdc_asdread']
        for line in f:
            for j in range(5):
                line = line.replace(start_action_list[j], locallist_pos[j])
            line = line.replace('probe_path', probe_path)
            fl.write(line)
        fl.close()
        import os
        os.system(vivado_path + " -mode tcl -source " + GUI_path + "\\start_action.tcl")

    def Fasd(self):
        addr = self.ui.lineEdit_36.text()
        f = open("start_action.txt", 'r')
        fl = open("start_action.tcl", 'w')
        start_action_list = ['label_1', 'label_2', 'tabname', 'FPGA_NAME0', 'name']
        locallist_pos = ['1', '0', 'vio_asd_setup_inst', self.ui.FPGA_1.text(), 'start_action_asd']
        for line in f:
            for j in range(5):
                line = line.replace(start_action_list[j], locallist_pos[j])
            line = line.replace('probe_path', probe_path)
            fl.write(line)
        fl.close()
        import os
        os.system(vivado_path + " -mode tcl -source " + GUI_path + "\\start_action.tcl")

    def Fcontrol0(self):
        f = open("start_action.txt", 'r')
        fl = open("start_action.tcl", 'w')
        start_action_list = ['label_1', 'label_2', 'tabname', 'FPGA_NAME0', 'name']
        locallist_pos = ['0', '1', 'vio_control0_inst', self.ui.FPGA_1.text(), 'start_action_control0']
        for line in f:
            for j in range(5):
                line = line.replace(start_action_list[j], locallist_pos[j])
            line = line.replace('probe_path', probe_path)
            fl.write(line)
        fl.close()
        import os
        os.system(vivado_path + " -mode tcl -source " + GUI_path + "\\start_action.tcl")

    def Fcontrol1(self):
        f = open("start_action.txt", 'r')
        fl = open("start_action.tcl", 'w')
        start_action_list = ['label_1', 'label_2', 'tabname', 'FPGA_NAME0', 'name']
        locallist_pos = ['0', '1', 'vio_control1_inst', self.ui.FPGA_1.text(), 'start_action_control1']
        for line in f:
            for j in range(5):
                line = line.replace(start_action_list[j], locallist_pos[j])
            line = line.replace('probe_path', probe_path)
            fl.write(line)
        fl.close()
        import os
        os.system(vivado_path + " -mode tcl -source " + GUI_path + "\\start_action.tcl")

    def Fsetup0(self):
        f = open("start_action.txt", 'r')
        fl = open("start_action.tcl", 'w')
        start_action_list = ['label_1', 'label_2', 'tabname', 'FPGA_NAME0', 'name']
        locallist_pos = ['0', '1', 'vio_setup0_inst', self.ui.FPGA_1.text(), 'start_action_setup0']
        for line in f:
            for j in range(5):
                line = line.replace(start_action_list[j], locallist_pos[j])
            line = line.replace('probe_path', probe_path)
            fl.write(line)
        fl.close()
        import os
        os.system(vivado_path + " -mode tcl -source " + GUI_path + "\\start_action.tcl")

    def Fsetup1(self):
        f = open("start_action.txt", 'r')
        fl = open("start_action.tcl", 'w')
        start_action_list = ['label_1', 'label_2', 'tabname', 'FPGA_NAME0', 'name']
        locallist_pos = ['0', '1', 'vio_setup1_inst', self.ui.FPGA_1.text(), 'start_action_setup1']
        for line in f:
            for j in range(5):
                line = line.replace(start_action_list[j], locallist_pos[j])
            line = line.replace('probe_path', probe_path)
            fl.write(line)
        fl.close()
        import os
        os.system(vivado_path + " -mode tcl -source " + GUI_path + "\\start_action.tcl")

    def Fsetup2(self):
        f = open("start_action.txt", 'r')
        fl = open("start_action.tcl", 'w')
        start_action_list = ['label_1', 'label_2', 'tabname', 'FPGA_NAME0', 'name']
        locallist_pos = ['0', '1', 'vio_setup2_inst', self.ui.FPGA_1.text(), 'start_action_setup2']
        for line in f:
            for j in range(5):
                line = line.replace(start_action_list[j], locallist_pos[j])
            line = line.replace('probe_path', probe_path)
            fl.write(line)
        fl.close()
        import os
        os.system(vivado_path + " -mode tcl -source " + GUI_path + "\\start_action.tcl")

    def Fhptdc_oldasd(self):
        f = open("start_action.txt", 'r')
        fl = open("start_action.tcl", 'w')
        start_action_list = ['label_1', 'label_2', 'tabname', 'FPGA_NAME0', 'name']
        locallist_pos = ['0', '1', 'vio_setup0_inst', self.ui.FPGA_1.text(), 'start_action_hptdc_oldasd']
        for line in f:
            for j in range(5):
                line = line.replace(start_action_list[j], locallist_pos[j])
            line = line.replace('probe_path', probe_path)
            fl.write(line)
        fl.close()
        import os
        os.system(vivado_path + " -mode tcl -source " + GUI_path + "\\start_action.tcl")

    def Fhptdc_oldasdread(self):
        f = open("start_action.txt", 'r')
        fl = open("start_action.tcl", 'w')
        start_action_list = ['label_1', 'label_2', 'tabname', 'FPGA_NAME0', 'name']
        locallist_pos = ['0', '1', 'vio_setup0_inst', self.ui.FPGA_1.text(), 'start_action_hptdc_oldasdread']
        for line in f:
            for j in range(5):
                line = line.replace(start_action_list[j], locallist_pos[j])
            line = line.replace('probe_path', probe_path)
            fl.write(line)
        fl.close()
        import os
        os.system(vivado_path + " -mode tcl -source " + GUI_path + "\\start_action.tcl")

    def Foldasd(self):
        f = open("start_action.txt", 'r')
        fl = open("start_action.tcl", 'w')
        start_action_list = ['label_1', 'label_2', 'tabname', 'FPGA_NAME0', 'name']
        locallist_pos = ['0', '1', 'vio_setup0_inst', self.ui.FPGA_1.text(), 'start_action_oldasd']
        for line in f:
            for j in range(5):
                line = line.replace(start_action_list[j], locallist_pos[j])
            line = line.replace('probe_path', probe_path)
            fl.write(line)
        fl.close()
        import os
        os.system(vivado_path + " -mode tcl -source " + GUI_path + "\\start_action.tcl")

    def datacapture(self): # raw data
        addr = self.ui.amount.text()
        fname, _ = QtWidgets.QFileDialog.getOpenFileName(self , 'Open file', 'c:\\', "Txt files (*.txt)")
        fl = open(fname, "w")
        conf.use_pcap = True
        for pkt in sniff(iface='Ethernet', count= int(addr)):
            fl.write(raw(pkt).hex() + '\n')


    def showresult(self): # click to show result on textEdit

        conf.use_pcap = True
        addr = self.ui.packagesum.text()
        self.ui.textEdit.setPlainText("show results as follow")
        for pkt in sniff(iface='Ethernet', count = int(addr), store = 0):
            string = raw(pkt).hex()
            string1 = []
            for i in range(8):
                string1.append(string[30+i])
            data = (str(bin(int('0x' + ''.join(string1), 16))))
            data1 = []
            for j in range(5):
                data1.append(data[2 + j])
            self.ui.textEdit.append("channel ID: " + str(int(''.join(data1), 2)))
            data2 = []
            for j in range(2):
                data2.append(data[7 + j])
            self.ui.textEdit.append("flag:            " + str(int(''.join(data2), 2)))
            data3 = []
            for k in range(17):
                data3.append(data[9 + k])
            self.ui.textEdit.append("leading:      " + str(int(''.join(data3), 2)))
            data4 = []
            for l in range(8):
                data4.append(data[26 + l])
            self.ui.textEdit.append("width:         " + str(int(''.join(data4), 2))+'\n')

            string2 = []
            for i in range(8):
                string2.append(string[40 + i])
            data_2 = (str(bin(int('0x' + ''.join(string2), 16))))
            data1 = []
            for j in range(5):
                data1.append(data_2[2 + j])
            self.ui.textEdit.append("channel ID: " + str(int(''.join(data1), 2)))
            data2 = []
            for j in range(2):
                data2.append(data_2[7 + j])
            self.ui.textEdit.append("flag:            " + str(int(''.join(data2), 2)))
            data3 = []
            for k in range(17):
                data3.append(data_2[9 + k])
            self.ui.textEdit.append("leading:      " + str(int(''.join(data3), 2)))
            data4 = []
            for l in range(8):
                data4.append(data_2[26 + l])
            self.ui.textEdit.append("width:         " + str(int(''.join(data4), 2))+'\n')


    def default_1(self):
        fname = "default.txt"
        f = open(fname, "r")
        for line in f.readlines():                   # write numbers in txt file into a LIST
            linestr = line.strip()
            linestrlist = linestr.split(" ")
        for i in range(26):
            getattr(getattr(getattr(self, 'ui'), 'lineEdit_' + str(i+1)), 'setText')(linestrlist[i])

    def default_2(self):
        fname = "default.txt"
        f = open(fname, "r")
        for line in f.readlines():                   # write numbers in txt file into a LIST
            linestr = line.strip()
            linestrlist = linestr.split(" ")
        for i in range(18):
            getattr(getattr(getattr(self, 'ui'), 'lineEdit_' + str(i+27)), 'setText')(linestrlist[i+26])

    def default_3(self):
        fname = "default.txt"
        f = open(fname, "r")
        for line in f.readlines():                   # write numbers in txt file into a LIST
            linestr = line.strip()
            linestrlist = linestr.split(" ")
        for i in range(14):
            getattr(getattr(getattr(self, 'ui'), 'lineEdit_' + str(i+47)), 'setText')(linestrlist[i+44])

    def default_4(self):
        fname = "default.txt"
        f = open(fname, "r")
        for line in f.readlines():                   # write numbers in txt file into a LIST
            linestr = line.strip()
            linestrlist = linestr.split(" ")
        for i in range(12):
            getattr(getattr(getattr(self, 'ui'), 'lineEdit_' + str(i+61)), 'setText')(linestrlist[i+58])

    def default_5(self):
        fname = "default.txt"
        f = open(fname, "r")
        for line in f.readlines():                   # rite numbers in txt file into a LIST
            linestr = line.strip()
            linestrlist = linestr.split(" ")
        for i in range(13):
            getattr(getattr(getattr(self, 'ui'), 'lineEdit_' + str(i+81)), 'setText')(linestrlist[i+70])


    def LOAD(self):#run tcl scripts
        import os
        os.system(vivado_path + " -mode tcl -source " + GUI_path + "\\COMMANDS.tcl")
        # os.system("C:\\Users\\zkcheng\\Xilinx\\Vivado\\2017.2\\bin\\vivado.bat -mode tcl -source C:\\Users\\zkcheng\\PycharmProjects\\practice\\programs\\COMMANDS.tcl ")

    def Setvalue(self):
        # set maximium value for variables
        list23 = [2,14,24] # [0:23] hex value
        for i in list23:
            line = getattr(getattr(getattr(self, 'ui'), 'lineEdit_' + str(i)), 'text')()
            value = int('0x'+line,16)
            if value > int('0xffffff',16):
                getattr(getattr(getattr(self, 'ui'), 'lineEdit_' + str(i)), 'setText')('FFFFFF')

        addr13 = self.ui.lineEdit_13.text()
        value = int('0x'+addr13,16)
        if value > int('1111111111111111111', 2):
            self.ui.lineEdit_13.setText(str(7)+'AAAA')

        list2 = [25,37,42,44,81,85,87,90]
        for i in list2:
            line = getattr(getattr(getattr(self, 'ui'), 'lineEdit_' + str(i)), 'text')()
            value = int('0x'+line,16)
            if value > 7 :
                getattr(getattr(getattr(self, 'ui'), 'lineEdit_' + str(i)), 'setText')('7')
        #
        list12 = [32,33]
        for i in list12:
            line = getattr(getattr(getattr(self, 'ui'), 'lineEdit_' + str(i)), 'text')()
            value = int('0x'+line,16)
            if value > int('0x1fff',16):
                getattr(getattr(getattr(self, 'ui'), 'lineEdit_' + str(i)), 'setText')('1FFF')
        #
        list11 = [34,61,62,64,65,66,67,69]
        for i in list11:
            line = getattr(getattr(getattr(self, 'ui'), 'lineEdit_' + str(i)), 'text')()
            value = int('0x'+line,16)
            if value > int('111111111111',2):
                getattr(getattr(getattr(self, 'ui'), 'lineEdit_' + str(i)), 'setText')('FFF')

        list15 = [38,84]
        for i in list15:
            line = getattr(getattr(getattr(self, 'ui'), 'lineEdit_' + str(i)), 'text')()
            value = int('0x'+line,16)
            if value > int('0xffff',16):
                getattr(getattr(getattr(self, 'ui'), 'lineEdit_' + str(i)), 'setText')('FFFF')

        list3 = [40,41,48,54,55,57,58,59,70,86,88]
        for i in list3:
            line = getattr(getattr(getattr(self, 'ui'), 'lineEdit_' + str(i)), 'text')()
            value = int('0x'+line,16)
            if value > 15 :
                getattr(getattr(getattr(self, 'ui'), 'lineEdit_' + str(i)), 'setText')('F')

        list7 = [43,83,89]
        for i in list7:
            line = getattr(getattr(getattr(self, 'ui'), 'lineEdit_' + str(i)), 'text')()
            value = int('0x'+line,16)
            if value > int('0xff',16):
                getattr(getattr(getattr(self, 'ui'), 'lineEdit_' + str(i)), 'setText')('FF')

        addr53 = self.ui.lineEdit_53.text()
        value = int('0x'+addr53,16)
        if value > 3:
            self.ui.lineEdit_53.setText('3')

        addr56 = self.ui.lineEdit_56.text()
        value = int('0x'+addr56,16)
        if value > 31:
            self.ui.lineEdit_56.setText('1F')

        addr63 = self.ui.lineEdit_63.text()
        value = int('0x'+addr63,16)
        if value > int('3FF', 16):
            self.ui.lineEdit_63.setText('3FF')

        addr71 = self.ui.lineEdit_71.text()
        value = int('0x'+addr71,16)
        if value > int('0xFFFFFFFF', 16):
            self.ui.lineEdit_71.setText('FFFFFFFF')
        #
        list1 = [1,3,4,5,6,7,8,9,10,11,12,15,16,17,18,19,20,21,22,23,26,27,28,30,31,36,39,47,49,50,51,52,60,68,72,82,91,92,93]
        for i in list1:
            line = getattr(getattr(getattr(self, 'ui'), 'lineEdit_' + str(i)), 'text')()
            value = int('0x'+line,16)
            if value > 1:
                getattr(getattr(getattr(self, 'ui'), 'lineEdit_' + str(i)), 'setText')('1')


        # generate tcl cmmands file
        f = open("COMMANDS VERSION2 new.TXT","r")
        fl = open("COMMANDS.tcl", "w")

        SS = []
        for i in range(93):
            SS.append('label_'+str(i+1)+' ')

        for line in f:
                for j in range(93):
                    if SS[j] in line:
                        line = line.replace(SS[j], getattr(getattr(getattr(self, 'ui'), 'lineEdit_' + str(j + 1)), 'text')()+' ')
                line = line.replace('probe_path', probe_path)
                line = line.replace('FPGA_NAME0', self.ui.FPGA_1.text())
                fl.write(line)

    def Select_1(self):  # choose FPGA name
        f = open("COMMANDS VERSION2 new.TXT", "r")
        fl = open("COMMANDS.tcl", "w")
        S = "FPGA_NAME0"
        SS = []
        for i in range(93):
            SS.append('label_' + str(i + 1) + ' ')
        for line in f:
            if S in line:
                line = line.replace(S, str(self.ui.FPGA_1.text()))
            for j in range(93):
                if SS[j] in line:
                    line = line.replace(SS[j],
                                        getattr(getattr(getattr(self, 'ui'), 'lineEdit_' + str(j + 1)), 'text')() + ' ')
            line = line.replace('probe_path', probe_path)
            fl.write(line)

    def Select_2(self):  # choose FPGA name
        f = open("COMMANDS VERSION2 new.TXT", "r")
        fl = open("COMMANDS.tcl", "w")
        S = "FPGA_NAME0"
        SS = []
        for i in range(93):
            SS.append('label_' + str(i + 1) + ' ')
        for line in f:
            if S in line:
                line = line.replace(S, str(self.ui.FPGA_2.text()))
            for j in range(93):
                if SS[j] in line:
                    line = line.replace(SS[j],
                                        getattr(getattr(getattr(self, 'ui'), 'lineEdit_' + str(j + 1)), 'text')() + ' ')
            line = line.replace('probe_path', probe_path)
            fl.write(line)


    def Select_3(self):  # choose FPGA name
        f = open("COMMANDS VERSION2 new.TXT", "r")
        fl = open("COMMANDS.tcl", "w")
        S = "FPGA_NAME0"
        SS = []
        for i in range(93):
            SS.append('label_' + str(i + 1) + ' ')
        for line in f:
            if S in line:
                line = line.replace(S, str(self.ui.FPGA_3.text()))
            for j in range(93):
                if SS[j] in line:
                    line = line.replace(SS[j],
                                        getattr(getattr(getattr(self, 'ui'), 'lineEdit_' + str(j + 1)), 'text')() + ' ')
            line = line.replace('probe_path', probe_path)
            fl.write(line)

    def FILEOPEN(self): # open presettings
        fname, _ = QtWidgets.QFileDialog.getOpenFileName(self, 'Open file', 'c:\\', "Txt files (*.txt)") # copy file direction and name
        f = open(fname, "r")
        for line in f.readlines():
            linestr1 = line.strip()
            linestrlist1 = linestr1.split(" ")
        list0 = []
        for i in range(44):
            list0.append(i+1)
        for i in range(47,73):
            list0.append(i)
        for i in range(81,94):
            list0.append(i)

        for i in range(83):  # 26 is the number of values
            getattr(getattr(getattr(self,'ui'), 'lineEdit_' + str(list0[i])), 'setText')(linestrlist1[i])

    def FILESAVE(self): # save settings into customized file
        fname, _ = QtWidgets.QFileDialog.getOpenFileName(self, 'save file', 'c:\\', "Txt files (*.txt)")
        print(fname)
        fl = open(fname, "w")
        list1 = []
        for i in range(44):
            list1.append(i+1)
        for i in range(47,73):
            list1.append(i)
        for i in range(81,94):
            list1.append(i)

        for i in list1:
            #print(i)
            line = getattr(getattr(getattr(self, 'ui'), 'lineEdit_' + str(i)), 'text')()
            fl.write(line + ' ')


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = Main()
    window.show()
    sys.exit(app.exec_())