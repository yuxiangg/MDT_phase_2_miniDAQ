open_hw
connect_hw_server
open_hw_target
current_hw_device [lindex [get_hw_devices] 0]
refresh_hw_device -update_hw_probes false [lindex [get_hw_devices] 0]


set_property PROBES.FILE {C:/Users/gyuxiang/Desktop/gyx_work/TDC/02_CSM/newmezz_firmware/newmezz_firmware.runs/impl_1/top.ltx} [get_hw_devices xc7a200t_0]


current_hw_device [lindex [get_hw_devices] 0]
refresh_hw_device [lindex [get_hw_devices] 0]
commit_hw_vio [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]

set_property PROBES.FILE {C:/Users/zkcheng/Downloads/newmezz_firmware/newmezz_firmware.runs/impl_1/top.ltx} [get_hw_devices xc7a200t_0]
set_property FULL_PROBES.FILE {C:/Users/zkcheng/Downloads/newmezz_firmware/newmezz_firmware.runs/impl_1/top.ltx} [get_hw_devices xc7a200t_0]

set_property OUTPUT_VALUE 0 [get_hw_probes Mezz_config_inst/bypass_bcr_distribution -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/bypass_bcr_distribution} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]

set_property OUTPUT_VALUE FFFFFF [get_hw_probes Mezz_config_inst/channel_enable_f -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/channel_enable_f} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]

set_property OUTPUT_VALUE 0 [get_hw_probes Mezz_config_inst/enable_direct_event_reset -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/enable_direct_event_reset} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/enable_high_speed -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/enable_high_speed} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]

set_property OUTPUT_VALUE 0 [get_hw_probes Mezz_config_inst/enable_leading -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/enable_leading} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]

set_property OUTPUT_VALUE 0 [get_hw_probes Mezz_config_inst/enable_master_reset_code -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/enable_master_reset_code} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/enable_pair -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/enable_pair} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]

set_property OUTPUT_VALUE 0 [get_hw_probes Mezz_config_inst/enable_trigger_timeout -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/enable_trigger_timeout} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]

set_property OUTPUT_VALUE 0 [get_hw_probes Mezz_config_inst/full_width_res -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/full_width_res} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]

set_property OUTPUT_VALUE 0 [get_hw_probes Mezz_config_inst/enable_TDC_ID -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/enable_TDC_ID} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]

set_property OUTPUT_VALUE 0 [get_hw_probes Mezz_config_inst/enable_direct_bunch_reset -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/enable_direct_bunch_reset} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/auto_roll_over -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/auto_roll_over} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]

set_property OUTPUT_VALUE 7AAAA [get_hw_probes Mezz_config_inst/TDC_ID -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/TDC_ID} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]

set_property OUTPUT_VALUE FFFFFF [get_hw_probes Mezz_config_inst/channel_enable_r -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/channel_enable_r} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]

set_property OUTPUT_VALUE 0 [get_hw_probes Mezz_config_inst/channel_data_debug -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/channel_data_debug} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/enable_8b10b -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/enable_8b10b} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]

set_property OUTPUT_VALUE 0 [get_hw_probes Mezz_config_inst/enable_direct_trigger -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/enable_direct_trigger} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]

set_property OUTPUT_VALUE 0 [get_hw_probes Mezz_config_inst/enable_error_packet -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/enable_error_packet} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]

set_property OUTPUT_VALUE 0 [get_hw_probes Mezz_config_inst/enable_insert -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/enable_insert} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]

set_property OUTPUT_VALUE 0 [get_hw_probes Mezz_config_inst/enable_legacy -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/enable_legacy} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]

set_property OUTPUT_VALUE 0 [get_hw_probes Mezz_config_inst/enable_new_ttc -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/enable_new_ttc} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]

set_property OUTPUT_VALUE 0 [get_hw_probes Mezz_config_inst/enable_trigger -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/enable_trigger} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]

set_property OUTPUT_VALUE 0 [get_hw_probes Mezz_config_inst/enbale_fake_hit -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/enbale_fake_hit} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]

set_property OUTPUT_VALUE FFFFFF [get_hw_probes Mezz_config_inst/rising_is_leading -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/rising_is_leading} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]

set_property OUTPUT_VALUE 0 [get_hw_probes Mezz_config_inst/width_select -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/width_select} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]

set_property OUTPUT_VALUE 0 [get_hw_probes Mezz_config_inst/enable_error_notify -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/enable_error_notify} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup0_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/align -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_Jtag_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/align} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_Jtag_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/start_action_user -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_Jtag_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/start_action_user} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_Jtag_inst"}]]

set_property OUTPUT_VALUE 111 [get_hw_probes Mezz_config_inst/bit_length_vio -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_Jtag_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/bit_length_vio} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_Jtag_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/start_action_hptdc_asd -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_Jtag_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/start_action_hptdc_asd} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_Jtag_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/start_action_hptdc_asdread -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_Jtag_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/start_action_hptdc_asdread} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_Jtag_inst"}]]

set_property OUTPUT_VALUE 1111 [get_hw_probes Mezz_config_inst/JTAG_instr_vio -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_Jtag_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/JTAG_instr_vio} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_Jtag_inst"}]]

set_property OUTPUT_VALUE 1111 [get_hw_probes Mezz_config_inst/not_used -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_Jtag_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/not_used} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_Jtag_inst"}]]

set_property OUTPUT_VALUE 111 [get_hw_probes Mezz_config_inst/config_period -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_Jtag_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/config_period} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_Jtag_inst"}]]

set_property OUTPUT_VALUE 1111111111111111111111111111111111111111111111111111111111111111 [get_hw_probes Mezz_config_inst/JTAG_bits_vio -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_Jtag_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/JTAG_bits_vio} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_Jtag_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/start_action_asd -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_asd_setup_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/start_action_asd} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_asd_setup_inst"}]]

set_property OUTPUT_VALUE 7 [get_hw_probes Mezz_config_inst/wilk_thr -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_asd_setup_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/wilk_thr} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_asd_setup_inst"}]]

set_property OUTPUT_VALUE 1111 [get_hw_probes Mezz_config_inst/channel_mode -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_asd_setup_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/channel_mode} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_asd_setup_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/chip_mode -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_asd_setup_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/chip_mode} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_asd_setup_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/hyst_dac_reversed -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_asd_setup_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/hyst_dac_reversed} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_asd_setup_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/int_gate -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_asd_setup_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/int_gate} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_asd_setup_inst"}]]

set_property OUTPUT_VALUE 7 [get_hw_probes Mezz_config_inst/deadtime -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_asd_setup_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/deadtime} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_asd_setup_inst"}]]

set_property OUTPUT_VALUE 11 [get_hw_probes Mezz_config_inst/main_thr -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_asd_setup_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/main_thr} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_asd_setup_inst"}]]

set_property OUTPUT_VALUE 7 [get_hw_probes Mezz_config_inst/rundown_curr -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_asd_setup_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/rundown_curr} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_asd_setup_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/chnl_fifo_overflow_clear -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_control0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/chnl_fifo_overflow_clear} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_control0_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/debug_port_select -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_control0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/debug_port_select} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_control0_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/event_teset_jtag_in -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_control0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/event_teset_jtag_in} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_control0_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/reset_jtag_in -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_control0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/reset_jtag_in} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_control0_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/rst_ePLL -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_control0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/rst_ePLL} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_control0_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/start_action_control0 -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_control0_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/start_action_control0} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_control0_inst"}]]

set_property OUTPUT_VALUE 3 [get_hw_probes Mezz_config_inst/ePllCap -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_control1_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/ePllCap} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_control1_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/ePllIcp -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_control1_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/ePllIcp} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_control1_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/ePllRes -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_control1_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/ePllRes} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_control1_inst"}]]

set_property OUTPUT_VALUE 11 [get_hw_probes Mezz_config_inst/phase_clk160 -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_control1_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/phase_clk160} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_control1_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/phase_clk320_0 -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_control1_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/phase_clk320_0} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_control1_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/phase_clk320_1 -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_control1_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/phase_clk320_1} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_control1_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/phase_clk320_2 -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_control1_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/phase_clk320_2} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_control1_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/start_action_control1 -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_control1_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/start_action_control1} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_control1_inst"}]]

set_property OUTPUT_VALUE 111 [get_hw_probes Mezz_config_inst/bunch_offset -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup1_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/bunch_offset} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup1_inst"}]]

set_property OUTPUT_VALUE 111 [get_hw_probes Mezz_config_inst/coarse_count_offset -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup1_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/coarse_count_offset} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup1_inst"}]]

set_property OUTPUT_VALUE 311 [get_hw_probes Mezz_config_inst/combine_time_out_config -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup1_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/combine_time_out_config} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup1_inst"}]]

set_property OUTPUT_VALUE 111 [get_hw_probes Mezz_config_inst/event_offset -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup1_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/event_offset} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup1_inst"}]]

set_property OUTPUT_VALUE 111 [get_hw_probes Mezz_config_inst/fake_hit_time_interval -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup1_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/fake_hit_time_interval} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup1_inst"}]]

set_property OUTPUT_VALUE 111 [get_hw_probes Mezz_config_inst/match_window -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup1_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/match_window} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup1_inst"}]]

set_property OUTPUT_VALUE 111 [get_hw_probes Mezz_config_inst/roll_over -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup1_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/roll_over} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup1_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/start_action_setup1 -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup1_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/start_action_setup1} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup1_inst"}]]

set_property OUTPUT_VALUE 111 [get_hw_probes Mezz_config_inst/syn_packet_number -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup1_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/syn_packet_number} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup1_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/fine_sel -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup2_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/fine_sel} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup2_inst"}]]

set_property OUTPUT_VALUE 11111111 [get_hw_probes Mezz_config_inst/lut -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup2_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/lut} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup2_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/start_action_setup2 -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup2_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/start_action_setup2} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_setup2_inst"}]]

set_property OUTPUT_VALUE 7 [get_hw_probes Mezz_config_inst/oldasd_cap_select -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_oldasd_setup_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/oldasd_cap_select} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_oldasd_setup_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/oldasd_chipmodeoldasd_chipmode -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_oldasd_setup_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/oldasd_chipmode} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_oldasd_setup_inst"}]]

set_property OUTPUT_VALUE 11 [get_hw_probes Mezz_config_inst/oldasd_chnl_mask -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_oldasd_setup_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/oldasd_chnl_mask} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_oldasd_setup_inst"}]]

set_property OUTPUT_VALUE 1111 [get_hw_probes Mezz_config_inst/oldasd_chnl_mode -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_oldasd_setup_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/oldasd_chnl_mode} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_oldasd_setup_inst"}]]

set_property OUTPUT_VALUE 7 [get_hw_probes Mezz_config_inst/oldasd_deadtime -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_oldasd_setup_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/oldasd_deadtime} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_oldasd_setup_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/oldasd_disc1_ -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_oldasd_setup_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/oldasd_disc1_} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_oldasd_setup_inst"}]]

set_property OUTPUT_VALUE 7 [get_hw_probes Mezz_config_inst/oldasd_disc2_ -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_oldasd_setup_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/oldasd_disc2_} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_oldasd_setup_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/oldasd_int_gate -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_oldasd_setup_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/oldasd_int_gate} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_oldasd_setup_inst"}]]

set_property OUTPUT_VALUE 11 [get_hw_probes Mezz_config_inst/oldasd_main_thr -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_oldasd_setup_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/oldasd_main_thr} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_oldasd_setup_inst"}]]

set_property OUTPUT_VALUE 7 [get_hw_probes Mezz_config_inst/oldasd_rundown_cur -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_oldasd_setup_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/oldasd_rundown_cur} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_oldasd_setup_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/start_action_hptdc_oldasd -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_oldasd_setup_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/start_action_hptdc_oldasd} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_oldasd_setup_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/start_action_hptdc_oldasdread -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_oldasd_setup_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/start_action_hptdc_oldasdread} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_oldasd_setup_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes Mezz_config_inst/start_action_oldasd -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_oldasd_setup_inst"}]]
commit_hw_vio [get_hw_probes {Mezz_config_inst/start_action_oldasd} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a200t_0] -filter {CELL_NAME=~"Mezz_config_inst/vio_oldasd_setup_inst"}]]



exit