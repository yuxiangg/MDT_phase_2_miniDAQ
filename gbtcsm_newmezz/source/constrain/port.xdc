
# Changed on 10/12/18 by Xueye for GBTx-CSM prototype v1


# from SMA, sync to VC707-L0MDT
set_property PACKAGE_PIN AK7 [get_ports sys_clk_p];
set_property IOSTANDARD LVDS_25 [get_ports sys_clk_p];
set_property PACKAGE_PIN AL7 [get_ports sys_clk_n];
set_property IOSTANDARD LVDS_25 [get_ports sys_clk_n];


# set_property PACKAGE_PIN AL30 [get_ports calibre_source_clk_p];
# set_property IOSTANDARD LVDS_25 [get_ports calibre_source_clk_p];
# set_property PACKAGE_PIN AM30 [get_ports calibre_source_clk_n];
# set_property IOSTANDARD LVDS_25 [get_ports calibre_source_clk_n];

# Note 1: r443 connection on board. Rst fanout done one csm mother board (MB)
# Note 2: Data lines comes from GBTx, not FPGA. So constraints on data lines can be removed 
set_property PACKAGE_PIN R26 [get_ports rst_TDC];  
set_property IOSTANDARD LVCMOS33 [get_ports rst_TDC];

# set_property PACKAGE_PIN AL10 [get_ports lock];  
# set_property IOSTANDARD LVCMOS25 [get_ports lock];

# set_property PACKAGE_PIN T7 [get_ports lock_calib];  
# set_property IOSTANDARD LVCMOS33 [get_ports lock_calib];

# ######################################################################################
# # MEZZANINE 0 --- adapt to H12 on mother board
# # 1 data line running at 80Mbps from AMT --> to GBTx-CSM GBTx chip 2: Elink din [0]
# # Legency Mezz. clock got from GBTx chip 2, progclk 0/7
# ######################################################################################
# set_property PACKAGE_PIN R27 [get_ports tdo[0]];
# set_property IOSTANDARD LVCMOS33 [get_ports tdo[0]];
# set_property PACKAGE_PIN P29 [get_ports tdi[0]];
# set_property IOSTANDARD LVCMOS33 [get_ports tdi[0]];
# set_property PACKAGE_PIN R28 [get_ports tck[0]];
# set_property IOSTANDARD LVCMOS33 [get_ports tck[0]];
# set_property PACKAGE_PIN P28 [get_ports tms[0]];
# set_property IOSTANDARD LVCMOS33 [get_ports tms[0]];
# #set_property PACKAGE_PIN AB21 [get_ports rst_fmc[0]];
# #set_property IOSTANDARD LVCMOS25 [get_ports rst_fmc[0]];
# set_property PACKAGE_PIN H27 [get_ports calibre_p[0]];
# set_property IOSTANDARD LVDS_25 [get_ports calibre_p[0]];
# set_property PACKAGE_PIN G27 [get_ports calibre_n[0]];
# set_property IOSTANDARD LVDS_25 [get_ports calibre_n[0]];

# set_property PACKAGE_PIN H26 [get_ports enc_rst_p[0]];
# set_property IOSTANDARD LVDS_25 [get_ports enc_rst_p[0]];
# set_property PACKAGE_PIN G26 [get_ports enc_rst_n[0]];
# set_property IOSTANDARD LVDS_25 [get_ports enc_rst_n[0]];

# #set_property PACKAGE_PIN AE25 [get_ports serial_data_p[0]];
# #set_property IOSTANDARD LVDS_25 [get_ports serial_data_p[0]];
# #set_property PACKAGE_PIN AE26 [get_ports serial_data_n[0]];
# #set_property IOSTANDARD LVDS_25 [get_ports serial_data_n[0]];


# #set_property PACKAGE_PIN D18 [get_ports tdo[0]];
# #set_property IOSTANDARD LVCMOS25 [get_ports tdo[0]];
# #set_property PACKAGE_PIN C18 [get_ports tdi[0]];
# #set_property IOSTANDARD LVCMOS25 [get_ports tdi[0]];
# #set_property PACKAGE_PIN G17 [get_ports tck[0]];
# #set_property IOSTANDARD LVCMOS25 [get_ports tck[0]];
# #set_property PACKAGE_PIN F18 [get_ports tms[0]];
# #set_property IOSTANDARD LVCMOS25 [get_ports tms[0]];
# #set_property PACKAGE_PIN AB21 [get_ports rst_fmc[0]];
# #set_property IOSTANDARD LVCMOS25 [get_ports rst_fmc[0]];
# #set_property PACKAGE_PIN AD23 [get_ports calibre_p[0]];
# #set_property IOSTANDARD LVDS_25 [get_ports calibre_p[0]];
# #set_property PACKAGE_PIN AD24 [get_ports calibre_n[0]];
# #set_property IOSTANDARD LVDS_25 [get_ports calibre_n[0]];
# #set_property PACKAGE_PIN AE25 [get_ports serial_data_p[0]];
# #set_property IOSTANDARD LVDS_25 [get_ports serial_data_p[0]];
# #set_property PACKAGE_PIN AE26 [get_ports serial_data_n[0]];
# #set_property IOSTANDARD LVDS_25 [get_ports serial_data_n[0]];
# ##set_property PACKAGE_PIN H14 [get_ports bcr_p[0]];
# ##set_property IOSTANDARD LVDS_25 [get_ports bcr_p[0]];
# ##set_property PACKAGE_PIN H15 [get_ports bcr_n[0]];
# ##set_property IOSTANDARD LVDS_25 [get_ports bcr_n[0]];
# ##set_property PACKAGE_PIN AC22 [get_ports serial_strobe_p[0]];
# ##set_property IOSTANDARD LVDS_25 [get_ports serial_strobe_p[0]];
# ##set_property PACKAGE_PIN AC23 [get_ports serial_strobe_n[0]];
# ##set_property IOSTANDARD LVDS_25 [get_ports serial_strobe_n[0]];


# ######################################################################################
# # MEZZANINE 1 --- adapt to H13 on mother board
# # 1 data line running at 80Mbps from AMT --> to GBTx-CSM GBTx chip 2: Elink din [8]
# # Legency Mezz. clock got from GBTx chip 2, progclk 1/7
# ######################################################################################
# set_property PACKAGE_PIN P1 [get_ports tdo[1]];
# set_property IOSTANDARD LVCMOS33 [get_ports tdo[1]];
# set_property PACKAGE_PIN N1 [get_ports tdi[1]];
# set_property IOSTANDARD LVCMOS33 [get_ports tdi[1]];
# set_property PACKAGE_PIN N2 [get_ports tck[1]];
# set_property IOSTANDARD LVCMOS33 [get_ports tck[1]];
# set_property PACKAGE_PIN M1 [get_ports tms[1]];
# set_property IOSTANDARD LVCMOS33 [get_ports tms[1]];
# #set_property PACKAGE_PIN AC21 [get_ports rst_fmc[1]];
# #set_property IOSTANDARD LVCMOS25 [get_ports rst_fmc[1]];

# set_property PACKAGE_PIN H1 [get_ports calibre_p[1]];
# set_property IOSTANDARD LVDS_25 [get_ports calibre_p[1]];
# set_property PACKAGE_PIN G1 [get_ports calibre_n[1]];
# set_property IOSTANDARD LVDS_25 [get_ports calibre_n[1]];

# set_property PACKAGE_PIN K1 [get_ports enc_rst_p[1]];
# set_property IOSTANDARD LVDS_25 [get_ports enc_rst_p[1]];
# set_property PACKAGE_PIN J1 [get_ports enc_rst_n[1]];
# set_property IOSTANDARD LVDS_25 [get_ports enc_rst_n[1]];

# #set_property PACKAGE_PIN AE22 [get_ports serial_data_p[1]];
# #set_property IOSTANDARD LVDS_25 [get_ports serial_data_p[1]];
# #set_property PACKAGE_PIN AF22 [get_ports serial_data_n[1]];
# #set_property IOSTANDARD LVDS_25 [get_ports serial_data_n[1]];


# #set_property PACKAGE_PIN F17 [get_ports tdo[1]];
# #set_property IOSTANDARD LVCMOS25 [get_ports tdo[1]];
# #set_property PACKAGE_PIN F19 [get_ports tdi[1]];
# #set_property IOSTANDARD LVCMOS25 [get_ports tdi[1]];
# #set_property PACKAGE_PIN C17 [get_ports tck[1]];
# #set_property IOSTANDARD LVCMOS25 [get_ports tck[1]];
# #set_property PACKAGE_PIN H16 [get_ports tms[1]];
# #set_property IOSTANDARD LVCMOS25 [get_ports tms[1]];
# #set_property PACKAGE_PIN AC21 [get_ports rst_fmc[1]];
# #set_property IOSTANDARD LVCMOS25 [get_ports rst_fmc[1]];
# #set_property PACKAGE_PIN AE18 [get_ports calibre_p[1]];
# #set_property IOSTANDARD LVDS_25 [get_ports calibre_p[1]];
# #set_property PACKAGE_PIN AF18 [get_ports calibre_n[1]];
# #set_property IOSTANDARD LVDS_25 [get_ports calibre_n[1]];
# #set_property PACKAGE_PIN AE22 [get_ports serial_data_p[1]];
# #set_property IOSTANDARD LVDS_25 [get_ports serial_data_p[1]];
# #set_property PACKAGE_PIN AF22 [get_ports serial_data_n[1]];
# #set_property IOSTANDARD LVDS_25 [get_ports serial_data_n[1]];
# ##set_property PACKAGE_PIN AD20 [get_ports bcr_p[1]];
# ##set_property IOSTANDARD LVDS_25 [get_ports bcr_p[1]];
# ##set_property PACKAGE_PIN AE20 [get_ports bcr_n[1]];
# ##set_property IOSTANDARD LVDS_25 [get_ports bcr_n[1]];
# ##set_property PACKAGE_PIN AE23 [get_ports serial_strobe_p[1]];
# ##set_property IOSTANDARD LVDS_25 [get_ports serial_strobe_p[1]];
# ##set_property PACKAGE_PIN AF23 [get_ports serial_strobe_n[1]];
# ##set_property IOSTANDARD LVDS_25 [get_ports serial_strobe_n[1]];


# ######################################################################################
# # MEZZANINE 2 --- adapt to H14 on mother board
# # 1 data line running at 80Mbps from AMT --> to GBTx-CSM GBTx chip 2: Elink din [16]
# # Legency Mezz. clock got from GBTx chip 2, progclk 2/7
# ######################################################################################
# set_property PACKAGE_PIN P24 [get_ports tdo[2]];
# set_property IOSTANDARD LVCMOS33 [get_ports tdo[2]];
# set_property PACKAGE_PIN P25 [get_ports tdi[2]];
# set_property IOSTANDARD LVCMOS33 [get_ports tdi[2]];
# set_property PACKAGE_PIN P26 [get_ports tck[2]];
# set_property IOSTANDARD LVCMOS33 [get_ports tck[2]];
# set_property PACKAGE_PIN N26 [get_ports tms[2]];
# set_property IOSTANDARD LVCMOS33 [get_ports tms[2]];
# #set_property PACKAGE_PIN AD25 [get_ports rst_fmc[2]];
# #set_property IOSTANDARD LVCMOS25 [get_ports rst_fmc[2]];

# set_property PACKAGE_PIN G24 [get_ports calibre_p[2]];
# set_property IOSTANDARD LVDS_25 [get_ports calibre_p[2]];
# set_property PACKAGE_PIN G25 [get_ports calibre_n[2]];
# set_property IOSTANDARD LVDS_25 [get_ports calibre_n[2]];

# set_property PACKAGE_PIN K23 [get_ports enc_rst_p[2]];
# set_property IOSTANDARD LVDS_25 [get_ports enc_rst_p[2]];
# set_property PACKAGE_PIN J23 [get_ports enc_rst_n[2]];
# set_property IOSTANDARD LVDS_25 [get_ports enc_rst_n[2]];
# #set_property PACKAGE_PIN W14 [get_ports serial_data_p[2]];
# #set_property IOSTANDARD LVDS_25 [get_ports serial_data_p[2]];
# #set_property PACKAGE_PIN W15 [get_ports serial_data_n[2]];
# #set_property IOSTANDARD LVDS_25 [get_ports serial_data_n[2]];


# #set_property PACKAGE_PIN B17 [get_ports tdo[2]];
# #set_property IOSTANDARD LVCMOS25 [get_ports tdo[2]];
# #set_property PACKAGE_PIN G16 [get_ports tdi[2]];
# #set_property IOSTANDARD LVCMOS25 [get_ports tdi[2]];
# #set_property PACKAGE_PIN E20 [get_ports tck[2]];
# #set_property IOSTANDARD LVCMOS25 [get_ports tck[2]];
# #set_property PACKAGE_PIN B19 [get_ports tms[2]];
# #set_property IOSTANDARD LVCMOS25 [get_ports tms[2]];
# #set_property PACKAGE_PIN AD25 [get_ports rst_fmc[2]];
# #set_property IOSTANDARD LVCMOS25 [get_ports rst_fmc[2]];
# #set_property PACKAGE_PIN Y15 [get_ports calibre_p[2]];
# #set_property IOSTANDARD LVDS_25 [get_ports calibre_p[2]];
# #set_property PACKAGE_PIN AA15 [get_ports calibre_n[2]];
# #set_property IOSTANDARD LVDS_25 [get_ports calibre_n[2]];
# #set_property PACKAGE_PIN W14 [get_ports serial_data_p[2]];
# #set_property IOSTANDARD LVDS_25 [get_ports serial_data_p[2]];
# #set_property PACKAGE_PIN W15 [get_ports serial_data_n[2]];
# #set_property IOSTANDARD LVDS_25 [get_ports serial_data_n[2]];
# ##set_property PACKAGE_PIN AA17 [get_ports bcr_p[2]];
# ##set_property IOSTANDARD LVDS_25 [get_ports bcr_p[2]];
# ##set_property PACKAGE_PIN AB17 [get_ports bcr_n[2]];
# ##set_property IOSTANDARD LVDS_25 [get_ports bcr_n[2]];
# ##set_property PACKAGE_PIN AB16 [get_ports serial_strobe_p[2]];
# ##set_property IOSTANDARD LVDS_25 [get_ports serial_strobe_p[2]];
# ##set_property PACKAGE_PIN AC16 [get_ports serial_strobe_n[2]];
# ##set_property IOSTANDARD LVDS_25 [get_ports serial_strobe_n[2]];

# ######################################################################################
# # MEZZANINE 3 --- adapt to H15 on mother board
# # 1 data line running at 80Mbps from AMT --> to GBTx-CSM GBTx chip 2: Elink din [24]
# # Legency Mezz. clock got from GBTx chip 2, progclk 3/7
# ######################################################################################
# set_property PACKAGE_PIN N34 [get_ports tdo[3]];
# set_property IOSTANDARD LVCMOS33 [get_ports tdo[3]];
# set_property PACKAGE_PIN N33 [get_ports tdi[3]];
# set_property IOSTANDARD LVCMOS33 [get_ports tdi[3]];
# set_property PACKAGE_PIN N32 [get_ports tck[3]];
# set_property IOSTANDARD LVCMOS33 [get_ports tck[3]];
# set_property PACKAGE_PIN N31 [get_ports tms[3]];
# set_property IOSTANDARD LVCMOS33 [get_ports tms[3]];

# #set_property PACKAGE_PIN G21 [get_ports rst_fmc[3]];
# #set_property IOSTANDARD LVCMOS25 [get_ports rst_fmc[3]];
# set_property PACKAGE_PIN G29 [get_ports calibre_p[3]];
# set_property IOSTANDARD LVDS_25 [get_ports calibre_p[3]];
# set_property PACKAGE_PIN G30 [get_ports calibre_n[3]];
# set_property IOSTANDARD LVDS_25 [get_ports calibre_n[3]];

# set_property PACKAGE_PIN J28 [get_ports enc_rst_p[3]];
# set_property IOSTANDARD LVDS_25 [get_ports enc_rst_p[3]];
# set_property PACKAGE_PIN H28 [get_ports enc_rst_n[3]];
# set_property IOSTANDARD LVDS_25 [get_ports enc_rst_n[3]];

# #set_property PACKAGE_PIN E25 [get_ports serial_data_p[3]];
# #set_property IOSTANDARD LVDS_25 [get_ports serial_data_p[3]];
# #set_property PACKAGE_PIN D25 [get_ports serial_data_n[3]];
# #set_property IOSTANDARD LVDS_25 [get_ports serial_data_n[3]];
# #set_property PACKAGE_PIN G25 [get_ports bcr_p[3]];
# #set_property IOSTANDARD LVDS_25 [get_ports bcr_p[3]];
# #set_property PACKAGE_PIN F25 [get_ports bcr_n[3]];
# #set_property IOSTANDARD LVDS_25 [get_ports bcr_n[3]];
# #set_property PACKAGE_PIN H26 [get_ports serial_strobe_p[3]];
# #set_property IOSTANDARD LVDS_25 [get_ports serial_strobe_p[3]];
# #set_property PACKAGE_PIN G26 [get_ports serial_strobe_n[3]];
# #set_property IOSTANDARD LVDS_25 [get_ports serial_strobe_n[3]];


######################################################################################
# MEZZANINE 4 --- adapt to H16 on mother board
# 1 data line running at 80Mbps from AMT --> to GBTx-CSM GBTx chip 2: Elink din [32]
# Legency Mezz. clock got from GBTx chip 2, progclk 4/7
######################################################################################
set_property PACKAGE_PIN M34 [get_ports tdo];
set_property IOSTANDARD LVCMOS33 [get_ports tdo];
set_property PACKAGE_PIN M32 [get_ports tdi];
set_property IOSTANDARD LVCMOS33 [get_ports tdi];
set_property PACKAGE_PIN M31 [get_ports tck];
set_property IOSTANDARD LVCMOS33 [get_ports tck];
set_property PACKAGE_PIN M30 [get_ports tms];
set_property IOSTANDARD LVCMOS33 [get_ports tms];
#set_property PACKAGE_PIN F23 [get_ports rst_fmc[4]];
#set_property IOSTANDARD LVCMOS25 [get_ports rst_fmc[4]];

# set_property PACKAGE_PIN J33 [get_ports calibre_p[4]];
# set_property IOSTANDARD LVDS_25 [get_ports calibre_p[4]];
# set_property PACKAGE_PIN H34 [get_ports calibre_n[4]];
# set_property IOSTANDARD LVDS_25 [get_ports calibre_n[4]];

# set_property PACKAGE_PIN H33 [get_ports enc_rst_p[4]];
# set_property IOSTANDARD LVDS_25 [get_ports enc_rst_p[4]];
# set_property PACKAGE_PIN G34 [get_ports enc_rst_n[4]];
# set_property IOSTANDARD LVDS_25 [get_ports enc_rst_n[4]];

#set_property PACKAGE_PIN J18 [get_ports serial_data_p[4]];
#set_property IOSTANDARD LVDS_25 [get_ports serial_data_p[4]];
#set_property PACKAGE_PIN H18 [get_ports serial_data_n[4]];
#set_property IOSTANDARD LVDS_25 [get_ports serial_data_n[4]];

#set_property PACKAGE_PIN AD17 [get_ports tdo[4]];
#set_property IOSTANDARD LVCMOS25 [get_ports tdo[4]];
#set_property PACKAGE_PIN Y17 [get_ports tdi[4]];
#set_property IOSTANDARD LVCMOS25 [get_ports tdi[4]];
#set_property PACKAGE_PIN K21 [get_ports tck[4]];
#set_property IOSTANDARD LVCMOS25 [get_ports tck[4]];
#set_property PACKAGE_PIN J21 [get_ports tms[4]];
#set_property IOSTANDARD LVCMOS25 [get_ports tms[4]];
#set_property PACKAGE_PIN F23 [get_ports rst_fmc[4]];
#set_property IOSTANDARD LVCMOS25 [get_ports rst_fmc[4]];
#set_property PACKAGE_PIN G22 [get_ports calibre_p[4]];
#set_property IOSTANDARD LVDS_25 [get_ports calibre_p[4]];
#set_property PACKAGE_PIN F22 [get_ports calibre_n[4]];
#set_property IOSTANDARD LVDS_25 [get_ports calibre_n[4]];
#set_property PACKAGE_PIN J18 [get_ports serial_data_p[4]];
#set_property IOSTANDARD LVDS_25 [get_ports serial_data_p[4]];
#set_property PACKAGE_PIN H18 [get_ports serial_data_n[4]];
#set_property IOSTANDARD LVDS_25 [get_ports serial_data_n[4]];
##set_property PACKAGE_PIN G24 [get_ports bcr_p[4]];
##set_property IOSTANDARD LVDS_25 [get_ports bcr_p[4]];
##set_property PACKAGE_PIN F24 [get_ports bcr_n[4]];
##set_property IOSTANDARD LVDS_25 [get_ports bcr_n[4]];
##set_property PACKAGE_PIN K22 [get_ports serial_strobe_p[4]];
##set_property IOSTANDARD LVDS_25 [get_ports serial_strobe_p[4]];
##set_property PACKAGE_PIN K23 [get_ports serial_strobe_n[4]];
##set_property IOSTANDARD LVDS_25 [get_ports serial_strobe_n[4]];

# ######################################################################################
# # MEZZANINE 5 --- adapt to H17 on mother board
# # 1 data line running at 80Mbps from AMT --> to GBTx-CSM GBTx chip 2: Elink DIO [1]
# # Legency Mezz. clock got from GBTx chip 2, progclk 4/7
# ######################################################################################
# set_property PACKAGE_PIN U2 [get_ports tdo[5]];
# set_property IOSTANDARD LVCMOS33 [get_ports tdo[5]];
# set_property PACKAGE_PIN U1 [get_ports tdi[5]];
# set_property IOSTANDARD LVCMOS33 [get_ports tdi[5]];
# set_property PACKAGE_PIN R1 [get_ports tck[5]];
# set_property IOSTANDARD LVCMOS33 [get_ports tck[5]];
# set_property PACKAGE_PIN T2 [get_ports tms[5]];
# set_property IOSTANDARD LVCMOS33 [get_ports tms[5]];

# #set_property PACKAGE_PIN E23 [get_ports rst_fmc[5]];
# #set_property IOSTANDARD LVCMOS25 [get_ports rst_fmc[5]];

# set_property PACKAGE_PIN H9 [get_ports calibre_p[5]];
# set_property IOSTANDARD LVDS_25 [get_ports calibre_p[5]];
# set_property PACKAGE_PIN H8 [get_ports calibre_n[5]];
# set_property IOSTANDARD LVDS_25 [get_ports calibre_n[5]];

# set_property PACKAGE_PIN H2 [get_ports enc_rst_p[5]];
# set_property IOSTANDARD LVDS_25 [get_ports enc_rst_p[5]];
# set_property PACKAGE_PIN G2 [get_ports enc_rst_n[5]];
# set_property IOSTANDARD LVDS_25 [get_ports enc_rst_n[5]];

# #set_property PACKAGE_PIN M14 [get_ports serial_data_p[5]];
# #set_property IOSTANDARD LVDS_25 [get_ports serial_data_p[5]];
# #set_property PACKAGE_PIN L14 [get_ports serial_data_n[5]];
# #set_property IOSTANDARD LVDS_25 [get_ports serial_data_n[5]];


# #set_property PACKAGE_PIN Y18 [get_ports tdo[5]];
# #set_property IOSTANDARD LVCMOS25 [get_ports tdo[5]];
# #set_property PACKAGE_PIN AA18 [get_ports tdi[5]];
# #set_property IOSTANDARD LVCMOS25 [get_ports tdi[5]];
# #set_property PACKAGE_PIN AC17 [get_ports tck[5]];
# #set_property IOSTANDARD LVCMOS25 [get_ports tck[5]];
# #set_property PACKAGE_PIN Y16 [get_ports tms[5]];
# #set_property IOSTANDARD LVCMOS25 [get_ports tms[5]];
# #set_property PACKAGE_PIN E23 [get_ports rst_fmc[5]];
# #set_property IOSTANDARD LVCMOS25 [get_ports rst_fmc[5]];
# #set_property PACKAGE_PIN M16 [get_ports calibre_p[5]];
# #set_property IOSTANDARD LVDS_25 [get_ports calibre_p[5]];
# #set_property PACKAGE_PIN M17 [get_ports calibre_n[5]];
# #set_property IOSTANDARD LVDS_25 [get_ports calibre_n[5]];
# #set_property PACKAGE_PIN M14 [get_ports serial_data_p[5]];
# #set_property IOSTANDARD LVDS_25 [get_ports serial_data_p[5]];
# #set_property PACKAGE_PIN L14 [get_ports serial_data_n[5]];
# #set_property IOSTANDARD LVDS_25 [get_ports serial_data_n[5]];
# ##set_property PACKAGE_PIN L17 [get_ports bcr_p[5]];
# ##set_property IOSTANDARD LVDS_25 [get_ports bcr_p[5]];
# ##set_property PACKAGE_PIN L18 [get_ports bcr_n[5]];
# ##set_property IOSTANDARD LVDS_25 [get_ports bcr_n[5]];
# ##set_property PACKAGE_PIN J19 [get_ports serial_strobe_p[5]];
# ##set_property IOSTANDARD LVDS_25 [get_ports serial_strobe_p[5]];
# ##set_property PACKAGE_PIN H19 [get_ports serial_strobe_n[5]];
# ##set_property IOSTANDARD LVDS_25 [get_ports serial_strobe_n[5]];
