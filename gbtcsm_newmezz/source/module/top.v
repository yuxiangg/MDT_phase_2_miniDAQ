`timescale 1ns / 1ps


module top (
  input   wire                sys_clk_p,        // 200MHz System Clock P. --SMA
  input   wire                sys_clk_n,        // 200MHz System Clock N.
  
  input   wire      calibre_source_clk_p,
  input   wire      calibre_source_clk_n,  // 200MHz clk -- osc
  
  input  wire tdo,
  output wire tdi,
  output wire tck,
  output wire tms,
  output wire rst_TDC
  
);

assign rst_TDC = 1'b0;


  
  
  // JTAG Master.
  wire [255:0] JTAG_bits_mezz;
  wire [511:0] JTAG_data_out;
  wire start_action_mezz;
  wire [11:0] config_period;
  wire JTAG_busy;
  wire [8:0] bit_length_mezz;
  wire [12:0] JTAG_instr_mezz;
  wire clk_40M;
  wire pll_lock;
  
    JTAG_master inst_JTAG_master
      (
          .clk           (clk_40M),
          .TCK           (tck),
          .TMS           (tms),
          .TDI           (tdi),
          .TDO           (tdo),
          .start_action  (start_action_mezz),
          .config_period (config_period),
          .JTAG_bits     ({256'b0,JTAG_bits_mezz}),
          .bit_length    (bit_length_mezz),
          .JTAG_inst     (JTAG_instr_mezz),
          .JTAG_data_out (JTAG_data_out),
          .JTAG_busy     (JTAG_busy)
      );


    

  Mezz_config Mezz_config_inst
    (
      .clk               (clk_40M),
      .JTAG_busy         (JTAG_busy),
      .JTAG_data_out     (JTAG_data_out[511:256]),
      .JTAG_bits_mezz    (JTAG_bits_mezz),
      .bit_length_mezz   (bit_length_mezz),
      .start_action_mezz (start_action_mezz),
      .JTAG_instr_mezz   (JTAG_instr_mezz),
      .config_period     (config_period)
    );



    // ila_JTAG ila_jtag_inst (
    //     .clk(clk_40M), // input wire clk    
    //     .probe0(tck), // input wire [0:0]  probe0  
    //     .probe1(tms), // input wire [0:0]  probe1 
    //     .probe2(tdi), // input wire [0:0]  probe2 
    //     .probe3(tdo), // input wire [0:0]  probe3 
    //     .probe4(start_action), // input wire [0:0]  probe4 
    //     .probe5(config_period), // input wire [11:0]  probe5 
    //     .probe6(JTAG_bits), // input wire [511:0]  probe6 
    //     .probe7(bit_length), // input wire [8:0]  probe7 
    //     .probe8(JTAG_instr), // input wire [12:0]  probe8 
    //     .probe9(JTAG_data_out), // input wire [511:0]  probe9 
    //     .probe10(JTAG_busy) // input wire [0:0]  probe10
    // );


  clk_wiz_0 sma_clk_inst
   (
    // Clock out ports
    .clk_out1(clk_40M),     // output clk_out1
    // Status and control signals
    .reset(1'b0), // input reset
    .locked(pll_lock),       // output locked
   // Clock in ports
    .clk_in1_p(sys_clk_p),    // input clk_in1_p
    .clk_in1_n(sys_clk_n));    // input clk_in1_n
  


endmodule
