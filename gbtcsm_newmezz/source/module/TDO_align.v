`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/01/2017 05:26:35 PM
// Design Name: 
// Module Name: TDO_align
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TDO_align(
	input clk,
	input align,
	input [255:0] JTAG_data_out,
	input [8:0] bit_length,
	output reg [255:0] bits_align
	);
reg [8:0] shift_counter;
reg [1:0] align_reg;
wire align_syn;
always @(posedge clk) begin
	align_reg <= {align_reg[0], align};
end
assign align_syn = ~align_reg[1] & align_reg[0];

always @(posedge clk) begin
	if(align_syn) begin
		shift_counter <= 256-bit_length;
		bits_align <= JTAG_data_out;
	end else if(|shift_counter) begin
		shift_counter <= shift_counter - 1'b1;
		bits_align <= {1'b0, bits_align[255:1]};
	end
end
endmodule