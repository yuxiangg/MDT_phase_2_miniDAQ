
#  _________   ________     ________       
# / U OF M  / | LSA    /   / Physics/
# /__ATLAS__/ |   ___   | |   ______/
#    |   |    |  |   /  | |  |
#    |   |    |  |___/  | |  /______     
#    |   |    |         | |         /
#    /___/    |________/   /________/

# File Name  : TDC_sim_build_vivado.tcl
# Author     : Yuxiang Guo
# Revision   : 
#              First created on 2019-02-17 
# Note       : 
     


set_param general.maxThreads 8

# Set the reference directory for source file relative paths (by default the value is script directory path)
set origin_dir [file dirname [info script]]
set source_dir "$origin_dir/../source"
set firmware_dir "$origin_dir/../../../newmezz_firmware"

create_project newmezz_firmware $firmware_dir -part xc7a200tffg1156-2  -force


#sim files include all source files
# set_property SOURCE_SET {} [get_filesets sim_1]
set_property SOURCE_SET sources_1 [get_filesets sim_1]

# set files [list "$source_dir/TDC_logic_TMR_include" "$source_dir/sim/TMR_module_sim/include"]


add_files -fileset [get_filesets sources_1] $source_dir/module
add_files -fileset constrs_1 -norecurse $source_dir/constrain

set_property "top" "top" [get_filesets sources_1]


create_ip -name clk_wiz -vendor xilinx.com -library ip -version 5.4 -module_name clk_wiz_0 
set_property -dict [list CONFIG.PRIMITIVE {MMCM} CONFIG.PRIM_SOURCE {Differential_clock_capable_pin} CONFIG.PRIM_IN_FREQ {200.000} CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {40} CONFIG.CLKIN1_JITTER_PS {50.0} CONFIG.CLKOUT1_DRIVES {BUFG} CONFIG.CLKOUT2_DRIVES {BUFG} CONFIG.CLKOUT3_DRIVES {BUFG} CONFIG.CLKOUT4_DRIVES {BUFG} CONFIG.CLKOUT5_DRIVES {BUFG} CONFIG.CLKOUT6_DRIVES {BUFG} CONFIG.CLKOUT7_DRIVES {BUFG} CONFIG.FEEDBACK_SOURCE {FDBK_AUTO} CONFIG.MMCM_DIVCLK_DIVIDE {1} CONFIG.MMCM_CLKFBOUT_MULT_F {5.000} CONFIG.MMCM_CLKIN1_PERIOD {5.000} CONFIG.MMCM_CLKIN2_PERIOD {10.0} CONFIG.MMCM_COMPENSATION {ZHOLD} CONFIG.MMCM_CLKOUT0_DIVIDE_F {25.000} CONFIG.CLKOUT1_JITTER {135.255} CONFIG.CLKOUT1_PHASE_ERROR {89.971}] [get_ips clk_wiz_0]

# vio ip JTAG
create_ip -name vio -vendor xilinx.com -library ip -version 3.0 -module_name vio_JTAG 
set_property -dict [list CONFIG.C_PROBE_OUT3_INIT_VAL {0x040} CONFIG.C_PROBE_OUT2_INIT_VAL {0x0E11} CONFIG.C_PROBE_OUT3_WIDTH {9} CONFIG.C_PROBE_OUT2_WIDTH {13} CONFIG.C_PROBE_OUT1_WIDTH {12} CONFIG.C_PROBE_OUT0_WIDTH {256} CONFIG.C_PROBE_IN4_WIDTH {256} CONFIG.C_PROBE_IN3_WIDTH {9} CONFIG.C_PROBE_IN2_WIDTH {256} CONFIG.C_PROBE_IN1_WIDTH {256} CONFIG.C_NUM_PROBE_OUT {6} CONFIG.C_NUM_PROBE_IN {5}] [get_ips vio_JTAG]

# vio ip setup0
create_ip -name vio -vendor xilinx.com -library ip -version 3.0 -module_name vio_setup0  
set_property -dict [list CONFIG.C_PROBE_OUT21_INIT_VAL {0x1} CONFIG.C_PROBE_OUT17_INIT_VAL {0x1} CONFIG.C_PROBE_OUT15_INIT_VAL {0x7aaaa} CONFIG.C_PROBE_OUT14_INIT_VAL {0xffffff} CONFIG.C_PROBE_OUT13_INIT_VAL {0xffffff} CONFIG.C_PROBE_OUT12_INIT_VAL {0x000000} CONFIG.C_PROBE_OUT10_INIT_VAL {0x1} CONFIG.C_PROBE_OUT5_INIT_VAL {0x1} CONFIG.C_PROBE_OUT20_WIDTH {3} CONFIG.C_PROBE_OUT15_WIDTH {19} CONFIG.C_PROBE_OUT14_WIDTH {24} CONFIG.C_PROBE_OUT13_WIDTH {24} CONFIG.C_PROBE_OUT12_WIDTH {24} CONFIG.C_NUM_PROBE_OUT {27} CONFIG.C_NUM_PROBE_IN {0} CONFIG.C_EN_PROBE_IN_ACTIVITY {0}] [get_ips vio_setup0]

# vio ip setup1
create_ip -name vio -vendor xilinx.com -library ip -version 3.0 -module_name vio_setup1 
set_property -dict [list CONFIG.C_PROBE_OUT7_INIT_VAL {0x1f} CONFIG.C_PROBE_OUT5_INIT_VAL {0xf9c} CONFIG.C_PROBE_OUT3_INIT_VAL {0xfff} CONFIG.C_PROBE_OUT2_INIT_VAL {0xfff} CONFIG.C_PROBE_OUT1_INIT_VAL {0x100} CONFIG.C_PROBE_OUT0_INIT_VAL {0x28} CONFIG.C_PROBE_OUT7_WIDTH {12} CONFIG.C_PROBE_OUT6_WIDTH {12} CONFIG.C_PROBE_OUT5_WIDTH {12} CONFIG.C_PROBE_OUT4_WIDTH {12} CONFIG.C_PROBE_OUT3_WIDTH {12} CONFIG.C_PROBE_OUT2_WIDTH {12} CONFIG.C_PROBE_OUT1_WIDTH {12} CONFIG.C_PROBE_OUT0_WIDTH {10} CONFIG.C_NUM_PROBE_OUT {9} CONFIG.C_NUM_PROBE_IN {0} CONFIG.C_EN_PROBE_IN_ACTIVITY {0}] [get_ips vio_setup1]

# vio ip setup2
create_ip -name vio -vendor xilinx.com -library ip -version 3.0 -module_name vio_setup2 
set_property -dict [list CONFIG.C_PROBE_OUT1_INIT_VAL {0x19CA01CC} CONFIG.C_PROBE_OUT0_INIT_VAL {0x3} CONFIG.C_PROBE_OUT1_WIDTH {32} CONFIG.C_PROBE_OUT0_WIDTH {4} CONFIG.C_NUM_PROBE_OUT {3} CONFIG.C_NUM_PROBE_IN {0} CONFIG.C_EN_PROBE_IN_ACTIVITY {0}] [get_ips vio_setup2]


# vio ip control0
create_ip -name vio -vendor xilinx.com -library ip -version 3.0 -module_name vio_control0
set_property -dict [list CONFIG.C_NUM_PROBE_OUT {6} CONFIG.C_NUM_PROBE_IN {0} CONFIG.C_EN_PROBE_IN_ACTIVITY {0}  CONFIG.C_PROBE_OUT4_WIDTH {4}] [get_ips vio_control0]

# vio ip control1
create_ip -name vio -vendor xilinx.com -library ip -version 3.0 -module_name vio_control1
set_property -dict [list CONFIG.C_PROBE_OUT6_INIT_VAL {0x2} CONFIG.C_PROBE_OUT5_INIT_VAL {0x4} CONFIG.C_PROBE_OUT4_INIT_VAL {0x2} CONFIG.C_PROBE_OUT3_INIT_VAL {0x2} CONFIG.C_PROBE_OUT1_INIT_VAL {0x4} CONFIG.C_PROBE_OUT6_WIDTH {2} CONFIG.C_PROBE_OUT5_WIDTH {4} CONFIG.C_PROBE_OUT4_WIDTH {4} CONFIG.C_PROBE_OUT3_WIDTH {4} CONFIG.C_PROBE_OUT2_WIDTH {4} CONFIG.C_PROBE_OUT1_WIDTH {4} CONFIG.C_PROBE_OUT0_WIDTH {5} CONFIG.C_NUM_PROBE_OUT {8} CONFIG.C_NUM_PROBE_IN {0} CONFIG.C_EN_PROBE_IN_ACTIVITY {0}] [get_ips vio_control1]



# vio ip asd_setup
create_ip -name vio -vendor xilinx.com -library ip -version 3.0 -module_name vio_asd_setup
set_property -dict [list CONFIG.C_PROBE_OUT7_INIT_VAL {0x6A} CONFIG.C_PROBE_OUT6_INIT_VAL {0x2} CONFIG.C_PROBE_OUT5_INIT_VAL {0xE} CONFIG.C_PROBE_OUT4_INIT_VAL {0x4} CONFIG.C_PROBE_OUT3_INIT_VAL {0x05} CONFIG.C_PROBE_OUT2_INIT_VAL {0x7} CONFIG.C_PROBE_OUT7_WIDTH {8} CONFIG.C_PROBE_OUT6_WIDTH {3} CONFIG.C_PROBE_OUT5_WIDTH {4} CONFIG.C_PROBE_OUT4_WIDTH {3} CONFIG.C_PROBE_OUT3_WIDTH {4} CONFIG.C_PROBE_OUT2_WIDTH {3} CONFIG.C_PROBE_OUT1_WIDTH {1} CONFIG.C_PROBE_OUT8_WIDTH {13} CONFIG.C_PROBE_OUT0_WIDTH {16} CONFIG.C_NUM_PROBE_OUT {12} CONFIG.C_NUM_PROBE_IN {0} CONFIG.C_EN_PROBE_IN_ACTIVITY {0}] [get_ips vio_asd_setup]

# vio ip oldasd_setup
create_ip -name vio -vendor xilinx.com -library ip -version 3.0 -module_name vio_oldasd_setup 
set_property -dict [list CONFIG.C_PROBE_OUT7_INIT_VAL {0x7} CONFIG.C_PROBE_OUT6_INIT_VAL {0x4} CONFIG.C_PROBE_OUT5_INIT_VAL {0x5} CONFIG.C_PROBE_OUT4_INIT_VAL {0x7} CONFIG.C_PROBE_OUT3_INIT_VAL {0x2} CONFIG.C_PROBE_OUT2_INIT_VAL {0x66} CONFIG.C_PROBE_OUT8_WIDTH {16} CONFIG.C_PROBE_OUT7_WIDTH {3} CONFIG.C_PROBE_OUT6_WIDTH {3} CONFIG.C_PROBE_OUT5_WIDTH {4} CONFIG.C_PROBE_OUT4_WIDTH {4} CONFIG.C_PROBE_OUT3_WIDTH {3} CONFIG.C_PROBE_OUT2_WIDTH {8} CONFIG.C_PROBE_OUT1_WIDTH {3} CONFIG.C_PROBE_OUT0_WIDTH {8} CONFIG.C_NUM_PROBE_OUT {13} CONFIG.C_NUM_PROBE_IN {0} CONFIG.C_EN_PROBE_IN_ACTIVITY {0}] [get_ips vio_oldasd_setup]

#add_files -fileset sim_1  $source_dir/top_sim
#set_property "top" "tdc_logic_TMR_top_sim" [get_filesets sim_1]

